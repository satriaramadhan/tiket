Fitur

[Y] Login 
[Y] Logout

[Y] Create New User
[Y] Edit User
[Y] Delete User

[Y] Create New Product
[] Edit Product
[Y] Delete Product

[Y] Create New Role
[Y] Edit Role
[Y] Delete Role

[Y] Create New Ticket
[Y] Edit Ticket
[Y] Delete Ticket
[] Set As Closed Ticket
[Y] Set Assign to Ticket
[] Set Resolution Time Ticket
[Y] Set Problem Ticket

[Y] View All Ticket
[Y] View Ticket by Agent
[Y] View Ticket by Product
[Y] View Ticket by Assignment
[Y] View Ticket by Status (Unassigned, Unreplied, Replied, Closed)

[] Filter Ticket by Level
[] Filter Ticket by Last Update Time
[] Filter Ticket by Last Update By

// Report (Number of Closed, Unassigned in 1 day, Unreplied in 1 day, Solving Time)
[] Report by CS and PIC
[] Report by Product
[] Report by Time (Daily, Weekly, Monthly, Annual)