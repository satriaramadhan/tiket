<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class CcTableSeeder extends Seeder {

    public function run()
    {
        DB::table('cc')->delete();
        DB::table('cc')->truncate();
    }
}
