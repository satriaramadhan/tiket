<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class SessionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('sessions')->delete();
        DB::table('sessions')->truncate();
    }
}
