<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class AttachmentTicketTableSeeder extends Seeder {

    public function run()
    {
        DB::table('attachmentticket')->delete();
        DB::table('attachmentticket')->truncate();
    }
}
