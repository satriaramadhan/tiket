<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Document;
use Carbon\carbon;


class TicketTableSeeder extends Seeder {

    public function run()
    {
        DB::table('ticket')->delete();
        DB::table('ticket')->truncate();
        # code...
        // 1
        // Tickets::create([
        //     'no_antrian' => '123',
        //     'transaction_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'subject' =>'Produk Bilna Tidak Sampai',
        //     'message' => 'Tolong dibantu ya, saya kemarin memesan Fiesta Chicken Nugget dari Bilna namun sampai saat ini produknya belum sampai.',
        //     'id_status' => '1',
        //     'level' => 'Medium',
        //     'channel' => 'Kudo Box',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Customer Service',
        //     'id_user' => '6',
        //     'id_product' => '3',
        //     'id_problem' => '2',
        //     'stat_flag' => 'unassigned',
        // ]);
        // // 2
        // Tickets::create([
        //     'no_antrian' => '122',
        //     'transaction_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'subject' =>'Produk Bilna Rusak',
        //     'message' => 'Tolong dibantu ya, saya kemarin memesan Fiesta Chicken Nugget dari Bilna namun sampai saat ini produknya belum sampai.',
        //     'id_status' => '1',
        //     'level' => 'Medium',
        //     'channel' => 'Kudo Box',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Customer Service',
        //     'id_user' => '7',
        //     'id_product' => '3',
        //     'id_problem' => '1',
        //     'stat_flag' => 'unassigned',
        // ]);
    }
}
