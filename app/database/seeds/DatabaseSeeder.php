<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		$this->call('ProductTableSeeder');
		$this->call('PicTableSeeder');
		$this->call('ProblemTableSeeder');
		$this->call('StatusTableSeeder');
		$this->call('TicketTableSeeder');
		$this->call('CommentTableSeeder');
		$this->call('AttachmentTicketTableSeeder');
		$this->call('AttachmentCommentTableSeeder');
		$this->call('CcTableSeeder');
		$this->call('SessionTableSeeder');
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}
