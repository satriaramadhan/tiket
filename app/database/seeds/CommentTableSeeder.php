<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Document;
use Carbon\carbon;


class CommentTableSeeder extends Seeder {

    public function run()
    {
        DB::table('comment')->delete();
        DB::table('comment')->truncate();

        # code...
        // 1
        // Comments::create([
        // 	'id_ticket' => '1',
        // 	'id_user' => '2',
        // 	'date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'message' => 'Oke akan segera saya cek.',
        //     'type' => '2'
        // ]);
        // // 2
        // Comments::create([
        //     'id_ticket' => '1',
        //     'id_user' => '6',
        //     'date' => Carbon::now(new DateTimeZone('Asia/Jakarta'))->addDays(4),
        //     'message' => 'Terima kasih.',
        //     'type' => '2'
        // ]);
        // // 3
        // Comments::create([
        //     'id_ticket' => '2',
        //     'id_user' => '2',
        //     'date' => Carbon::now(new DateTimeZone('Asia/Jakarta'))->addDays(6),
        //     'message' => 'Oke akan segera saya cek.',
        //     'type' => '2'
        // ]);
        // // 4
        // Comments::create([
        //     'id_ticket' => '2',
        //     'id_user' => '7',
        //     'date' => Carbon::now(new DateTimeZone('Asia/Jakarta'))->addDays(8),
        //     'message' => 'Terima kasih.',
        //     'type' => '2'
        // ]);
    }
}
