<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class AttachmentCommentTableSeeder extends Seeder {

    public function run()
    {
        DB::table('attachmentcomment')->delete();
        DB::table('attachmentcomment')->truncate();
    }
}
