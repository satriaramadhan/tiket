<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Document;


class StatusTableSeeder extends Seeder {

    public function run()
    {
        DB::table('status')->delete();
        DB::table('status')->truncate();
        # code...
        // 1
        Status::create([
            'name' => 'Open' // -> Unassigned (Red label)
        ]);
        // 2
        Status::create([
            'name' => 'Closed' // -> After set closed (Green checklist label)
        ]);
        // 3
        Status::create([
            'name' => 'Unreplied' // -> After Assigned (Yellow label with name people assigned to)
        ]);
        // 4
        Status::create([
            'name' => 'Replied' // -> After Assigned (Blue label with name people assigned to)
        ]);
    }
}
