<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Document;
use Carbon\carbon;


class ProblemTableSeeder extends Seeder {

    public function run()
    {
        DB::table('problem')->delete();
        DB::table('problem')->truncate();
        # code...
        // 1
        // Problems::create([
        // 	'name' => 'Warna tidak sesuai',
        // 	'id_product' => '1',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'created_by' => 'Administrator',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Administrator'
        // ]);
        // 2
        // Problems::create([
        // 	'name' => 'Produk tidak sampai',
        // 	'id_product' => '2',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'created_by' => 'Administrator',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Administrator'
        // ]);
        // 3
        // Problems::create([
        // 	'name' => 'Produk kurang',
        // 	'id_product' => '2',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'created_by' => 'Administrator',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Administrator'
        // ]);
        // // 4
        // Problems::create([
        // 	'name' => 'Produk tidak sampai',
        // 	'id_product' => '1'
        // ]);
        // // 5
        // Problems::create([
        // 	'name' => 'Produk tidak sesuai',
        // 	'id_product' => '1'
        // ]);
        // // 6
        // Problems::create([
        // 	'name' => 'Produk tidak sesuai',
        // 	'id_product' => '3'
        // ]);
        // // 7
        // Problems::create([
        // 	'name' => 'Produk tidak sampai',
        // 	'id_product' => '3'
        // ]);
    }
}
