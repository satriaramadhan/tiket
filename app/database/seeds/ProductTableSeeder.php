<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Document;
use Carbon\carbon;


class ProductTableSeeder extends Seeder {

    public function run()
    {
        DB::table('product')->delete();
        DB::table('product')->truncate();
        # code...
        // 1
        // Products::create([
        // 	'name' => 'Fashion',
        //     'id_parent' => '0',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'created_by' => 'Administrator',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Administrator'
        // ]);
        // 2
        // Products::create([
        //     'name' => 'Online Shop',
        //     'id_parent' => '0',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'created_by' => 'Administrator',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Administrator'
        // ]);
        // // 3
        // Products::create([
        //     'name' => 'Food',
        //     'id_parent' => '0',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'created_by' => 'Administrator',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Administrator',
        // ]);
        // // 4
        // Products::create([
        //     'name' => 'Supermarket',
        //     'id_parent' => '0',
        //     'created_date' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'created_by' => 'Administrator',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'Administrator',
        // ]);
        // // 5
        // Products::create([
        //     'name' => 'Pulsa XL',
        //     'id_parent' => '1',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'ramadhan',
        // ]);
        // // 6
        // Products::create([
        //     'name' => 'Pulsa Telkomsel',
        //     'id_parent' => '1',
        //     'last_update_time' => Carbon::now(new DateTimeZone('Asia/Jakarta')),
        //     'last_update_by' => 'akbar',
        // ]);
    }
}
