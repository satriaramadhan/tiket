<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_ticket')->unsigned();
			$table->string('email_user');
			$table->string('user_name');
			$table->dateTime('date');
			$table->text('message');
			$table->string('type'); // 1 -> internal; 2 -> to agent; 3 -> deleted (only show in admin mode)
		});

		Schema::table('comment', function(Blueprint $table)
		{			
			$table->foreign('id_ticket')->references('id')->on('ticket')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('comment', function(Blueprint $table)
		{
			$table->dropForeign('comment_id_ticket_foreign');
		});
		
		Schema::drop('comment');
	}

}
