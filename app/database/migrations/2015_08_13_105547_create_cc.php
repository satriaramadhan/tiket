<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCc extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cc', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_ticket')->unsigned();
			$table->string('email_user');
			$table->string('user_name');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
		});

		Schema::table('cc', function(Blueprint $table)
		{
			$table->foreign('id_ticket')->references('id')->on('ticket')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cc', function(Blueprint $table)
		{
			$table->dropForeign('cc_id_ticket_foreign');
		});

		Schema::drop('cc');
	}

}
