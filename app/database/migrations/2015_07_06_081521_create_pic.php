<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePic extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pic', function(Blueprint $table)
		{
			$table->increments('id')->unique();
			$table->integer('id_product')->unsigned();
			$table->integer('id_pic');
			$table->string('email_pic');
			$table->string('nama_pic');
		});

		Schema::table('pic', function(Blueprint $table)
		{
			$table->foreign('id_product')->references('id')->on('product')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pic', function(Blueprint $table)
		{
			$table->dropForeign('pic_id_product_foreign');
		});
		
		Schema::drop('pic');
	}

}
