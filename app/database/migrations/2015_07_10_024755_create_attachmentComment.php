<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentComment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attachmentComment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_comment')->unsigned();
			$table->string('file_name');
			$table->string('path');
		});

		Schema::table('attachmentComment', function(Blueprint $table)
		{
			$table->foreign('id_comment')->references('id')->on('comment')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('attachmentComment', function(Blueprint $table)
		{
			$table->dropForeign('attachmentComment_id_comment_foreign');
		});

		   Schema::drop('attachmentComment');
	}

}
