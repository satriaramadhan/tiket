<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicket extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ticket', function(Blueprint $table)
		{
			$table->increments('id'); 
			$table->integer('no_antrian'); // Manually Input
			$table->date('transaction_date'); // Manually Input
			$table->text('detail_transaction'); // Retrieve data from API Kudo
			$table->string('subject'); // Manually Input
			$table->text('message'); // Manually Input
			$table->integer('id_status')->unsigned(); // Auto set to 1 (Unassigned)
			$table->string('level'); // Manually Input
			$table->string('channel'); 
			$table->dateTime('created_date');
			$table->string('created_by'); 
			$table->dateTime('last_update_time');
			$table->string('last_update_by');
			$table->string('email_user');
			$table->string('id_fo');
			$table->string('name_fo');
			$table->string('user_name');
			$table->integer('id_product')->unsigned();
			$table->integer('id_problem')->unsigned();
			$table->string('petugas');
			$table->integer('resolution_time'); // On minutes
			$table->dateTime('assign_time'); // Saat di assign to me pertama kali oleh cs
			$table->dateTime('closing_time');
			$table->string('ass_cs');
			$table->string('ass_it');
			$table->string('ass_qa');
			$table->string('ass_pic');
			$table->string('last_solved_by');
			$table->integer('group_ass');
			$table->integer('duplicate_to');
			$table->string('device_id');
			$table->softDeletes();
		});

		Schema::table('ticket', function(Blueprint $table)
		{
			$table->foreign('id_product')->references('id')->on('product')->onDelete('cascade');
			$table->foreign('id_problem')->references('id')->on('problem')->onDelete('cascade');
			$table->foreign('id_status')->references('id')->on('status')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ticket', function(Blueprint $table)
		{
			$table->dropForeign('ticket_id_product_foreign');
			$table->dropForeign('ticket_id_problem_foreign');
			$table->dropForeign('ticket_id_status_foreign');
		});
		
		Schema::drop('ticket');
	}

}
