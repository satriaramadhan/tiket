<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProblem extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('problem', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('id_product')->unsigned();
			$table->dateTime('created_date');
			$table->string('created_by');
			$table->dateTime('last_update_time');
			$table->string('last_update_by');
		});

		Schema::table('problem', function(Blueprint $table)
		{
			$table->foreign('id_product')->references('id')->on('product')->onDelete('cascade');
			

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('problem', function(Blueprint $table)
		{
			$table->dropForeign('problem_id_product_foreign');
		});
		
		Schema::drop('problem');
	}

}
