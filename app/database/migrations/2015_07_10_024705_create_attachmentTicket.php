<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentTicket extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attachmentTicket', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_ticket')->unsigned();
			$table->string('file_name');
			$table->string('path');
		});

		Schema::table('attachmentTicket', function(Blueprint $table)
		{
			$table->foreign('id_ticket')->references('id')->on('ticket')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('attachmentTicket', function(Blueprint $table)
		{
			$table->dropForeign('attachmentTicket_id_ticket_foreign');
		});

		   Schema::drop('attachmentTicket');
	}

}
