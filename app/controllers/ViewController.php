<?php

use Carbon\carbon;

class ViewController extends BaseController {
    public function get500() {
        return View::make('new.500');
    }
    public function get404() {
        return View::make('new.404');
    }
    public function getProduct($id) {
        $product = Products::where('id','=',$id)->get();
        return $product->name;
    }
    public function reportProduk() {
         $data = DB::table('ticket')
                         ->join('product','ticket.id_product','=','product.id')
                         ->select(DB::raw('product.name as name, count(*) as data'))
                         ->groupBy('product.id','ticket.id_product')
                         ->get(); 
        //$report = json_encode($data);
        //dd($report);die;
        return View::make('new.report_produk', compact('data'));     
    }
    public function reportProblem() {
         $data = DB::table('ticket')
                         ->join('problem','ticket.id_problem','=','problem.id')
                         ->select(DB::raw('problem.name as name, count(*) as data'))
                         ->groupBy('problem.name')
                         ->get(); 
        //$report = json_encode($data);
        //dd($report);die;
        return View::make('new.report_problem', compact('data'));     
    }
    public function reportStatus() {
         $data = DB::table('ticket')
                         ->join('status','ticket.id_status','=','status.id')
                         ->select(DB::raw('status.name as name, count(*) as data'))
                         ->groupBy('status.name')
                         ->get(); 
        //$report = json_encode($data);
        //dd($report);die;
        return View::make('new.report_problem', compact('data'));     
    }

    public function reportPriority() {
         $data = DB::table('ticket')
                         ->select(DB::raw('ticket.level as name, count(*) as data'))
                         ->groupBy('ticket.level')
                         ->get(); 
        //$report = json_encode($data);
        //dd($report);die;
        return View::make('new.report_priority', compact('data'));     
    }

     public function reportPeriode() {
         $data = DB::table('ticket')
                         ->select(DB::raw('EXTRACT(YEAR_MONTH FROM created_date) AS name, count(*) as data'))
                         ->groupBy('name')
                         ->get(); 
        //$report = json_encode($data);
        //dd($report);die;
        return View::make('new.report_periode', compact('data'));     
    }
    public function waAPI() {
        $count=0;
        $client = new \GuzzleHttp\Client();
        $waktu = mktime(date('H'),date('i'),date('s'),date('m'),date('d'),date('Y'));
        $url = 'https://api.wha.tools/v1/history?key=1eaa205a-ba5d-41f7-ca21-44e379888f58&start=1439179929194&end='.$waktu.'000';
        $json = file_get_contents($url);
        $_data = json_decode($json,true);
        $data = $_data['result'];
        $msg =json_decode($data);
        $mss = $msg->messages;
        //var_dump($_data);die;
        foreach($mss as $mes){
            if($mes->mine==false){
                //var_dump($mes->body);die;
                    if(Tickets::where('subject','=','nomer WA: '.$mes->to.'')->where('message','=',$mes->body)->exists()){
                        //var_dump('error2');
                    }
                    else{
                        //var_dump($no_antri);die;
                        // Create a new object Ticket
                        $ticket = new Tickets;
                        // Fill atribute with input value
                        //$ticket->no_antrian = '';
                        //$ticket->transaction_date = '';
                        $ticket->subject = 'nomer WA: '.$mes->to.'';
                        $ticket->message = $mes->body;
                        $ticket->id_status = 1;
                        $ticket->level = 'Low';
                        $ticket->channel = 'WhatsApp';
                        $ticket->created_date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                        $ticket->created_by = "Agen via Whatsapp";
                        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                        $ticket->last_update_by = "Agen via Whatsapp";
                        //$ticket->email_user = "";
                        //$ticket->user_name = $email;
                        //$ticket->id_fo = Input::get('ticket_id_fo');
                        //$ticket->name_fo = Input::get('ticket_name_fo');
                        $ticket->id_product = 1;//dummy
                        $ticket->id_problem = 1;//dummy
                        // Save to database
                        //$ticket->save();
                        if($ticket->save()){
                            // $key = '1eaa205a-ba5d-41f7-ca21-44e379888f58';
                            // $to = $mes->to;
                            // $body = 'Keluhan Anda sudah kami terima, silahkan tunggu informasi dari CS kami, terima kasih.';
                            // $response = $client->post('https://api.wha.tools/v1/message?key='.$key.'&to='.$to.'&body='.$body.'');
                            //dd($response->getBody());
                           $count++;
                        }
                    }
                
            }
            
        }
        $message = ''.$count.' Ticket Added From Whatsapp success';
        return Redirect::to('/home')->with('flash_notice', $message);;
        // $ticket = Tickets::All();
        //var_dump($mss);die;
        //return View::make('new.testWA', compact('mss'));     
    }
}
