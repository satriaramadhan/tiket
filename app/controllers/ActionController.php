<?php

use Carbon\carbon;

class ActionController extends BaseController {

	/**
	 * Agen menambahkan komentar dan attachment pada halaman detail tiket di sisi agen
	 *
	 * @return Komentar dan/atau Attachment baru ditambahkan ke tiket, status last update at dan last update by diperbarui
	 */
    public function addComment() {
      $client = new \GuzzleHttp\Client();	
      $comment = new Comments;
      $comment->id_ticket = Input::get('id_ticket');
      $comment->user_name = Auth::user()->name;
      $comment->date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
      $comment->message = Input::get('komentar');
      // $comment->type = Input::get('tipe');
      $comment->type = Input::get('type');
      $ticket = Tickets::find($comment->id_ticket);
      if($comment->save()){
      	if($comment->type==2){
      		if($ticket->channel=="WhatsApp"){
	      		$key = '1eaa205a-ba5d-41f7-ca21-44e379888f58';
	            $to = substr($ticket->subject,10);
	            //var_dump($to);die;
	            $body = $comment->message;
	            $response = $client->post('https://api.wha.tools/v1/message?key='.$key.'&to='.$to.'&body='.$body.'');
	            //var_dump($response);die;
             	$attachment = null;
		      	if(Input::hasFile('attachment')) {
			        $file = Input::file('attachment');
			        $filename = 'Comment'.$comment->id.'_'.$file->getClientOriginalName();
			        $destinationPath = 'att';
			        $file->move($destinationPath, $filename);
			        $attachment = new AttachmentsComments;
			        $attachment->id_comment = $comment->id;
			        $attachment->file_name = $filename;
			        $attachment->path = $destinationPath.'/'.$filename;
			        $attachment->save();
		     	 }
		      	//$ticket = Tickets::find($comment->id_ticket);
			      $ticket->id_status = 4;
			      $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
			      $ticket->last_update_by = $comment->user_name;
			      $ticket->save(); 
			      $id_tiket = $comment->id_ticket;
		      //Mail::send('emails.newComment', array('id_tiket'=>$comment->id_ticket,'agen'=>$comment->user->name,'messages'=>$comment->message,'recipient'=>$comment->ticket->user->name), function($message) use ($id_tiket, $attachment, $comment){
		       //  $message->to($comment->ticket->user->email, $comment->ticket->user->name)->subject('[#'.$id_tiket.' New Comment]');
		        // if($attachment != null) {
		        //    $message->attach($attachment->path);
		         //}
		      //});
				
     		 return Redirect::to('detail/'.$comment->id_ticket);
         }
      	}
      	else{
      		$attachment = null;
		      if(Input::hasFile('attachment')) {
		        $file = Input::file('attachment');
		        $filename = 'Comment'.$comment->id.'_'.$file->getClientOriginalName();
		        $destinationPath = 'att';
		        $file->move($destinationPath, $filename);
		        $attachment = new AttachmentsComments;
		        $attachment->id_comment = $comment->id;
		        $attachment->file_name = $filename;
		        $attachment->path = $destinationPath.'/'.$filename;
		        $attachment->save();
		      }
		      $ticket = Tickets::find($comment->id_ticket);
		      if($ticket->id_status == 1) {
		      	$ticket->petugas = $comment->user_name;
		      }
		      $ticket->id_status = 4;
		      $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
		      $ticket->last_update_by = $comment->user_name;
		      $ticket->save(); 
		      $id_tiket = $comment->id_ticket;
		      //Mail::send('emails.newComment', array('id_tiket'=>$comment->id_ticket,'agen'=>$comment->user->name,'messages'=>$comment->message,'recipient'=>$comment->ticket->user->name), function($message) use ($id_tiket, $attachment, $comment){
		       //  $message->to($comment->ticket->user->email, $comment->ticket->user->name)->subject('[#'.$id_tiket.' New Comment]');
		        // if($attachment != null) {
		        //    $message->attach($attachment->path);
		         //}
		      //});
				
		      return Redirect::to('detail/'.$comment->id_ticket);

      	}
      }
    }

    /**
	 * Merubah status ticket menjadi closed
	 *
	 * @return status Ticket menjadi closed, last update at dan last update by diperbarui
	 */
    public function closingTicket() {
	    $ticket = Tickets::find(Input::get('id_tiket'));
	    $ticket->id_status = 2;
	    $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
	    $ticket->last_update_by = $ticket->user->name;
	    $ticket->save();
	    return Redirect::to('');
	}

	public function closedTicket($id) {
	    $ticket = Tickets::find($id);
	    $ticket->id_status = 2;
	    $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
	    $ticket->last_update_by = Auth::user()->name;
	    $ticket->save();
	    return Redirect::to('');
	}

	public function ticketDestroy($id)
	{
		Tickets::find($id)->delete();
        return Redirect::to('');
	}

	  public function petugasTicket() {
	    $ticket = Tickets::find(Input::get('id_tiket'));
	    $ticket->id_status = 3; //Set jadi unreplied
	    $ticket->petugas = Input::get('petugas');
	    $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
	    $ticket->last_update_by = Auth::user()->name;
	    //var_dump($ticket);die;
	    $ticket->save();
	    return Redirect::to('');
	}

	/**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function createNewTicket() {
		$ticket = new Tickets;
		$ticket->no_antrian = Input::get('noAntrian');
		$ticket->transaction_date = Input::get('tglTransaksi');
		$ticket->subject = Input::get('subject');
		$ticket->message = Input::get('deskripsi');
		$ticket->id_status = 1;
		$ticket->level = 'Low';	
		$ticket->channel = 'Kudo Box';
		$ticket->created_date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
		$ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
		$ticket->last_update_by = Auth::user()->name;
		$ticket->id_user = Auth::user()->id;
		$ticket->id_product = Input::get('produk');   
		$ticket->id_problem = 5;
		$ticket->save();    
		$attachment = null;
		if(Input::hasFile('attachment')) {
	        $file = Input::file('attachment');
	        $filename = 'Tiket'.$ticket->id.'_'.$file->getClientOriginalName();
	        $destinationPath = 'att';
	        $file->move($destinationPath, $filename);
	        $attachment = new AttachmentsTickets;
	        $attachment->id_ticket = $ticket->id;
	        $attachment->file_name = $filename;
	        $attachment->path = $destinationPath.'/'.$filename;
	        $attachment->save();
		}
		$id_tiket = $ticket->id;
   //      Mail::send('emails.newTicket', array('id_tiket'=>$id_tiket,'agen'=>$comment->user->name,'messages'=>$ticket->message,'subject'=>$ticket->subject,'recipient'=>$ticket->user->name), function($message) use ($id_tiket, $attachment){
   //       	$message->to($ticket->user->email, $ticket->user->name)->subject('[New Ticket #'.$id_tiket.']');
			// if($attachment != null) {
			// 	$message->attach($attachment->path);
			// }
   //    	});
    	return Redirect::to('');
    }

    public function editTicket($id) {
		$ticket = Tickets::find($id);
		$ticket->no_antrian = Input::get('noAntrian');
		$ticket->transaction_date = Input::get('tglTransaksi');
		$ticket->subject = Input::get('subject');
		$ticket->message = Input::get('deskripsi');
		$ticket->id_status = 1;
		$ticket->level = Input::get('level');	
		$ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
		$ticket->last_update_by = Auth::user()->name;
		$ticket->id_product = Input::get('produk');   
		$ticket->id_problem = Input::get('problem');
		$ticket->save();    
		$attachment = null;
		if(Input::hasFile('attachment')) {
	        $file = Input::file('attachment');
	        $filename = 'Tiket'.$ticket->id.'_'.$file->getClientOriginalName();
	        $destinationPath = 'att';
	        $file->move($destinationPath, $filename);
	        $attachment = new AttachmentsTickets;
	        $attachment->id_ticket = $ticket->id;
	        $attachment->file_name = $filename;
	        $attachment->path = $destinationPath.'/'.$filename;
	        $attachment->save();
		}
		$id_tiket = $ticket->id;
        Mail::send('emails.newTicket', array('id_tiket'=>$id_tiket,'agen'=>$comment->user->name,'messages'=>$ticket->message,'subject'=>$ticket->subject,'recipient'=>$ticket->user->name), function($message) use ($id_tiket, $attachment){
         	$message->to($ticket->user->email, $ticket->user->name)->subject('[New Ticket #'.$id_tiket.']');
			if($attachment != null) {
				$message->attach($attachment->path);
			}
      	});
    	return Redirect::to('');
    }

   public function edit($id) {
		$ticket = Tickets::find($id);
		$ticket->level = Input::get('level');
		$ticket->id_product = Input::get('product');
		$ticket->id_problem = Input::get('problem');
		$ticket->petugas = Input::get('petugas');	
		$ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
		$ticket->last_update_by = Auth::user()->name;
		$ticket->save();
		$message = 'Update Ticket Succes';
        return Redirect::to('/detail/'.$id)->with('flash_notice', $message);
    }
    

    

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function daftarAdmin()
	{
		$password = Input::get('password');
        $data = array(
	        'email'           => Input::get('email'),
	        'password'        => $password
        ); 
		$rules = array(
		    'email'=> 'required',
		    'password'=>'required'
	    );
	    $validator = Validator::make($data, $rules);       
        if ($validator->fails()) {
            return Redirect::to('signup')
                ->withErrors($validator)
                ->with('flash_error', 'This email is already registered, please choose another one.')
                ->withInput(Input::except('password'));
        } else {
        	$admin = new Users;
			$admin->email = Input::get('email');
			$admin->password = $password;
			$admin->name = 'Administrator';
			$admin->id_role = 1;
			$admin->save();
            return Redirect::to('login')->with('flash_notice', 'Successfully created account! Please login to submit your ad.');
        }
	}

}
