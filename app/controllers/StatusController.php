<?php

class StatusController extends BaseController {
    /**
     * Instantiate a new StatusController instance.
     */
    public function __construct() {
        // $this->beforeFilter('admin'); -> Uncomment setelah dipastikan filter admin sudah berjalan
    }
    /**
     * Create a new status 
     */
    public function postCreate() {
        $status = new Status;
        $status->name = Input::get('status_name');
        $status->save();
        return Redirect::to('');
    }
    /**
     * Edit a status
     */
    public function postEdit() {
        $id = Input::get('status_id');
        $status = Status::find($id);
        $status->name = Input::get('status_name');
        $status->save();
        return Redirect::to('');
    }
    /**
     * Delete a status
     */
    public function postDelete() {
        $id = Input::get('status_id');
        Status::destroy($id);
        return Redirect::to('');
    } 
}