<?php

use Carbon\carbon;

class CommentController2 extends BaseController {
    /**
     * Instantiate a new CommentController instance.
     */
    public function __construct() {
        // $this->beforeFilter('admin'); -> Uncomment setelah dipastikan filter admin sudah berjalan
    }
    /**
     * Comment by Admin, PIC, CS
     */
    public function postComment() {
        $client = new \GuzzleHttp\Client();   
        $comment = new Comments;
        $comment->id_ticket = Input::get('id_ticket');
        $comment->user_name = Session::get('name');
        $comment->date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $comment->message = Input::get('komentar');
        // $comment->type = Input::get('tipe');
        $comment->type = Input::get('type');
        $ticket = Tickets::find($comment->id_ticket);
        if($comment->save()) {
            if($comment->type==2) {
                if($ticket->channel=="WhatsApp") {
                    $key = 'd43b453b-430d-468f-97e9-eb56480aa0a5';
                    $to = substr($ticket->subject,10);
                    //var_dump($to);die;
                    $body = $comment->message;
                    if($response = $client->post('https://api.wha.tools/v1/message?key='.$key.'&to='.$to.'&body='.$body.'')){

                    }
                    //var_dump($response);die;
                    $attachment = null;
                    if(Input::hasFile('attachment')) {
                        $file = Input::file('attachment');
                        $filename = 'Comment'.$comment->id.'_'.$file->getClientOriginalName();
                        $destinationPath = 'att';
                        $file->move($destinationPath, $filename);
                        $attachment = new AttachmentsComments;
                        $attachment->id_comment = $comment->id;
                        $attachment->file_name = $filename;
                        $attachment->path = $destinationPath.'/'.$filename;
                        $attachment->save();
                    }
                    //$ticket = Tickets::find($comment->id_ticket);
                    $ticket->id_status = 4;
                    $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                    $ticket->last_update_by = $comment->user_name;
                    $ticket->save();
                    //dd("goblok");die; 
                    $id_tiket = $comment->id_ticket;
                    //Mail::send('emails.newComment', array('id_tiket'=>$comment->id_ticket,'agen'=>$comment->user->name,'messages'=>$comment->message,'recipient'=>$comment->ticket->user->name), function($message) use ($id_tiket, $attachment, $comment){
                    //  $message->to($comment->ticket->user->email, $comment->ticket->user->name)->subject('[#'.$id_tiket.' New Comment]');
                    // if($attachment != null) {
                    //    $message->attach($attachment->path);
                    //}
                    //});

                    return Redirect::to('detail/'.$comment->id_ticket);
                }
                else{
                    $attachment = null;
                    if(Input::hasFile('attachment')) {
                        $file = Input::file('attachment');
                        $filename = 'Comment'.$comment->id.'_'.$file->getClientOriginalName();
                        $destinationPath = 'att';
                        $file->move($destinationPath, $filename);
                        $attachment = new AttachmentsComments;
                        $attachment->id_comment = $comment->id;
                        $attachment->file_name = $filename;
                        $attachment->path = $destinationPath.'/'.$filename;
                        $attachment->save();
                    }
                    //$ticket = Tickets::find($comment->id_ticket);
                    $ticket->id_status = 4;
                    $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                    $ticket->last_update_by = $comment->user_name;
                    $ticket->save();
                    //dd("goblok");die; 
                    $id_tiket = $comment->id_ticket;
                    //Mail::send('emails.newComment', array('id_tiket'=>$comment->id_ticket,'agen'=>$comment->user->name,'messages'=>$comment->message,'recipient'=>$comment->ticket->user->name), function($message) use ($id_tiket, $attachment, $comment){
                    //  $message->to($comment->ticket->user->email, $comment->ticket->user->name)->subject('[#'.$id_tiket.' New Comment]');
                    // if($attachment != null) {
                    //    $message->attach($attachment->path);
                    //}
                    //});

                    return Redirect::to('detail/'.$comment->id_ticket);
                }
                
            }
            else{
                $attachment = null;
                if(Input::hasFile('attachment')) {
                    $file = Input::file('attachment');
                    $filename = 'Comment'.$comment->id.'_'.$file->getClientOriginalName();
                    $destinationPath = 'att';
                    $file->move($destinationPath, $filename);
                    $attachment = new AttachmentsComments;
                    $attachment->id_comment = $comment->id;
                    $attachment->file_name = $filename;
                    $attachment->path = $destinationPath.'/'.$filename;
                    $attachment->save();
                }
                $ticket = Tickets::find($comment->id_ticket);
                // if($ticket->id_status == 1) {
                //     $ticket->petugas = $comment->user_name;
                // }
                $ticket->id_status = 4;
                $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                $ticket->last_update_by = $comment->user_name;
                $ticket->save(); 
                $id_tiket = $comment->id_ticket;
                foreach ($ticket->cc as $cc) {
                    # code...
                    $url= url('/detail/'.$ticket->id);
                    Mail::send('emails.newComment', array('id_tiket'=>$comment->id_ticket,'messages'=>$comment->message,'sender'=>$comment->user_name,'url'=>$url), function($message) use ($ticket, $attachment, $comment, $cc){
                    $message->to('ad_chadel@yahoo.com', $cc->user_name)->subject('[#'.$ticket->id.' New Comment]');
                         if($attachment != null) {
                            $message->attach($attachment->path);
                        }
                    });
                }

                return Redirect::to('detail/'.$comment->id_ticket);
            }
        }
    }
    /**
     * Soft Delete.
     */
    public function postSoftDelete($id) {
        $comment = Comments::find($id);
        $comment->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $comment->last_update_by = Session::get('name');
        $comment->save();
        $comment->delete();
        return Redirect::to('');
    } 
    /**
     * Delete a comment. Will remove comment from database
     */
    public function postDelete() {
        Comments::withTrashed()->find(Input::get('id_comment'))->forceDelete();
        return Redirect::to('');
    }
}