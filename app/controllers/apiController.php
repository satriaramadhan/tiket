<?php

use Carbon\carbon;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class apiController extends BaseController {
    
    public function getProducts() {
        $data = $this->listProducts(0);
        return APIHelper::response(200,$data);
    }
    public function listProducts($parent) {
        $sql = DB::table('product')->select('id','name','id_parent')
                    ->whereRaw("id_parent = '".$parent."'")->get();
        $index = 0;
        foreach($sql as $rs) {
            $query = DB::table('product')->select('id','name','id_parent')
                        ->whereRaw("id_parent = '".$rs->id."'")->get();
            if(count($query)=='0') {
                $data[$index]['id'] = $rs->id;
                $data[$index]['name'] = $rs->name;
            } else {
                $data[$index]['id'] = $rs->id;
                $data[$index]['name'] = $rs->name;
                $data[$index]['child'] = $this->listProducts($rs->id,$index);
            }
           $index++; 
        }
        return $data;
    }
    /** 
     * Get list ticket berdasarkan email agen.
     */
    public function getListTicket() {
        $input = Input::all();
        $email = $input['email'];
        $tickets = Tickets::where('email_user','=',$email)->get();
        $row = null;
        foreach ($tickets as $ticket) {
            if($ticket->id_status != 5) {
                $row[$ticket->id]['id'] = $ticket->id;
                $row[$ticket->id]['no_antrian'] = $ticket->no_antrian;
                $row[$ticket->id]['transaction_date'] = $ticket->transaction_date;
                $row[$ticket->id]['subject'] = $ticket->subject; 
                $row[$ticket->id]['message'] = $ticket->message;
                if($ticket->id_status == 2) {
                    $row[$ticket->id]['status'] = 'Closed';
                }
                else if($ticket->id_status == 1) {
                    $row[$ticket->id]['status'] = 'Open';
                }
                else {
                    $row[$ticket->id]['status'] = 'Ongoing';
                }
                $row[$ticket->id]['created_date'] = $ticket->created_date;
                $row[$ticket->id]['id_product'] = $ticket->id_product;
                $row[$ticket->id]['id_problem'] = $ticket->id_problem;
                $row[$ticket->id]['resolution_time'] = $ticket->resolution_time;
                $row[$ticket->id]['assign_time'] = $ticket->assign_time;
                $row[$ticket->id]['closing_time'] = $ticket->closing_time;
            }
        }
        return APIHelper::response(200,$row);
    }
    /**
     * Set status ticket as Closed
     */
    public function postSetClosed() {
        $input = Input::all();
        $id = $input['ticket_id'];
        $user_name = $input['partner_name'];
        $ticket = Tickets::find($id);
        $ticket->id_status = 2;
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->closing_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->last_update_by = $user_name;
        $ticket->save();

        $row['id_status'] = 'Closed';
        $row['last_update_time'] = $ticket->last_update_time;
        $row['closing_time'] = $ticket->closing_time;
        $row['last_update_by'] = $user_name;
        return APIHelper::response(200,$row);;
    }
    /**
     * Set status ticket as Open
     */
    public function postSetOpen() {
        $input = Input::all();
        $id = $input['ticket_id'];
        $user_name = $input['partner_name'];
        $ticket = Tickets::find($id);
        if($ticket->comment->count() > 0) {
            $ticket->id_status = 4;
        } 
        else if($ticket->petugas == '') {
            $ticket->id_status = 1;
        }        
        else {
            $ticket->id_status = 3;
        }
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->last_update_by = $user_name;
        $ticket->save();

        $row['id_status'] = 'Open';
        $row['last_update_time'] = $ticket->last_update_time;
        $row['last_update_by'] = $user_name;
        return APIHelper::response(200,$row);;
    }
    public function getProblems() {
        $input = Input::all();
        $product_id = $input['product_id'];
        $data = array();
        $sql = DB::connection('kudo_tiket_support')->table('problem')->select('id','name')
                    ->whereRaw("id_product = '".$product_id."'")->get();
        $i = 0;
        foreach($sql as $rs) {
            $data[$i]['id'] = $rs->id;
            $data[$i]['name'] = $rs->name;
            $i++;
        }
        return APIHelper::response(200,$data);
    }
    /**
     * Get comment & attachment ticket by ID
     */
    public function getCommentTicket() {
        $input = Input::all();
        $id = $input['ticket_id'];
        $comments = Comments::where('id_ticket', '=', $id)->where('type', '=', 2)->get();
        foreach ($comments as $comment) {
            $row[$comment->id]['id'] = $comment->id;
            $row[$comment->id]['email_user'] = $comment->email_user;
            $row[$comment->id]['user_name'] = $comment->user_name;
            $row[$comment->id]['date'] = $comment->date;
            $row[$comment->id]['message'] = $comment->message;
            if($comment->attachment == null) {
                $row[$comment->id]['attachment'] = '';
            }
            else {
                $row[$comment->id]['attachment'] = $comment->attachment->path;
            }
        }
        return APIHelper::response(200,$row);
    }
}