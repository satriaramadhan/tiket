<?php

use Carbon\carbon;

class userController extends BaseController {
	/**
     * Menampilkan halaman form login
     * @return view form login
     */
  	public function getLogin() {
	    return View::make('new.login');     
	}
	/**
	 * Do log in.
	 * @return Response
	 */
	public function postLogin() {
		$url = 'http://test.kudoserver.com:24864/api/box/tiketSupport/loginUser';
		$client = new GuzzleHttp\Client();
		$response = $client->post($url, ['body' => ['email' => Input::get('email'), 'password' => Input::get('password')]]);
		if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
            $response_code = 200;
            $response_data = $response->json();
            if($response_data['message']['status'] == 000) {
            	Session::put('email', $response_data['message']['email']);
	            Session::put('name', $response_data['message']['username']);
	            Session::put('role', $response_data['message']['tiket_user_id']);
	            Session::put('id', $response_data['message']['user_id']);
	            if($response_data['message']['tiket_user_id'] == 1) {
	            	$role = 'Admin';
	            }
	            else if($response_data['message']['tiket_user_id'] == 2) {
	            	$role = 'PIC Produk';
	            }
	            else if($response_data['message']['tiket_user_id'] == 3) {
	            	$role = 'Customer Service';
	            }
	            else if($response_data['message']['tiket_user_id'] == 5) {
	            	$role = 'IT Helpdesk';
	            }
	            else if($response_data['message']['tiket_user_id'] == 6) {
	            	$role = 'Quality Assurance';
	            }
	            else {
	            	$role = 'Kudo Team';
	            }
	            return Redirect::to('')
	            	->with('flash_notice', 'Hi '.$response_data['message']['username'].', you are successfully logged in as '.$role.'');
            }
            else {
            	return Redirect::to('/login')
            	->with('flash_error', $response_data['message']['message'])
	        	->withInput();
            }            
        } else {
            return Redirect::to('/login')
            	->with('flash_error', 'Kombinasi email dan password yang anda masukkan salah')
	        	->withInput();
        }
	}
	/**
	 * Do log out.
	 * @return Response
	 */
	public function postLogout() { 
	    Auth::logout();
	    Session::flush();
	    return Redirect::to('')->with('flash_notice', 'You are successfully logged out.');
	}
	/**
	 * Remove the specified resource from storage.
	 * @param  int  $id
	 * @return Response
	 */
	public function viewProfile() {
		$user_name = Session::get('name');
		$tiket_assign = Tickets::where('petugas', '=', $user_name)->count();
		$tiket_closed = Tickets::where('petugas', '=', $user_name)->where('id_status', '=', 2)->count();
		$user_email = Session::get('email');
		$user_role = Session::get('role');
        return View::make('new.profile', [
        	'user_name'=>$user_name,
        	'user_email'=>$user_email,
        	'user_role'=>$user_role,
        	'tiket_assign'=>$tiket_assign,
        	'tiket_closed'=>$tiket_closed
        ]); 
	}
	/**
    * Get All User
    */
    public static function getAllUsers() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
        	$response_code = 200;
			$response_data = $response->json();
			foreach ($response_data['message'] as $user) {
			    echo '<option value="'.$user['email'].','.$user['username'].'">'.$user['username'].'</option>';
			}
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list PIC. Hubungi admin.');
        }
    }
    /**
    * Get All Admin
    */
    public static function getAllAdmin() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
        	$response_code = 200;
			$response_data = $response->json();
			$i = 0;
			foreach ($response_data['message'] as $user) {
				if($response_data['message'][$i++]['tiket_user_id'] == 1) {
					echo '<option value="'.$user['email'].','.$user['username'].'">'.$user['username'].'</option>';
				}
			}
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list admin.');
        }
    }
    /**
    * Get All Customer Service
    */
    public static function getAllCs() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
        	$response_code = 200;
			$response_data = $response->json();
			$i = 0;
			foreach ($response_data['message'] as $user) {
				if($response_data['message'][$i++]['tiket_user_id'] == 3) {
					echo '<option value="'.$user['email'].','.$user['username'].'" selected>'.$user['username'].'</option>';
				}
			}
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list customer service. Hubungi admin.');
        }
    }
    /**
    * Get All PIC
    */
    public static function getAllPic() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
        	$response_code = 200;
			$response_data = $response->json();
			$i = 0;
			foreach ($response_data['message'] as $user) {
				if($response_data['message'][$i++]['tiket_user_id'] == 2) {
					echo '<option value="'.$user['email'].','.$user['username'].'">'.$user['username'].'</option>';
				}
			}
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list PIC. Hubungi admin.');
        }
    }
    /**
    * Get All Other
    */
    public static function getAllOther() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
        	$response_code = 200;
			$response_data = $response->json();
			$i = 0;
			foreach ($response_data['message'] as $user) {
				if($response_data['message'][$i++]['tiket_user_id'] == 4) {
					echo '<option value="'.$user['email'].','.$user['username'].'">'.$user['username'].'</option>';
				}
			}
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list other user. Hubungi admin.');
        }
    }
    /**
    * Get Detail User
    */
    public static function getUserById($id) {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getUserDetail?id='.$id.'');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
        	$response_code = 200;
			$response_data = $response->json();
			return $response_data['message'];
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list other user. Hubungi admin.');
        }
    }    
    /**
    * Get All Other
    */
    public static function getAllCc($id_tiket) {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        $list_cc = Tickets::find($id_tiket)->cc;
        $arr = array();
        foreach ($list_cc as $cc) {
        	array_push($arr, $cc->email_user);
        }
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
        	$response_code = 200;
			$response_data = $response->json();
			echo '<optgroup label="Admin">';
			$i = 0;
	        foreach ($response_data['message'] as $user) {
				if($response_data['message'][$i++]['tiket_user_id'] == 1) {
					if(in_array($user['email'], $arr)) {
						echo '<option value="'.$user['email'].','.$user['username'].'" selected>'.$user['username'].'</option>';
					}
					else {
						echo '<option value="'.$user['email'].','.$user['username'].'">'.$user['username'].'</option>';
					}
				}
			}
	        echo '</optgroup>';
	        echo '<optgroup label="Customer Service">';
			$i = 0;
	        foreach ($response_data['message'] as $user) {
				if($response_data['message'][$i++]['tiket_user_id'] == 3) {
					if(in_array($user['email'], $arr)) {
						echo '<option value="'.$user['email'].','.$user['username'].'" selected>'.$user['username'].'</option>';
					}
					else {
						echo '<option value="'.$user['email'].','.$user['username'].'">'.$user['username'].'</option>';
					}
				}
			}
	        echo '</optgroup>';
	        echo '<optgroup label="PIC Product">';
			$i = 0;
	        foreach ($response_data['message'] as $user) {
				if($response_data['message'][$i++]['tiket_user_id'] == 2) {
					if(in_array($user['email'], $arr)) {
						echo '<option value="'.$user['email'].','.$user['username'].'" selected>'.$user['username'].'</option>';
					}
					else {
						echo '<option value="'.$user['email'].','.$user['username'].'">'.$user['username'].'</option>';
					}
				}
			}
	        echo '</optgroup>';
	        echo '<optgroup label="Other">';
			$i = 0;
	        foreach ($response_data['message'] as $user) {
				if($response_data['message'][$i++]['tiket_user_id'] == 4) {
					if(in_array($user['email'], $arr)) {
						echo '<option value="'.$user['email'].','.$user['username'].'" selected>'.$user['username'].'</option>';
					}
					else {
						echo '<option value="'.$user['email'].','.$user['username'].'">'.$user['username'].'</option>';
					}
				}
			}
	        echo '</optgroup>';
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list other user. Hubungi admin.');
        }
    }
    /**
    * Get All Agent
    */
    public static function getAllAgent() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListAgents');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
        	$response_code = 200;
			$response_data = $response->json();
			$i = 0;
			foreach ($response_data['message'] as $user) {
				echo '<option value="'.$user['email'].'|'.$user['first_name'].' '.$user['last_name'].'">'.$user['first_name'].' '.$user['last_name'].'</option>';
			}
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list agent. Hubungi admin.');
        }
    }
}
