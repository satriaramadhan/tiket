<?php 

use Carbon\carbon;

class ProductController extends BaseController {
  /**
   * Show list of product
   */
  public function showlist() {
    $ticketcontroller2 = new TicketController2();
    $products = Products::orderBy('last_update_time','DESC')->get();
    $pics =  $ticketcontroller2::getListPic();
    $parents = Products::where('id_parent','=',0)->get();
    return View::make('new.manageproduct',[
      'products'=>$products,
      'parents'=>$parents,
      'pics'=>$pics,
    ]);
  }
  /**
   * Show the form for creating a new resource.
   */
  public function createProduct() {
    //validator
    $rules = array(
      'productName' => 'required|string', 
      );

    $validator = Validator::make(Input::all(), $rules);

    // check if the validator failed 
    if ($validator->fails()) {
        // get the error messages
        // $messages = $validator->messages();
        $message = 'Add Product fail';
        return Redirect::to('/home/product')->with('flash_error', $message);

    } else {
        $product = new Products;
        $product->name = Input::get('productName');
        $product->id_parent = Input::get('id_parent');
        $product->created_date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $product->created_by = Session::get('name');
        $product->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $product->last_update_by = Session::get('name');
        $product->save();
        $pics = Input::get('pic_add');
        foreach ($pics as $arr_pic) {
          $pic = new Pics;
          $arr = explode(',',$arr_pic);
          $pic->id_pic = $arr[0];
          $pic->email_pic = $arr[2];
          $pic->nama_pic = $arr[1];
          $pic->id_product = $product->id;
          $pic->save();
        }
        $message = 'Add "'.$product->name.'" success';
        return Redirect::to('/home/product')->with('flash_notice', $message);
    }
  }
  /**
   * Memproses inputan dari form update client
   */
  public function editProduct() {
    //validator
    $rules = array(
      'productName' => 'required|string', 
      );

    $validator = Validator::make(Input::all(), $rules);

    // check if the validator failed 
    if ($validator->fails()) {
        // get the error messages
        $message = 'Update Product fail';
        return Redirect::to('/home/product')->with('flash_error', $message);

    } else {
        $product = Products::find(Input::get('id'));
        $product->name = Input::get('productName');
        $product->id_parent = Input::get('id_parent');
        $product->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $product->last_update_by = Session::get('name');
        $product->save();
        $arr_pic = $product->pic;
        $pics = Input::get('pic_edit'.$product->id);
        foreach ($arr_pic as $pic) {
          $pic->delete();
        }
        foreach ($pics as $arr_pic) {
          $pic = new Pics;
          $arr = explode(',',$arr_pic);
          $pic->id_pic = $arr[0];
          $pic->email_pic = $arr[2];
          $pic->nama_pic = $arr[1];
          $pic->id_product = $product->id;
          $pic->save();
        }
        $message = 'Update "'.$product->name.'" success';
        return Redirect::to('/home/product')->with('flash_notice', $message);
    }
  }
  /**
   * Hapus product dan picnya.
   */
  public function deleteProduct($id) {
    $product= Products::where('id', '=', $id)->first();
    $message = 'Delete '.$product->name.' success';
    $pics = $product->pic;
    $childproducts = Products::where('id_parent', '=', $id)->get();
    foreach ($pics as $pic) {
      $pic->delete();
    }
    foreach ($childproducts as $childproduct) {
      $childproduct->id_parent = 0;
      $childproduct->save();
    }
    $product->delete();    
    return Redirect::to('/home/product')->with('flash_notice', $message);
  }
  /**
   * Hapus product dan picnya.
   */
  public function showDetail($id) {
    $product = Products::find($id);
    $problems = Problems::where('id_product','=',$id)->orWhere('id_product','=',$product->id_parent)->get();
    return View::make('new.problem',[
      'problems'=>$problems,
      'product'=>$product
      ]);
  }
  /**
   * Show the form for creating a new resource.
   */
  public function createProblem() {
    //validator
    $rules = array(
      'problemName' => 'required|string', 
      );

    $validator = Validator::make(Input::all(), $rules);

    // check if the validator failed 
    if ($validator->fails()) {
        // get the error messages
        // $messages = $validator->messages();
        $message = 'Add Product fail';
        return Redirect::to('product/'.Input::get('product_id'))->with('flash_error', $message);

    } else {

      $masukan = Products::find(Input::get('product_id'));
      
      $problem = new Problems;
      $problem->name = Input::get('problemName');
      $problem->id_product = Input::get('product_id');
      $problem->created_date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
      $problem->created_by = Session::get('name');
      $problem->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
      $problem->last_update_by = Session::get('name');
      $problem->save();
      $message = 'Add "'.$problem->name.'" success';
      return Redirect::to('product/'.Input::get('product_id'))->with('flash_notice', $message);
        
    }
  }
  /**
   * Show the form for creating a new resource.
   */
  public function editProblem() {
    //validator
    $rules = array(
      'problemName' => 'required|string', 
      );

    $validator = Validator::make(Input::all(), $rules);

    // check if the validator failed 
    if ($validator->fails()) {
        // get the error messages
        $message = 'Update Product fail';
        return Redirect::to('product/'.Input::get('product_id'))->with('flash_error', $message);

    } else {

      $masukan = Products::find(Input::get('product_id'));
      
      $problem = Problems::where('id', '=',Input::get('problem_id'))->first();
      $problem->name = Input::get('problemName');
      $problem->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
      $problem->last_update_by = Session::get('name');
      $problem->save();

      $message = 'Update "'.$problem->name.'" success';
      return Redirect::to('product/'.Input::get('product_id'))->with('flash_notice', $message);
    }
  }


  public function deleteProblem($id, $p_id) {
    $masukan = Products::find($id); 
    
    $problem = Problems::where('id', '=', $p_id)->first();
    $problem->delete();
    
    $message = 'Delete Problem '.$masukan->name.' success';
    return Redirect::to('product/'.$id)->with('flash_notice', $message);
  }
  /**
   * Get list of problem by id product
   */
  public function getListProblemByIdProduct($id) {
      $product = Products::find($id);
      $problems = $product->problem;
      foreach ($problems as $problem) {
          echo '<option value="'.$problem->id.'">'.$problem->name.'</option>';
      }
      if($product->id_parent != 0) {
        $problem_parent = Problems::where('id_product','=',$product->id_parent)->get();
        foreach ($problem_parent as $problem) {
          echo '<option value="'.$problem->id.'">'.$problem->name.'</option>';
        }
      }
  }
  /**
   * Get list of pic and cs by id product
   */
  public function getListAssignmentByIdProduct($id) {
      $product = Products::find($id);
      $pics = $product->pic;
      $list_cs = TicketController2::getListCs();
        foreach ($list_cs as $cs) {
          echo '<option value="'.$cs['username'].'">'.$cs['username'].'</option>'; 
        }
      foreach ($pics as $pic) {
          echo '<option value="'.$pic->nama_pic.'">'.$pic->nama_pic.'</option>';
      }
  }
  /**
   * Get trx by tgl & no_antrian
   */
  public function getTrx($param) {
    $arr = explode('|', $param);
    $client = new GuzzleHttp\Client();
    $url = 'http://test.kudoserver.com:24864/api/box/tiketSupport/getProductDetail';
    $response = $client->post($url, ['body' => ['tgl' => $arr[1], 'no_antrian' => $arr[0]]]);
    if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
      $response_code = 200;
      $response_data = $response->json();
      $i = 0;
      foreach ($response_data['message'] as $trx) {
        $value = 'id:'.$trx['id'].';queue_no:'.$trx['queue_no'].';order_id:'.$trx['order_id'].';reference:'.$trx['reference'].';
        email:'.$trx['email'].';item_reference_id:'.$trx['item_reference_id'].';price:'.$trx['price'].';item_name:'.$trx['item_name'].';
        ref1:'.$trx['ref1'].';ref2:'.$trx['ref2'].';recipient_name:'.$trx['recipient_name'].';address:'.$trx['address'].'';
        echo '<option value="'.$value.'">'.$trx['queue_no'].' - '.$trx['item_name'].'</option>';
      }
    } 
    else {
      echo 'No data trx found. Check your trx date and no antrian.';
    }
  }
}
