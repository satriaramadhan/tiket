<?php

class RoleController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$roles = Roles::All();
        return View::make('new.managerole', compact('roles')); 
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('roleCreate');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$input = Input::all();
        $validation = Validator::make($input, Roles::$rules);

        if ($validation->passes()){
            Roles::create($input);
            $message = 'Add New Role success';
            return Redirect::to('/home/role')->with('flash_notice', $message);
        }
        else{
        	$message = 'Add New Role fail';
            return Redirect::to('/home/role')->with('flash_error', $message);
        }
        // return Redirect::route('role.create')
        //     ->withInput()
        //     ->withErrors($validation)
        //     ->with('message', 'There were validation errors.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		//$role = $this->roles->findOrFail($id);

		//return View::make('roleShow',compact('role'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$role = Roles::find($id);
        if (is_null($role))
        {
            return Redirect::route('role.index');
        }
        return View::make('roleEdit', compact('role'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$input = Input::all();
        $validation = Validator::make($input, Roles::$rules);
        if ($validation->passes())
        {
            $role = Roles::find($id);
            $role->update($input);
            $message = 'Update Role to '.$role->name.' success';
            return Redirect::to('/home/role')->with('flash_notice', $message);
        }
        else{
        	$message = 'Update Role '.$role->name.' fail';
            return Redirect::to('/home/role')->with('flash_error', $message);
        }
		// return Redirect::to('/home/role', $id)
  //           ->withInput()
  //           ->withErrors($validation)
  //           ->with('message', 'There were validation errors.');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Roles::find($id)->delete();
		$message = 'Delete Role success';
        return Redirect::to('/home/role')->with('flash_notice', $message);
	}


}
