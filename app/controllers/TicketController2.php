<?php

use Carbon\carbon;

class TicketController2 extends BaseController {
    /** 1
     * Instantiate a new TicketController instance.
     */
    public function __construct() {
        // $this->beforeFilter('admin'); -> Uncomment setelah dipastikan filter admin sudah berjalan
    }
    /** 2
     * Create a new ticket by CS
     */
    public function postCreateByCS() {
        // Create a new object Ticket
        $ticket = new Tickets;
        // Fill atribute with input value based on form
        $ticket->email_user = Input::get('ticket_email_agent');
        $ticket->user_name = explode('|', Input::get('ticket_agent_name'))[1];
        if(Input::has('tglTransaksi')) {
            $ticket->transaction_date = Input::get('tglTransaksi');
        }
        if(Input::has('noAntrian')) {
            $ticket->no_antrian = Input::get('noAntrian');
        }
        if(Input::has('detail_trx')) {
            $trxs = implode('|', Input::get('detail_trx'));
            $ticket->detail_transaction = $trxs;
        }
        $ticket->id_product = Input::get('produk');
        $ticket->id_problem = Input::get('id_problem');
        $ticket->message = Input::get('deskripsi');
        $ticket->channel = '-';
        // Default value for new ticket
        $ticket->id_status = 1; // Unassigned
        $ticket->group_ass = 3; // Add to group customer service view (1: Admin, 2: PIC, 3: CS, 4: Other)
        // Set time 
        $ticket->created_date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->created_by = Session::get('name');
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->last_update_by = Session::get('name');
        
        // Save to database
        if($ticket->save()){
            // Add other user to list cc
            $listcc = Input::get('cc');
            if($listcc != null){
                foreach ($listcc as $data_cc) {
                    $cc = new Cc;
                    $arr = explode(',',$data_cc);
                    $cc->id_ticket = $ticket->id;
                    $cc->email_user = $arr[0];
                    $cc->user_name = $arr[1];
                    $cc->save();
                }
            }
            // Add creator to list cc
            $cu = new Cc;
            $cu->id_ticket = $ticket->id;
            $cu->email_user = Session::get('email');
            $cu->user_name = Session::get('name');
            $cu->save();
            // If has attachment, create object attachment
            $attachment = null;
            if(Input::hasFile('attachment')) {
                $file = Input::file('attachment');
                $filename = 'Tiket'.$ticket->id.'_'.$file->getClientOriginalName();
                $destinationPath = 'att';
                $file->move($destinationPath, $filename);
                $attachment = new AttachmentsTickets;
                $attachment->id_ticket = $ticket->id;
                $attachment->file_name = $filename;
                $attachment->path = $destinationPath.'/'.$filename;
                $attachment->save();
            }
            // Send email to every user in list cc
            $tiket = Tickets::find($ticket->id);
            foreach ($tiket->cc as $ccc) {
                # code...
                $url= url('/detail/'.$tiket->id);
                //var_dump($tiket->no_antrian);die;
                Mail::queue('emails.newTicket', array('id_tiket'=>$tiket->id,'user'=>Session::get('name'),'messages'=>$tiket->message,'subject'=>$tiket->subject ,'no_antrian'=>$tiket->no_antrian,'transaction_date'=>$tiket->transaction_date,'product_name'=>$tiket->product->name,'problem_name'=>$tiket->problem->name,'url'=>$url), function($message) use ($ccc, $tiket, $attachment){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$tiket->id.' New Ticket]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                    if($attachment != null) {
                       $message->attach($attachment->path);
                    }
                });
            }
            $message = 'Create Ticket Succes';
            return Redirect::to('/home')->with('flash_notice', $message);
        }
        else {
            $message = 'Failed to create ticket';
            return Redirect::to('/home')->with('flash_error', $message);
        }
    }
    /**
     * 3. Create ticket from Whatsapp
     */
    public function postCreateFromWa() {
        //---Add ke WA---//
        $count=0;
        $client = new \GuzzleHttp\Client();
        $waktu = mktime(date('H'),date('i'),date('s'),date('m'),date('d'),date('Y'));
        $url = 'https://api.wha.tools/v1/history?key=d43b453b-430d-468f-97e9-eb56480aa0a5&start=1439453205000&end='.$waktu.'000';
        $json = file_get_contents($url);
        $_data = json_decode($json,true);
        $data = $_data['result'];
        $msg =json_decode($data);
        $mss = $msg->messages;
        //var_dump($_data);die;
        foreach($mss as $mes){
            if(isset($mes->body)){
                if($mes->mine==false){
                    //var_dump($mes->body);
                   if(Tickets::where('subject','=','Nomor WA: '.$mes->to.'')->where('id_status','!=',2)->exists()){
                        $ticks = Tickets::where('subject','=','Nomor WA: '.$mes->to.'')->where('id_status','!=',2)->get();
                        foreach($ticks as $tic){
                            if(Comments::where('user_name','=','Agen')->where('message','=',$mes->body)->exists()){

                            }
                            else{
                                if(Tickets::where('subject','=','Nomor WA: '.$mes->to.'')->where('message','=',$mes->body)->exists()){
                                }
                                else{
                                    $comment = new Comments;
                                    $comment->id_ticket = $tic->id;
                                    $comment->user_name = "Agen";
                                    $comment->date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                                    $comment->message = $mes->body;
                                    $comment->type = 1;
                                    $comment->save();
                                    $tic->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                                    $tic->last_update_by = "Agen";
                                    $tic->save();
                                }
                            }
                        }
                        //var_dump('error2');
                    }
                    elseif(Tickets::where('subject','=','Nomor WA: '.$mes->to.'')->where('message','=',$mes->body)->where('id_status','=',2)->exists()){

                    }
                    else{
                        if(Comments::where('user_name','=','Agen')->where('message','=',$mes->body)->exists()){

                        }
                        else{
                            // Create a new object Ticket
                            $ticket = new Tickets;
                            // Fill atribute with input value
                            $ticket->subject = 'Nomor WA: '.$mes->to.'';
                            $ticket->message = $mes->body;
                            $ticket->id_status = 1;
                            $ticket->level = '-';
                            $ticket->channel = 'WhatsApp';
                            $ticket->created_date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                            $ticket->created_by = "Agen";
                            $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
                            $ticket->last_update_by = "Agen";
                            $ticket->id_product = 1;//dummy
                            $ticket->id_problem = 1;//dummy
                            $ticket->group_ass = 3; // Auto assign to cs group
                            // Save to database
                            if($ticket->save()){
                                // $key = 'd43b453b-430d-468f-97e9-eb56480aa0a5';
                                // $to = $mes->to;
                                // $body = 'Keluhan Anda sudah kami terima, silahkan tunggu informasi dari CS kami, terima kasih.';
                                // $response = $client->post('https://api.wha.tools/v1/message?key='.$key.'&to='.$to.'&body='.$body.'');
                                //dd($response->getBody());
                               $count++;
                            }
                        }
                    }
                }
            }
        }
        return Tickets::all()->count();
    }
    /**
     * 4. Report duplicate ticket
     */
    public function postReportDuplicateTo() {
        $thisTicket = Tickets::find(Input::get('ticket_id'));
        $dupTicket = Tickets::find(Input::get('duplicate_to'));
        $thisTicket->duplicate_to = Input::get('duplicate_to');
        if($thisTicket->save()) {
            $comment1 = new Comments;
            $comment1->id_ticket = Input::get('ticket_id');
            $comment1->email_user = Session::get('email');
            $comment1->user_name = Session::get('name');
            $comment1->date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
            $comment1->message = 'Ticket #'.Input::get('ticket_id').' duplicate to ticket #'.Input::get('duplicate_to');
            $comment1->type = 1;
            $comment1->save();

            $comment2 = new Comments;
            $comment2->id_ticket = Input::get('duplicate_to');
            $comment2->email_user = Session::get('email');
            $comment2->user_name = Session::get('name');
            $comment2->date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
            $comment2->message = 'Ticket #'.Input::get('duplicate_to').' duplicate by ticket #'.Input::get('ticket_id');
            $comment2->type = 1;
            $comment2->save();

            $thisTicket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
            $thisTicket->last_update_by = Session::get('name');
            $dupTicket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
            $dupTicket->last_update_by = Session::get('name');

            $thisTicket->save();
            $dupTicket->save();

            return Redirect::to('detail/'.Input::get('ticket_id'))
                ->with('flash_notice', 'Success report ticket #'.Input::get('ticket_id').' as duplicate to tiket'.Input::get('duplicate_to'));
        }
        else {
            return Redirect::to('detail/'.Input::get('ticket_id'))
                ->with('flash_error', 'Failed to report duplicate');
        }
    }
    /** 3
     * Edit cc ticket
     */
    public function postEditCc() {
        $ticket = Tickets::find(Input::get('ticket_id'));
        $ccbefore = $ticket->cc;
        $listcc = Input::get('cc');
        foreach ($ccbefore as $cc) {
          $cc->delete();
        }
        foreach ($listcc as $data_cc) {
          $cc = new Cc;
          $arr = explode(',',$data_cc);
          $cc->id_ticket = $ticket->id;
          $cc->email_user = $arr[0];
          $cc->user_name = $arr[1];
          $cc->save();
        }
        $message = 'Add CC Succes';
        return Redirect::to('/detail/'.Input::get('ticket_id'))->with('flash_notice', $message);
    }
    /** 4
     * Soft Delete a ticket. Will not deleted ticket from database. Only change ticket's status to deleted.
     */
    public function postSoftDelete($id) {
        $ticket = Tickets::find($id);
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->last_update_by = Session::get('name');
        $ticket->save();
        $url= url('/detail/'.$ticket->id);
        $messages = "Delete";
            foreach($ticket->cc as $ccc){
                 Mail::queue('emails.newUpdate', array('id_tiket'=>$ticket->id,'user'=>$ticket->last_update_by,'time'=>$ticket->last_update_time,'url'=>$url,'messages'=>$messages), function($message) use ($ccc, $ticket){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$ticket->id.' New Ticket Update]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                });
            }
        $ticket->delete();
        return Redirect::to('');
    } 
    /** 5
     * Delete a ticket. Will remove ticket from database
     */
    public function postDelete() {
        Tickets::withTrashed()->find(Input::get('id_tiket'))->forceDelete();
        return Redirect::to('');
    }
    /** 6
     * Restore a ticket. 
     */
    public function postRestore() {
        Tickets::withTrashed()->find(Input::get('id_tiket'))->restore();
        return Redirect::back();
    }
    /** 7
     * Assign ticket to Me
     */
    public function postSetAssignTo() {
        $id = Input::get('id_tiket');
        $ticket = Tickets::find($id);
        if(Session::get('role') == 3){
            $ticket->ass_cs = Session::get('name');
        }
        else if(Session::get('role') == 5) {
            $ticket->ass_it = Session::get('name');    
        }
        else if(Session::get('role') == 6) {
            $ticket->ass_qa = Session::get('name');
        }
        else if (Session::get('role') == 2) {
            $ticket->ass_pic = Session::get('name');
        }
        $ticket->petugas = Session::get('name');
        if(empty($ticket->comment)){
            $ticket->id_status = 3; // Set status menjadi unreplied
        }
        else{
            $ticket->id_status = 4; // Set status menjadi replied
        }
        $ticket->assign_time = Carbon::now(new DateTimeZone('Asia/Jakarta')); // Set waktu assignment
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->last_update_by = Session::get('name');
        if($ticket->save()){
            if(!Cc::where('id_ticket','=',$ticket->id)->where('user_name','=',$ticket->petugas)->exists()){
                $cc = new Cc;
                $cc->id_ticket = $ticket->id;
                $cc->user_name = Session::get('name');
                $cc->email_user = Session::get('email');
                $cc->save();
            }
            $url= url('/detail/'.$ticket->id);
            foreach($ticket->cc as $ccc){
                 Mail::queue('emails.newAssignme', array('id_tiket'=>$ticket->id,'petugas'=>$ticket->petugas,'date'=>$ticket->last_update_time,'url'=>$url), function($message) use ($ccc, $ticket){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$ticket->id.' New Ticket Assignment]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                });
            }
        }
        return Redirect::to('/detail/'.$ticket->id);
    }
    /** 8
     * Assign ticket to Group 
     */
    public function postSetAssignToGroup() {
        $id = Input::get('id_tiket');
        $ticket = Tickets::find($id);
        $ticket->group_ass = Input::get('group');
        $ticket->id_status = 1; // Set status menjadi OPEN LAGI
        $ticket->petugas = null;
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->last_update_by = Session::get('name');
        if($ticket->save()){
            $url= url('/detail/'.$ticket->id);
            foreach($ticket->cc as $ccc){
                 Mail::queue('emails.newAssigngrup', array('id_tiket'=>$ticket->id,'group'=>$ticket->group_ass,'date'=>$ticket->last_update_time,'url'=>$url), function($message) use ($ccc, $ticket){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$ticket->id.' New Ticket Assignment]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                });
            }
        }
        return Redirect::to('');
    }
    /** 10 
     * Set status ticket as Closed
     */
    public function postSetClosed() {
        $id = Input::get('ticket_id');
        $ticket = Tickets::find($id);
        $ticket->id_status = 2;
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->closing_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->last_update_by = Session::get('name');
        if($ticket->save()){
            $url= url('/detail/'.$ticket->id);
            $messages = "Close Ticket";
            foreach($ticket->cc as $ccc){
                 Mail::queue('emails.newUpdate', array('id_tiket'=>$ticket->id,'user'=>$ticket->last_update_by,'time'=>$ticket->last_update_time,'url'=>$url,'messages'=>$messages), function($message) use ($ccc, $ticket){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$ticket->id.' New Ticket Update]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                });
            }

        }
        return Redirect::to('');
    }

    /** 10 
     * Set mark ticket as Solved
     */
    public function postSetSolved() {
        $id = Input::get('ticket_id');
        $ticket = Tickets::find($id);
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        if(Session::get('role')==2){
            $ticket->petugas = $ticket->ass_qa;
            $ticket->group_ass = 6;
        }
        elseif(Session::get('role')==6){
            $ticket->petugas = $ticket->ass_it;
            $ticket->group_ass = 5;
        }
        elseif(Session::get('role')==5){
            $ticket->petugas = $ticket->ass_cs;
            $ticket->group_ass = 3;
        }
        $ticket->last_solved_by = Session::get('name');
        $ticket->last_update_by = Session::get('name');
        if($ticket->save()){
            $url= url('/detail/'.$ticket->id);
            $messages = "Ticket Mark as Solved";
            foreach($ticket->cc as $ccc){
                 Mail::queue('emails.newUpdate', array('id_tiket'=>$ticket->id,'user'=>$ticket->last_update_by,'time'=>$ticket->last_update_time,'url'=>$url,'messages'=>$messages), function($message) use ($ccc, $ticket){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$ticket->id.' New Ticket Update]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                });
            }

        }
        return Redirect::to('/detail/'.$ticket->id);
    }
    /** 10 
     * Set mark ticket as Solved
     */
    public function postSetUnsolved() {
        $id = Input::get('ticket_id');
        $ticket = Tickets::find($id);
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        if(Session::get('role')==3){
            $ticket->petugas = $ticket->ass_it;
            $ticket->group_ass = 5;
        }
        elseif(Session::get('role')==5){
            $ticket->petugas = $ticket->ass_qa;
            $ticket->group_ass = 6;
        }
        elseif(Session::get('role')==6){
            $ticket->petugas = $ticket->ass_pic;
            $ticket->group_ass = 2;
        }
        $ticket->last_solved_by = "";
        $ticket->last_update_by = Session::get('name');
        if($ticket->save()){
            $url= url('/detail/'.$ticket->id);
            $messages = "Ticket Mark as UnSolved";
            foreach($ticket->cc as $ccc){
                 Mail::queue('emails.newUpdate', array('id_tiket'=>$ticket->id,'user'=>$ticket->last_update_by,'time'=>$ticket->last_update_time,'url'=>$url,'messages'=>$messages), function($message) use ($ccc, $ticket){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$ticket->id.' New Ticket Update]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                });
            }

        }
        return Redirect::to('/detail/'.$ticket->id);
    }
    /** 10 
     * Set status ticket as Open
     * MASIH HARUS DI CEK LAGI!!!
     */
    public function postSetOpen() {
        $id = Input::get('ticket_id');
        $ticket = Tickets::find($id);
        if($ticket->petugas != '') {
            if($ticket->comment == null) {
                $ticket->id_status = 3;
            }
            else {
                $ticket->id_status = 4;
            }
        }
        else {
            $ticket->id_status = 1;
        }
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->closing_time = null;
        $ticket->last_update_by = Session::get('name');
        if($ticket->save()){
            $url= url('/detail/'.$ticket->id);
            $messages = "Re-Open Ticket";
            foreach($ticket->cc as $ccc){
                 Mail::queue('emails.newUpdate', array('id_tiket'=>$ticket->id,'user'=>$ticket->last_update_by,'time'=>$ticket->last_update_time,'url'=>$url,'messages'=>$messages), function($message) use ($ccc, $ticket){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$ticket->id.' New Ticket Update]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                });
            }
        }
        return Redirect::to('');
    }
    /*
     * Get list all ticket.
     */
    public function getListAllTicket() {
        // Get ticket
        if(in_array(Session::get('role'), array(1,3))) {
            $tickets = Tickets::orderBy('last_update_time','desc')->get();
        }
        else if(in_array(Session::get('role'), array(2,5,6))) {
            $arr = Cc::where('email_user','=',Session::get('email'))->lists('id_ticket');
            $tickets = Tickets::whereIn('id',$arr)->orWhere('group_ass','=',Session::get('role'))->orderBy('last_update_time','desc')->get();
        }
        else {
             $arr = Cc::where('email_user','=',Session::get('email'))->lists('id_ticket');
             $tickets = Tickets::whereIn('id',$arr)->orderBy('last_update_time','desc')->get();
        }
        // Return to view
        $dataproduct = Products::where('id_parent','=','0')->where('id','!=','0')->get();
        $nama = 'All Tickets';
        $count = Tickets::all()->count();
        if(Input::get("action")=="content") {
            return View::make('new.content.home', compact('tickets','nama','dataproduct','count'))->with("ajax",true); 
        }else{
            return View::make('new.dashboard', compact('tickets','nama','dataproduct','count'))->with("ajax",false); 
        }
    }
    /*
     * Get list my ticket.
     */
    public function getListMyTicket() {
        // Get ticket
        $arr = Cc::where('email_user','=',Session::get('email'))->lists('id_ticket');
        $tickets = Tickets::whereIn('id',$arr)->orderBy('last_update_time','desc')->get();
        // Return to view
        $dataproduct = Products::All();
        $nama = 'My Tickets';
        $count = Tickets::all()->count();
        if(Input::get("action")=="content") {
            return View::make('new.content.home', compact('tickets','nama','dataproduct','count'))->with("ajax",true); 
        }else{
            return View::make('new.dashboard', compact('tickets','nama','dataproduct','count'))->with("ajax",false); 
        }
    }
    /*
     * Get list open ticket.
     */
    public function getListTicketByStatus($id_status) {
        // Get ticket
        if(in_array(Session::get('role'), array(1,3))) {
            $tickets = Tickets::where('id_status','=',$id_status)->orderBy('last_update_time','desc')->get();
        }
        else if(in_array(Session::get('role'), array(2,5,6))) {
            $arr = Cc::where('email_user','=',Session::get('email'))->lists('id_ticket');
            $tickets = Tickets::whereIn('id',$arr)->orWhere('group_ass','=',Session::get('role'))->lists('id');
            $tickets = Tickets::whereIn('id',$tickets)->where('id_status','=',$id_status)->orderBy('last_update_time','desc')->get();
        }
        else {
            $arr = Cc::where('email_user','=',Session::get('email'))->lists('id_ticket');
            $tickets = Tickets::whereIn('id',$arr)->where('id_status','=',$id_status)->orderBy('last_update_time','desc')->get();
        }
        // Return to view
        $dataproduct = Products::All();
        $nama = '';
        $count = Tickets::all()->count();
        switch ($id_status) {
            case 1:
                $nama = 'Open Tickets';
                break;
            case 2:
                $nama = 'Closed Tickets';
                break;
            case 3:
                $nama = 'Unreplied Tickets';
                break;
            case 4:
                $nama = 'Replied Tickets';
                break;
            default:
                $nama = 'Hah???';
                break;
        }
        if(Input::get("action")=="content") {
            return View::make('new.content.home', compact('tickets','nama','dataproduct','count'))->with("ajax",true); 
        }else{
            return View::make('new.dashboard', compact('tickets','nama','dataproduct','count'))->with("ajax",false); 
        }
    }
    /**
    * Get list ticket by Product. Include all ticket related to product's child.
    */
    public function getListTicketByProduct($id_product) {
        $product = Products::find($id_product);
        $child = array();
        // If this product is a parent, get all child.
        if($product->id_parent == 0) {
            $child = Products::where('id_parent','=',$id_product)->lists('id');
        }
        array_push($child, $id_product);
        // Get ticket
        if(in_array(Session::get('role'), array(1,3))) {
            $tickets = Tickets::where('id_product','=',$id_product)->orderBy('last_update_time','desc')->get();
        }
        else if(in_array(Session::get('role'), array(2,5,6))) {
            $arr = Cc::where('email_user','=',Session::get('email'))->lists('id_ticket');
            $tickets = Tickets::whereIn('id',$arr)->orWhere('group_ass','=',Session::get('role'))->lists('id');
            $tickets = Tickets::whereIn('id',$tickets)->whereIn('id_product',$child)->orderBy('last_update_time','desc')->get();
        }
        else {
            $arr = Cc::where('email_user','=',Session::get('email'))->lists('id_ticket');
            $tickets = Tickets::whereIn('id',$arr)->whereIn('id_product',$child)->orderBy('last_update_time','desc')->get();
        }
        // Return to view
        $dataproduct = Products::All();
        $nama = 'All Tickets Related to Products'.$product->name;
        if(Input::get("action")=="content") {
            return View::make('new.content.home', compact('tickets','nama','dataproduct'))->with("ajax",true); 
        }else{
            return View::make('new.dashboard', compact('tickets','nama','dataproduct'))->with("ajax",false); 
        }       
    }
    /*
     * Get list open ticket.
     */
    public function getListTicketByGroupAss($group_ass) {
        // Get ticket
        $tickets = Tickets::where('group_ass','=',$group_ass)->orderBy('last_update_time','desc')->get();
        // Return to view
        $dataproduct = Products::All();
        $nama = '';
        switch ($group_ass) {
            case 2:
                $nama = 'All Tickets Assigned to PIC Product';
                break;
            case 3:
                $nama = 'All Tickets Assigned to Customer Service';
                break;
            case 5:
                $nama = 'All Tickets Assigned to IT Helpdesk';
                break;
            case 6:
                $nama = 'All Tickets Assigned to QA';
                break;
            default:
                $nama = 'Hah???';
                break;
        }
        if(Input::get("action")=="content") {
            return View::make('new.content.home', compact('tickets','nama','dataproduct'))->with("ajax",true); 
        }else{
            return View::make('new.dashboard', compact('tickets','nama','dataproduct'))->with("ajax",false); 
        }
    }
    /**
    * Get list of pic and cs by id ticket
    */
    public function getListTicketByPetugas($petugas) {
        $tickets = Tickets::where('petugas','=',$petugas)->orderBy('last_update_time','desc')->get();
        $nama = 'All ticket assigned to '.$petugas;
        $dataproduct = Products::All();
        if(Input::get("action")=="content") {
            return View::make('new.content.home', compact('tickets','nama','dataproduct'))->with("ajax",true); 
        }else{
            return View::make('new.dashboard', compact('tickets','nama','dataproduct'))->with("ajax",false); 
        }     
    }
    /**
    * Get list trashed ticket
    */
    public function getListTrashedTicket() {
        $tickets = Tickets::onlyTrashed()->orderBy('last_update_time','desc')->get();
        $dataproduct = Products::All();
        $nama = 'All Trashed Tickets';
        if(Input::get("action")=="content") {
            return View::make('new.content.home', compact('tickets','nama','dataproduct'))->with("ajax",true); 
        }else{
            return View::make('new.dashboard', compact('tickets','nama','dataproduct'))->with("ajax",false); 
        }       
    }

    /**
     * Return view home. Call getListTicket(home).
     */
    public function getIndex() {
        return Redirect::to('/myticket');
    }

    /**
     * Menampilkan halaman detail tiket
     *
     * @return Response
     */
    public function getDetailTicket($id) {
        $ticket = Tickets::find($id);
        if($ticket == null) {
            return Redirect::to('home')->with('flash_error','Ticket #'.$id.' has been deleted. Restore to see detail ticket!');
        }
        $products = Products::All();
        $problem = Problems::find($ticket->id_problem);
        $comments = $ticket->comment;
        $arr_trx = explode('|', $ticket->detail_transaction);
        if($ticket->id_status == 2) {
            $closed_by = $ticket->last_update_by;
            $closed_at = $ticket->closing_time;
        }
        else {
            $closed_by = '-';
            $closed_at = '-';
        }
        $duplicate_by = Tickets::where('duplicate_to','=',$ticket->id)->lists('id');;
        return View::make('new.detail', array(
            'ticket' => $ticket,
            'comments' => $comments,
            'problem' => $problem,
            'products' => $products,
            'closed_by' => $closed_by,
            'closed_at' =>$closed_at,
            'listcc' => $ticket->cc,
            'arr_trx' => $arr_trx,
            'duplicate_by' => $duplicate_by
        ));
    }
    /**
    * Get list of pic and cs by id ticket
    */
    public function getListAssignmentByIdTicket($id) {
        $ticket = Tickets::find($id);
        $petugas = $ticket->petugas;
        $product = Products::find($ticket->id_product);
        $pics = $product->pic;
        $list_cs = TicketController2::getListCs();
        foreach ($list_cs as $cs) {
            if($petugas == $cs['username']) {
                echo '<option value="'.$cs['username'].'" selected>'.$cs['username'].'</option>';
            }
            else {
                echo '<option value="'.$cs['username'].'">'.$cs['username'].'</option>';
            }   
        }
        foreach ($pics as $pic) {
            if($petugas == $pic->nama_pic) {
                echo '<option value="'.$pic->nama_pic.'" selected>'.$pic->nama_pic.'</option>';
            }
            else {
                echo '<option value="'.$pic->nama_pic.'">'.$pic->nama_pic.'</option>';
            }   
        }
    }
    /**
    * Get List KudoTeam
    */
    public function getKudoTeam() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
          $response_code = 200;
          $response_data = $response->json();
          $teams = array();
          for($i = 0; $i < count($response_data['message']); $i++) {
            if($response_data['message'][$i]['tiket_user_id'] != 4 && $response_data['message'][$i]['tiket_user_id'] != 1) {
                array_push($teams, $response_data['message'][$i]);
            }
          }
          return View::make('new.kudoteam',['teams'=>$teams]);
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list PIC. Hubungi admin.');
        }
    }
    /**
    * Get List Products
    */
    public function getListProduct() {
        $products = Products::All();
        return View::make('new.listproduct', ['products'=>$products]);       
    }
    
    /**
    * COUNT Unassigned ticket
    */
    public static function countUnassigned() {
        if(Session::get('role') == 2) {
            $arr = array();
            $pics = Pics::where('email_pic','=',Session::get('email'));
            foreach ($pics as $pic) {
                array_push($arr, $pic->id_product);
            }
            return $tickets = Tickets::where('id_status','=',1)->whereIn('id_product', $arr)->count();
        }
        else {
			if(Session::get("email")=="admin@kudo.co.id"){
			 return Tickets::where('id_status','=',1)->count();	
			}else{
				return Tickets::where('id_status','=',1)->where("group_ass","=",Session::get('role'))->count();
			}
            
        }        
    }

    public static function countUnreplied() {
        if(Session::get('role') == 2) {
            return $tickets = Tickets::where('petugas','=',Session::get('name'))
            ->where('id_status','=',3)->count();
        }
        else {
			if(Session::get("email")=="admin@kudo.co.id"){
			 return Tickets::where('id_status','=',3)->count();	
			}else{
				return Tickets::where('id_status','=',3)->where("group_ass","=",Session::get('role'))->count();
			}
            
        }
    }

    public static function countReplied() {
        if(Session::get('role') == 2) {
            return $tickets = Tickets::where('petugas','=',Session::get('name'))
            ->where('id_status','=',4)->count();
        }
        else {

			if(Session::get("email")=="admin@kudo.co.id"){
			 return Tickets::where('id_status','=',4)->count();	
			}else{
				return Tickets::where('id_status','=',4)->where("group_ass","=",Session::get('role'))->count();
			}
        }
    }

    public static function countClosed() {
        if(Session::get('role') == 2) {
            return $tickets = Tickets::where('petugas','=',Session::get('name'))
            ->where('id_status','=',2)->count();
        }
        else {
				if(Session::get("email")=="admin@kudo.co.id"){
			 return Tickets::where('id_status','=',2)->count();	
			}else{
				return Tickets::where('id_status','=',2)->where("group_ass","=",Session::get('role'))->count();
			}
        }
    }
    /**
    * COUNT All ticket by id product
    */
    public static function countAllTicket($id) {
        $product = Products::find($id);
        if(Session::get('role') == 2) {
            if($product->id_parent == 0){
                $childs = Products::where('id_parent','=',$id)->get();
                $arr = array();
                array_push($arr, $id);
                foreach ($childs as $child) {
                    array_push($arr, $child->id);
                }
                return $tickets = Tickets::whereIn('id_product',$arr)->where('petugas','=',Session::get('name'))->count();
            }else{
                return $tickets = Tickets::where('id_product','=',$id)->where('petugas','=',Session::get('name'))->count();
            }            
        }
        else {
            if($product->id_parent == 0){
                $childs = Products::where('id_parent','=',$id)->get();
                $arr = array();
                array_push($arr, $id);
                foreach ($childs as $child) {
                    array_push($arr, $child->id);
                }
                return $tickets = Tickets::whereIn('id_product',$arr)->count();
            }
            else {
                return $tickets = Tickets::where('id_product','=',$id)->count();
            }
        }        
    }
    /**
    * COUNT Closed ticket by id product
    */
    public static function countClosedTicket($id) {
        $product = Products::find($id);
        if(Session::get('role') == 2) {
            if($product->id_parent == 0){
                $childs = Products::where('id_parent','=',$id)->get();
                $arr = array();
                array_push($arr, $id);
                foreach ($childs as $child) {
                    array_push($arr, $child->id);
                }
                return $tickets = Tickets::whereIn('id_product',$arr)->where('petugas','=',Session::get('name'))->where('id_status','=', 2)->count();
            }else{
                return $tickets = Tickets::where('id_product','=',$id)->where('petugas','=',Session::get('name'))->where('id_status','=', 2)->count();
            }            
        }
        else {
            if($product->id_parent == 0){
                $childs = Products::where('id_parent','=',$id)->get();
                $arr = array();
                array_push($arr, $id);
                foreach ($childs as $child) {
                    array_push($arr, $child->id);
                }
                return $tickets = Tickets::whereIn('id_product',$arr)->where('id_status','=', 2)->count();
            }
            else {
                return $tickets = Tickets::where('id_product','=',$id)->where('id_status','=', 2)->count();
            }
        }        
    }
    /**
    * COUNT Ongoing ticket by id product
    */
    public static function countOngoingTicket($id) {
        $product = Products::find($id);
        if(Session::get('role') == 2) {
            if($product->id_parent == 0){
                $childs = Products::where('id_parent','=',$id)->get();
                $arr = array();
                array_push($arr, $id);
                foreach ($childs as $child) {
                    array_push($arr, $child->id);
                }
                return $tickets = Tickets::whereIn('id_product',$arr)->where('petugas','=',Session::get('name'))->where('id_status','!=', 2)->count();
            }else{
                return $tickets = Tickets::where('id_product','=',$id)->where('petugas','=',Session::get('name'))->where('id_status','!=', 2)->count();
            }            
        }
        else {
            if($product->id_parent == 0){
                $childs = Products::where('id_parent','=',$id)->get();
                $arr = array();
                array_push($arr, $id);
                foreach ($childs as $child) {
                    array_push($arr, $child->id);
                }
                return $tickets = Tickets::whereIn('id_product',$arr)->where('id_status','!=', 2)->count();
            }
            else {
                return $tickets = Tickets::where('id_product','=',$id)->where('id_status','!=', 2)->count();
            }
        }        
    }

    public static function getListPic() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
          $response_code = 200;
          $response_data = $response->json();
          $arr = array();
          for($i = 0; $i < count($response_data['message']); $i++) {
            if($response_data['message'][$i]['tiket_user_id'] == 2) {
                array_push($arr, $response_data['message'][$i]);
            }
          }
          return $arr;
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list PIC. Hubungi admin.');
        }
    }

    public static function getListCs() {
        $client = new GuzzleHttp\Client();
        $response = $client->get('http://test.kudoserver.com:24864/api/box/tiketSupport/getListPic');
        if ($response->getHeader('content-type') == 'application/json' && $response->getStatusCode() == '200') {
          $response_code = 200;
          $response_data = $response->json();
          $arr = array();
          for($i = 0; $i < count($response_data['message']); $i++) {
            if($response_data['message'][$i]['tiket_user_id'] == 3) {
                array_push($arr, $response_data['message'][$i]);
            }
          }
          return $arr;
        } 
        else {
          return Redirect::to('/')->with('flash_error', 'Error saat pengambilan list PIC. Hubungi admin.');
        }
    }
  
    public function edit($id) {
        $ticket = Tickets::find($id);
        $ticket->level = Input::get('level');
        $ticket->id_product = Input::get('product');
        $ticket->id_problem = Input::get('problem');
        $ticket->no_antrian = Input::get('noAntrian');
        $ticket->transaction_date = Input::get('tglTransaksi');   
        $ticket->last_update_time = Carbon::now(new DateTimeZone('Asia/Jakarta'));
        $ticket->last_update_by = Session::get('name');
        $ticket->resolution_time = Input::get('resolution_time');
        if($ticket->id_status==1){
            $ticket->id_status = 3;
        }
        if($ticket->save()){
            $url= url('/detail/'.$ticket->id);
            $messages = "Edit Ticket";
            foreach($ticket->cc as $ccc){
                 Mail::queue('emails.newUpdate', array('id_tiket'=>$ticket->id,'user'=>$ticket->last_update_by,'time'=>$ticket->last_update_time,'url'=>$url,'messages'=>$messages), function($message) use ($ccc, $ticket){
                    $message->to('ad_chadel@yahoo.com', $ccc->user_name)->subject('[#'.$ticket->id.' New Ticket Update]');//Kalo udah di deploy ad_chadel@yahoo.com diganti sama ccc->email_user
                });
            }
        }
        $message = 'Update Ticket Succes';
        return Redirect::to('/detail/'.$id)->with('flash_notice', $message);
    }

    public static function countMyTicket() {
        
        return $tickets = Tickets::where('petugas','=',Session::get('name'))->where('id_status','!=',2)->count();
    }

    // Testing
    public function tesInOrWhere() {
        $status = Status::whereIn('id',array(1,2))->lists('id');
        $status = Status::whereIn('id',$status)->get();
        foreach ($status as $stat) {
            switch ($stat->id) {
                case 1:
                    echo 'Admin';
                    break;
                case 2:
                    echo 'PIC';
                    break;
                case 3:
                    echo 'CS';
                    break;
                default:
                    echo 'Hah?';
                    break;
                echo '\n';
            }
        }
        echo 'End';
    }
	
	public function getTicketStatusJson() {
		return json_encode(array("unassigned"=>self::countUnassigned(),"unreplied"=>self::countUnreplied(), "replied"=>self::countReplied(),"closed"=>self::countClosed()));
	}
    /**
     * Get all 
     */
    public static function getTicketNoDuplicate($id) {
        $thisTicket = Tickets::find($id);
        $tickets = Tickets::where('duplicate_to','=',0)->where('id','!=',$id)->get();
        if($thisTicket->duplicate_to == 0) {
            echo '<option value="0" selected>No Duplicate</option>';
        }
        else {
            echo '<option value="0">No Duplicate</option>';
        }
        foreach ($tickets as $ticket) {
            if($thisTicket->duplicate_to == $ticket->id) {
                echo '<option value="'.$ticket->id.'" selected>Ticket #'.$ticket->id.' - '.substr($ticket->message, 0, 25).'</option>'; 
            }
            else {
                echo '<option value="'.$ticket->id.'">Ticket #'.$ticket->id.' - '.substr($ticket->message, 0, 25).'</option>'; 
            }
        }
    }
}