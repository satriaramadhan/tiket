<?php

/*
|--------------------------------------------------------------------------
| API Constant
|--------------------------------------------------------------------------
|
| Define all required constant for API here
|
*/
define('API_CODE', 'code');
define('API_MESSAGE', 'message');

// success code
define('API_GENERAL_SUCCESS', 1000);

// error code
define('API_ERROR_UNKNOWN', 2000);
define('API_ERROR_INVALID_SESSION', 2001);
define('API_ERROR_INVALID_USERNAME', 2002);
define('API_ERROR_INVALID_USERMAIL', 2003);
define('API_ERROR_INVALID_PASSWORD', 2004);
define('API_ERROR_VALIDATION', 2005);
define('API_ERROR_ACTION_FORBIDDEN', 2006);
define('API_ERROR_ID_UNAVAILABLE', 2007);
define('API_ERROR_ORDER_ITEM', 2008);
define('API_ERROR_RESOURCE_UNAVAILABLE', 2009);
define('API_ERROR_DEPOSIT_OVER_LIMIT', 2010);
define('API_ERROR_ORDER_NOT_REFUNDABLE', 2011);
define('API_ERROR_VENDOR_MAINTENANCE', 2012);
define('API_ERROR_OUT_OF_STOCK', 2013);
define('API_ERROR_PRICE', 2014);
define('API_ERROR_USERMAIL_NOT_ACTIVE', 2015);
define('API_ERROR_USERMAIL_ALLOCATION', 2016);
define('API_ERROR_EMPTY_NEWPASSWORD', 2017);


class APIHelper
{

    public static function randomPassword()
    {
        $alphabet = "0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public static function XOR_encrypt($message, $key)
    {
        $ml = strlen($message);
        $kl = strlen($key);
        $newmsg = "";

        for ($i = 0; $i < $ml; $i++) {
            $newmsg = $newmsg . ($message[$i] ^ $key[$i % $kl]);
        }

        return base64_encode($newmsg);
    }

    public static function XOR_decrypt($encrypted_message, $key)
    {
        $msg = base64_decode($encrypted_message);
        $ml = strlen($msg);
        $kl = strlen($key);
        $newmsg = "";

        for ($i = 0; $i < $ml; $i++) {
            $newmsg = $newmsg . ($msg[$i] ^ $key[$i % $kl]);
        }

        return $newmsg;
    }

    /**
     * return JSON response.
     * @param int $status
     * @param array $data
     * @param null $error_code
     * @param array $headers
     * @return mixed
     */
    public static function response($status = 200, $data = array(), $error_code = null, array $headers = array())
    {
        // check wheter status is not OK
        if ($status != 200) {
            // assign error code and message
            if (isset($error_code)) {
                $result[API_CODE] = $error_code;
                $result[API_MESSAGE] = static::requestErrorReadableFormat($error_code);
            } else {
                $result[API_CODE] = API_ERROR_UNKNOWN;
                $result[API_MESSAGE] = static::requestStatus($status);
            }

            // override error message if there is available error message
            if (count($data) > 0) {
                $result[API_MESSAGE] = $data;
            }
        } else {
            // check whether code is not null
            if ($error_code == null) {
                $result[API_CODE] = API_GENERAL_SUCCESS;
                $result[API_MESSAGE] = $data;
            } else {
                $result[API_CODE] = $error_code;
                $result[API_MESSAGE] = static::requestErrorReadableFormat($error_code);
            }

            // override error message if there is available error message
            if (count($data) > 0) {
                $result[API_MESSAGE] = $data;
            }
        }

        return Response::json($result, $status, $headers);
    }

    /**
     * request readable message error based on error code
     * @param  error_code $error error code constant
     * @return message error message in readable format
     */
    public static function requestErrorReadableFormat($error)
    {
        $errorMessage = array(
		API_ERROR_UNKNOWN => 'Unknown error.',
		API_ERROR_INVALID_SESSION => 'Invalid token, make sure you send a valid token.',
		API_ERROR_INVALID_USERNAME => 'Invalid username.',
		API_ERROR_INVALID_USERMAIL => 'Invalid usermail.',
		API_ERROR_INVALID_PASSWORD => 'Invalid user password.',
		API_ERROR_VALIDATION => 'Error validation.',
		API_ERROR_ACTION_FORBIDDEN => 'Action forbidden.',
		API_ERROR_ID_UNAVAILABLE => 'Given field id is unavailable.',
		API_ERROR_ORDER_ITEM => 'Order Item Failed.',
		API_ERROR_RESOURCE_UNAVAILABLE => 'Resource unavailable.',
		API_ERROR_DEPOSIT_OVER_LIMIT => 'Cashier Deposit Over Limit',
		API_ERROR_ORDER_NOT_REFUNDABLE => 'Order Item is not refundable',
		API_ERROR_VENDOR_MAINTENANCE => 'Merchants Under Manitenance',
		API_ERROR_PRICE => 'Price Do Not Match',
		API_ERROR_OUT_OF_STOCK => 'Out Of Stock',
		API_ERROR_USERMAIL_NOT_ACTIVE => 'Email Not Active',
		API_ERROR_USERMAIL_ALLOCATION => 'Account do not have allocation',
		API_ERROR_EMPTY_NEWPASSWORD => 'Please Input New Password');
		return ($errorMessage[$error]) ? $errorMessage[$error] : $errorMessage[100];
    }

    /**
     * request readable header status format
     * @param  header_status $code error code constant
     * @return message header status message in readable format
     */
    public static function requestStatus($code)
    {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request. Potentially request format malformed',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$code]) ? $status[$code] : $status[500];
    }

    public static function requestVendorCodes($vendor_id)
    {
        $vendor_codes = array(
            9 => 1, // indomog
            10 => 2, // groupon
            11 => 3, // fastpay
            13 => 4, // ukrida
            14 => 5, // tripvisto
            15 => 6, // datacell
            16 => 7, // MKM
            17 => 8, // berrybenka
            18 => 9, // pesona
            19 => 10, // shopdeca
            20 => 11, // multifinance
            21 => 12, // finnet
            22 => 13, // PLN
            23 => 14, // Bilna
            24 => 15, // Bolt
            25 => 16, // Bukalapak
            26 => 17, // Hijabenka
            27 => 18, // Tiket Event
            28 => 19, // VIP Plaza
            29 => 20, // Sociolla
            30 => 21, // Bali Unite
            31 => 22, // Sale Stock
            32 => 23); // Tiket Eratel

        try {
            return ($vendor_codes[$vendor_id]) ? $vendor_codes[$vendor_id] : 0;
        } catch (Exception $e) {
            return 0;
        }
    }
}
