@extends('layout')
@section('menu')
@if (Auth::guest()==true)
@elseif (Auth::user()->role->name=='Admin')
<li class="divider-vertical">
    <a href="{{url('/users')}}">Daftar User</a>
  </li>
  @else
   <li role="presentation"><a href="{{url('')}}">Daftar Tiket</a></li>
   @endif
   @stop
@section('konten')
    <h1>Home page</h1>
    <p>Current time: {{ date('F j, Y, g:i A') }}  </p>
@stop