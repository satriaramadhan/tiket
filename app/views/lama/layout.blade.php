<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Kudo Ticketing System</title>

    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('css/jumbotron-narrow.css')}}" rel="stylesheet">
    <style type="text/css">
      @import url(//fonts.googleapis.com/css?family=Lato:700);
      h3 {
        font-family:'Lato', sans-serif;
        color: #999;
      }
      h4 {
        font-family:'Lato', sans-serif;
      }
      h3 a {
        color: #999;
      }
      h3 a:hover {
        color: #337AB7;
        text-decoration: none;
      }
    </style>

  </head>

  <body>
    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li>{{url('')}}</li>
                @if(Auth::check())
                    <li>{{url('/home/profile')}}</li>
                    <li>{{url('/logout/'.Auth::user()->id.'')}}</li>
                @else
                    <li>{{url('login')}}</li>
                @endif
            @yield('menu')
          </ul>
        </nav>
         <h3><a href="{{url('')}}"><img src="{{url('img/kudo.png')}}"></a></h3>
      </div>

      @yield('konten')

      <footer class="footer">
        <p>&copy; Kudo Ticketing System 2015</p>
      </footer>

    </div> <!-- /container -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    
  </body>
</html>