@extends('agen.layout')
@section('konten')
    <style type="text/css">
        .header {
            text-align: center;
        }
        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0px auto;
        }
    </style>
        @if (Session::has('flash_error'))
            <div id="flash_error" style="text-align:center" class="bg-danger">
                <div style="padding:10px; font-weight:bold;">
                    {{ Session::get('flash_error') }}
                </div>
            </div>
        @endif
      <div class="form">
        {{ Form::open(array('login','POST','class'=>'form-signin')) }}
          <fieldset>
            <h3 style="text-align:center"><label class="control-label">Kudo Ticketing System<br></label><h3>
            <div class="form-group">
                <input class="form-control" name="email" placeholder="name@domain.com" type="email">
            </div>
            <div class="form-group">
                <input class="form-control" name="password" placeholder="password" type="password">
            </div>
            <div class="form-group" id="submitbutton" style="text-align:center">
                <button type="submit" class="btn btn-primary">Daftar</button>
            </div>
          </fieldset>
        </form>
      </div>

@stop
