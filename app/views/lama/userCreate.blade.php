@extends('dashboard.layout')
@section('konten')

<h1>Create User</h1>
<div class="form">
{{ Form::open(array('route' => 'users.store','class'=>'form-horizontal')) }}
    <fieldset>
        <div class="form-group @if ($errors->has('name')) has-error @endif">
            <div class="col-lg-3 control-label">
                {{ Form::label('name', 'Name:') }}
            </div>
            <div class="col-lg-6">
                {{ Form::text('name') }}
                @if ($errors->has('name')) <p class="help-block">{{$errors->first('name')}} @endif</p>
            </div>
        </div>
        <div class="form-group @if ($errors->has('password')) has-error @endif">
            <div class="col-lg-3 control-label">
                {{ Form::label('password', 'Password:') }}
            </div>
            <div class="col-lg-9">
                {{ Form::password('password') }}
                @if ($errors->has('password')) <p class="help-block">{{$errors->first('password')}} @endif</p>
            </div>
        </div>
        <div class="form-group @if ($errors->has('password_confirmation')) has-error @endif">
            <div class="col-lg-3 control-label">
                {{ Form::label('password', 'Confirm Password:') }}
            </div>
            <div class="col-lg-9">
                {{ Form::password('password_confirmation') }}
                @if ($errors->has('password_confirmation')) <p class="help-block">{{$errors->first('password_confirmation')}} @endif</p>
            </div>
        </div>
        <div class="form-group @if ($errors->has('email')) has-error @endif">
            <div class="col-lg-3 control-label">
                {{ Form::label('email', 'Email:') }}
            </div>
            <div class="col-lg-9">
                {{ Form::text('email') }}
                @if ($errors->has('email')) <p class="help-block">{{$errors->first('email')}} @endif</p>
            </div>
        </div>
         <div class="form-group">
            <div class="col-lg-3 control-label">
                {{ Form::label('id_role', 'Nama Role:') }}
            </div>
            <div class="col-lg-9">
                {{ Form::select('id_role',$role) }}
            </div>
        </div> 
        <div class="form-group" id="submitbutton">
            {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
        </div>
    </fieldset>
{{ Form::close() }}
</div>

@stop