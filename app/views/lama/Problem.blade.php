@extends('dashboard.layout')

@section('konten')
      <div class="form">
      	<h2 class="sub-header">{{$product->name}}</h2>
    	<table class="table table-hover">
			<thead>
				<th>Problem</th>
				<th>Action</th>
				<th></th>
			</thead>
			@foreach ($problems as $problem)
                <tr>
				  <td>{{ $problem->name}}</a></td>				  
				  <td><a href="{{url('product/'.$product->id.'/editProblem/'.$problem->id)}}"><button type="submit" class="btn btn-info pull-left">Edit</button></a></td>
				  <td><a href="{{url('product/'.$product->id.'/deleteProblem/'.$problem->id)}}"><button type="submit" class="btn btn-danger pull-left">Delete</button></a></td>
				</tr>
			@endforeach
		</table>
		<hr>
		<div class="form-group" id="submitbutton" style="padding-bottom:13px">
	        <a href="{{url('product/'.$product->id.'/createProblem')}}"><button type="submit" class="btn btn-primary pull-left">Tambah Problem</button></a>
	    </div>
	  </div>
@stop
