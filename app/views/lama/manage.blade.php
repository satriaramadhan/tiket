@extends('dashboard.layout')

@section('konten')

<h1>Edit Profile</h1>
<div class="form">
{{ Form::model($user, array('method' => 'PATCH', 'route' =>
 array('users.update', $user->id),'class'=>'form-horizontal')) }}    
 <fieldset>
        <div class="form-group">
            <div class="col-lg-3 control-label">
            {{ Form::label('name', 'Name:') }}
        </div>
        <div class="col-lg-9">
            {{ Form::text('name') }}
        </div>
        </div>
        <div class="form-group">
             <div class="col-lg-3 control-label">
            {{ Form::label('password', 'Password:') }}
        </div>
        <div class="col-lg-9">
            {{ Form::password('password') }}
        </div>
        </div>
        <div class="form-group">
            <div class="col-lg-3 control-label">
            {{ Form::label('password', 'Confirm Password:') }}
        </div>
        <div class="col-lg-9">
            {{ Form::password('password_confirmation') }}
        </div>
        </div>
        <div class="form-group">
            <div class="col-lg-3 control-label">
            {{ Form::label('email', 'Email:') }}
        </div>
        <div class="col-lg-9">
            {{ Form::text('email') }}
        </div>
        </div>
         <div class="form-group">
            <div class="col-lg-3 control-label">
                 {{ Form::label('id_role', 'Nama Role:') }}
                  </div>
                <div class="col-lg-9">
            {{ Form::select('id_role',$role) }}
        </div>
        </div> 
        <div class="form-group" id="submitbutton">
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            <a class="btn btn-default" href="../profile" role="button">Cancel</a>
        </div>
    </fieldset>
{{ Form::close() }}
</div>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop