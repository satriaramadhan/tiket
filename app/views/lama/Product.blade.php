@extends('dashboard.layout')

@section('konten')
      <div class="form">
      	<h2 class="sub-header">Daftar Product</h2>
    	<table class="table table-hover">
			<thead>
				<th>Product</th>
				<th>Type</th>
				<th>Nama PIC</th>
				<th>Last Update By</th>
				<th>Action</th>
				<th></th>
			</thead>
			@foreach ($products as $product)
                <tr onclick="location.href='{{url('product/'.$product->id)}}'" style="cursor:pointer">
				  <td style="color: #337ab7">{{ $product->name}}</td>
				  <td>{{ $product->parent->name or $product->name}}</td>
				  <td>@foreach($product->pic as $pic)
				  {{$pic->pic->name or 1}}
				  @endforeach </td>
				  <td>{{ $product->user->name or "admin"}}</td>
				  <td><a href="{{url('product/'.$product->id.'/editProduct')}}"><button type="submit" class="btn btn-info pull-left">Edit</button></a></td>
				  <td><a href="{{url('product/'.$product->id.'/deleteProduct')}}"><button type="submit" class="btn btn-danger pull-left">Delete</button></a></td>
				</tr>
			@endforeach
		</table>
		<hr>
		<div class="form-group" id="submitbutton" style="padding-bottom:13px">
	        <a href="{{url('createProduct')}}"><button type="submit" class="btn btn-primary pull-left">Tambah Product</button></a>
	    </div>
	  </div>
@stop
