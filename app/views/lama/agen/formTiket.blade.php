@extends('agen.layout')
@section('menu')
    <li role="presentation"><a href="{{url('')}}">Daftar Tiket</a></li>
	<li role="presentation" class="active"><a href="{{url('lapor')}}">Lapor</a></li>
@stop
@section('konten')

      <div class="form">
      	{{ Form::open( array('action'=>'ActionController@createNewTicket','method'=>'post','class'=>'form-horizontal', 'files' => true)) }}
		  <fieldset>
		    <div class="form-group">
		      <label for="idAgen" class="col-lg-3 control-label">ID Agen</label>
		      <div class="col-lg-9">
		        <input class="form-control" name="idAgen" placeholder="Agen123" type="text">
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="tglTransaksi" class="col-lg-3 control-label">Tanggal Transaksi</label>
		      <div class="col-lg-9">
		        <input class="form-control" name="tglTransaksi" type="text" placeholder="DD-MM-YYYY">
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="noAntrian" class="col-lg-3 control-label">Nomor Antrian</label>
		      <div class="col-lg-9">
		        <input class="form-control" name="noAntrian" placeholder="123" type="text">
		      </div>
		    </div>
		    <hr>
		    <div class="form-group">
		      <label for="produk" class="col-lg-3 control-label">Produk</label>
		      <div class="col-lg-9">
		        <select class="form-control" name="produk">
				  <option value="1">Pulsa</option>
				  <option value="2">Bilna</option>
				  <option value="3">Fashion</option>
				  <option value="4">Voucher</option>
				</select>
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="subject" class="col-lg-3 control-label">Subject</label>
		      <div class="col-lg-9">
		        <input class="form-control" name="subject" placeholder="Pulsa tidak masuk" type="text">
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="deskripsi" class="col-lg-3 control-label">Deskripsi</label>
		      <div class="col-lg-9">
		        <textarea class="form-control" rows="3" name="deskripsi"></textarea>
		        <span class="help-block">Deskripsikan masalah yang anda temukan dalam bertransaksi.</span>
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="attachment" class="col-lg-3 control-label">Lampiran</label>
		      <div class="col-lg-9">
		        <input name="attachment" type="file">
		      </div>
		    </div>
		    <div class="form-group" id="submitbutton">
		        <button type="reset" class="btn btn-default">Cancel</button>
		        <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		  </fieldset>
		</form>
      </div>

@stop
