@extends('dashboard.layout')
@section('konten')
	<hr>
      <div class="form" id="detail">
      	<div class="tiket">
	      	<div class="subject">
		      	@if($ticket->id_status!=2)
	      			<h4 style="padding-bottom:10px">
	      				[#{{$ticket->id}}] {{$ticket->subject}}
	      				<label style="color:green">({{$ticket->status->name}})</label>
				    	<button data-toggle="modal" data-target="#setclosed" class="btn btn-danger pull-right">Set As Closed</button>
				    </h4>
				    <form method="post" action="{{url('setClosed')}}">
				   	  <input type="hidden" name="id_tiket" value="{{$ticket->id}}">
                      <div class="modal fade" id="setclosed" tabindex="-1" role="dialog" aria-labelledby="setclosed" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-body">
                              <h3 id="done">Set status tiket #{{$ticket->id}} menjadi closed ?</h3>
                              <br>
                              <div class="lead" id="btn_modaldelete">
                                <button type="submit" class="btn btn-danger" id="btn-save">Yes</button>
                                <button type="close" class="btn" id="btn-close" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                         </div>
                        </div>
                      </div>
                    </form>
				    <form action="{{url('setPetugas')}}" method="post" class="form-horizontal">
		      			<input type="hidden" name="id_tiket" value="{{$ticket->id}}">
		      			 <div class="form-group">
							      <label for="petugas" class="col-lg-4">Assign to (Petugas saat ini: {{$ticket->petugas}})</label>
							      <div class="col-lg-2">
							        <select name="petugas" class="selectpicker form-control">
							        @foreach($petugas as $petugas)
					                  <option value="{{$petugas->name}}">{{$petugas->name}}</option>         
					                @endforeach
					                </select>
							      </div>
					    	<button type="submit" class="btn btn-danger">Assign</button>
					    </h4>
				    </form>
				@else 		
		      		<h4 style="padding-bottom:10px">
		      			[#{{$ticket->id}}] {{$ticket->subject}}
		      			<label style="color:red">({{$ticket->status->name}})</label>
		      		</h4>
		      	@endif	
	      	</div>
	      	<hr>
	      	<div class="detail-tiket">
	      		<label style="text-align:left" class="col-lg-3">{{$ticket->user->name}}</label>
	      		<label style="text-align:right" class="col-lg-9">{{$ticket->created_date}}</label>
	      	</div> 
	      	<div class="konten">
	      		<p>
	      			{{$ticket->message}}
	      		</p>
	      	</div>
	      	<div class="attachment-tiket">
		      	@if($ticket->attachment != null)
		      		<img src="{{url($ticket->attachment->path)}}">
		      	@else 
		      		hahaha
		      	@endif
	      	</div>
      	</div>
      	<div class="komentar">
      	@foreach ($comments as $comment)
	      	<hr>
      		<div class="detail-komentar">
	      		<label style="text-align:left; padding-left:0px" class="col-lg-6">
	      			<?php 
	      				if($comment->user->id_role == 3) echo '[Customer Service]';
	      			?>
	      			{{$comment->user->name}}
	      		</label>
	      		<label style="text-align:right" class="col-lg-6">{{$comment->date}}</label>
	      	</div> 
	      	<div class="isi-komentar">
	      		<p>{{$comment->message}}</p>
	      	</div>
	      	<div class="attachment">
		      	@if($comment->attachment != null)
		      		<img src="{{url($comment->attachment->path)}}">
		      	@endif
	      	</div>
	    @endforeach
      	</div>
      	@if($ticket->id_status!=2)
      	<hr>
      	<div class="form-komentar">
      		{{ Form::open( array('route'=>'komentari','method'=>'post','class'=>'form-horizontal', 'files' => true)) }}
				<label class="control=label">Balas pesan</label>
	      		<textarea class="form-control" rows="3" name="komentar" placeholder="Balas pesan ini . . ."></textarea>
	      		<div class="form-group" style="padding-top: 10px; margin-left:0px;">
			      <label class="col-lg-3 control-label" style="text-align:left">Tambahkan lampiran</label>
			      <div class="col-lg-9">
			      	<?php echo Form::file('attachment');?>
			      </div>
			    </div>
			    <input name="id_ticket" type="hidden" value="{{$ticket->id}}">
			    <input name="id_user" type="hidden" value="{{Auth::user()->id}}">
			    <input name="type" type="hidden" value="2">
			    <div class="form-group" style="padding-right:16px" id="submitbutton">
			        <button type="submit" class="btn btn-primary">Balas</button>
			    </div>
		    </form>
      	</div>
      	@endif
      </div>
@stop
