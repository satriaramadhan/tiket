@extends('agen.layout')
@section('menu')
	<li role="presentation" class="active"><a href="{{url('')}}">Daftar Tiket</a></li>
    <li role="presentation"><a href="{{url('lapor')}}">Lapor</a></li>
@stop
@section('konten')
      <div class="form">
      	<div class="dropdown">
		  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" 
		  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    Pilih Status
		    <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		    <li><a href="#">Open</a></li>
		    <li><a href="#">Ongoing</a></li>
		    <li><a href="#">Closed</a></li>
		  </ul>
		</div>
		
    	<table class="table table-hover">
			<tr>
			  <td>No. Tiket</td>
			  <td>Subject</td>
			  <td>Produk</td>
			  <td>Status</td>
			  <td>Last Update Time</td>
			  <td>Last Update By</td>
			  <td>Detail</td>
			</tr>
			@foreach ($tickets as $ticket)
                <tr onclick="location.href='{{url('detail/'.$ticket->id)}}'" style="cursor:pointer">
				  <td style="text-align:center">#{{ $ticket->id }}</td>
				  <td>{{ $ticket->subject }}</td>
				  <td>{{ $ticket->product->name }}</td>
				  <td>{{ $ticket->status->name }}</td>
				  <td>{{ $ticket->last_update_time }}</td>
				  <td>{{ $ticket->last_update_by }}</td>
				  <td><a href="{{url('detail/'.$ticket->id)}}">detail</a></td>
				</tr>
			@endforeach
		</table>
		<hr>
		<div class="form-group" id="submitbutton" style="padding-bottom:13px">
	        <a href="{{url('lapor')}}"><button type="submit" class="btn btn-primary pull-left">Tambah Tiket</button></a>
	    </div>
	  </div>
@stop
