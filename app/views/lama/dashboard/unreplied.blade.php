@if(!Request::ajax())
@extends('dashboard.layout')
@section('konten')
@endif
<h2 class="sub-header">Daftar Unreplied  Tiket</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>No. Tiket</th>
                  <th>Nama Agen</th>
                  <th>Subject</th>
                  <th>Produk</th>
                  <th>Status</th>
                  <th>Tanggal Masuk</th>
                  <th>Assigned to</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
               @foreach ($tickets as $ticket)
                <tr onclick="location.href='{{url('detail/'.$ticket->id)}}'" style="cursor:pointer">
				  <td style="text-align:center">#{{ $ticket->id }}</td>
				  <td>{{ $ticket->user->name}}</td>
				  <td>{{ $ticket->subject }}</td>
				  <td>{{ $ticket->product->name }}</td>
				  <td>{{ $ticket->status->name}}</td>
				  <td>{{ $ticket->created_date}}</td>
          @if($ticket->petugas != NULL)
          <td>{{ $ticket->petugas}}</td>
          @else
          <td>Unassigned</td>
          @endif
				  <td><a href="{{url('detail/'.$ticket->id)}}"><button>detail</button></a></td>
				</tr>
			@endforeach
              </tbody>
            </table>
          </div>
@if(!Request::ajax())
@stop
@endif