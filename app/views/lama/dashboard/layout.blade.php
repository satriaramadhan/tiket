<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Kudo Ticketing System</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/dashboard.css')}}" rel="stylesheet">
    <link href="{{url('css/hover.css')}}" rel="stylesheet" media="all">
   
    <link rel="STYLESHEET" type="text/css" href="{{url('codebase/dhtmlx.css')}}">


  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{url('')}}">Kudo Ticketing System</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{url('')}}"><span class="glyphicon glyphicon-signal" aria-hidden="true"></span></a></li>
            <li><a href="{{url('home/profile/manage')}}"> <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a></li>
            <li><a href="{{url('home/profile')}}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
            <li><a href="#"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></li>
            <li><a href="{{url('logout')}}"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
          </ul>
          <ul class="nav navbar-nav navbar-left">
            @if(Auth::user()->id_role==1)
              <li><a href="{{url('users')}}">Manage User</a></li>
              <li><a href="{{url('role')}}">Manage Role</a></li>
              <li><a href="{{url('product')}}">Manage Product</a></li>
            @endif
            <li><a href="{{url('dashboard/myticket')}}">My Ticket</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Cari Tiket">
          </form>
        </div>
      </div>
    </nav>

    <style type="text/css">
      li.list-group-item:hover {
        cursor: pointer;
      }
    </style>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar" style="background-color:white">
          <?php $classView=new ViewController();?>
            <?php $count1=$classView::countUnassigned();?>
            <?php $count2=$classView::countUnreplied();?>
            <?php $count3=$classView::countReplied();?>
            <?php $count4=$classView::countClosed();?>
          <div><label style="margin-left:10px">TICKETS <span class="badge pull-right" style="margin-left:82px">{{$count1+$count2+$count3+$count4}}</span></label></div>
          <ul class="list-group">
            <li class="list-group-item hvr-fade" onclick="unassigned()"><span class="badge pull-right" style="background-color:#d9534f">{{$count1}}</span>Unassigned</li>
            <li class="list-group-item hvr-fade" onclick="unreplied()"><span class="badge pull-right">{{$count2}}</span>Unreplied</li>
            <li class="list-group-item hvr-fade" onclick="replied()"><span class="badge pull-right">{{$count3}}</span>Replied</li>
            <li class="list-group-item hvr-fade" onclick="closed()"><span class="badge pull-right">{{$count4}}</span>Closed</li>
          </ul>
          <div><label style="margin-left:10px">KUDO TEAM</label></div>
          <ul class="list-group">
            <?php $data_user=$classView::listUser();?>
            @foreach($data_user as $data)
              <li class="list-group-item hvr-fade" onclick="petugas('{{$data->name}}')"><span class="badge pull-right">{{$data->user_count}}</span>{{$data->name}}</li>
            @endforeach
          </ul>
          <div><label style="margin-left:10px">PRODUCTS</label></div>
          <ul class="list-group">
             <?php $data_product=$classView::listProduct();?>
              @foreach($data_product as $datap)
                <li class="list-group-item hvr-fade" onclick="product('{{$datap->id}}')"><span class="badge pull-right">{{$datap->product_count}}</span>{{$datap->name}}</a></li>
              @endforeach
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" id='pageload'>
        @if(Session::has('flash_message'))
        <div class="alert-box success">
          <h2>{{ Session::get('flash_message') }}</h2>
        </div>
        @endif
          @yield('konten')
          <footer class="footer">
            <div class="container">
              <p class="text-muted">&copy; Kudo Ticketing System 2015</p>
            </div>
          </footer>
        </div>
      </div>
    </div>
        
    
    

  </body>
</html>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>
      function petugas(nama) {
        $("#pageload").load('{{url("dashboard/petugas")}}/'+nama);
        window.history.pushState("object or string", "Title", "{{url("dashboard/petugas")}}/"+nama);
      };

      function product(id) {
        $("#pageload").load('{{url("dashboard/product")}}/'+id);
        window.history.pushState("object or string", "Title", "{{url("dashboard/product")}}/"+id);
      };

      function unassigned() {
        $("#pageload").load('{{url("dashboard/unassigned")}}');
        window.history.pushState("object or string", "Title", "{{url("dashboard/unassigned")}}");
      };

      function unreplied() {
        $("#pageload").load('{{url("dashboard/unreplied")}}');
        window.history.pushState("object or string", "Title", "{{url("dashboard/unreplied")}}");
      };

      function replied() {
        $("#pageload").load('{{url("dashboard/replied")}}');
        window.history.pushState("object or string", "Title", "{{url("dashboard/replied")}}");
      };

      function closed() {
        $("#pageload").load('{{url("dashboard/closed")}}');
        window.history.pushState("object or string", "Title", "{{url("dashboard/closed")}}");
      };

    </script>
     <script src="{{url('js/ie-emulation-modes-warning.js')}}"></script>

    <script src="{{url('codebase/dhtmlx.js')}}" type="text/javascript"></script>
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script>
      function petugas(name) {
        $("#pageload").load('{{url("dashboard/petugas")}}/'+name);
      }
      function product(id)
    </script>
  </body>
</html>
