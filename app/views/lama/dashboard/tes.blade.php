<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{url('favicon.ico')}}">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/dashboard.css')}}" rel="stylesheet">

    <!-- Javascript and CSS for dhtmlx -->
    <script type="text/javascript" src="codebase/dhtmlx.js"></script>
	<link rel="stylesheet" type="text/css" href="codebase/dhtmlx.css">
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Kudo Ticketing System</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar" style="background-color:white">
          <div><label style="margin-left:10px">TICKETS</label></div>
          <ul class="list-group">
            <li class="list-group-item"><span class="badge pull-right">14</span>Unassigned</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Unreplied</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Replied</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Closed</li>
          </ul>
          <div><label style="margin-left:10px">KUDO TEAM</label></div>
          <ul class="list-group">
            <li class="list-group-item"><span class="badge pull-right">14</span>Remon</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Adlian</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Satria</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Devakto</li>
          </ul>
          <div><label style="margin-left:10px">PRODUCTS</label></div>
          <ul class="list-group">
            <li class="list-group-item"><span class="badge pull-right">14</span>Pulsa</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Bilna</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Voucher</li>
            <li class="list-group-item"><span class="badge pull-right">14</span>Fashion</li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" id="r-col" style="height:400px;">
        </div>
        <script type="text/javascript">
      		mygrid = new dhtmlXGridObject('r-col');
          mygrid.setImagePath("codebase/imgs/");
          mygrid.setHeader("Sales,Book title,Author,Price");
          mygrid.setInitWidths("100,250,150,100");
          mygrid.setColAlign("right,left,left,left"); 
          mygrid.setColTypes("ro,ed,ed,ed");   
          mygrid.setColSorting("int,str,str,int");  
          mygrid.init();
    	</script>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="{{url('js/vendor/holder.min.js')}}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
  </body>
</html>
