<!doctype html>
<html>
    <head>
       <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Kudo Ticketing System</title>
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap
/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
        <style>
            table form { margin-bottom: 0; }
            form ul { margin-left: 0; list-style: none; }
            .error { color: red; font-style: italic; }
            body { padding-top: 20px; }
        </style>
    </head>

    <body>
            <div class="page-header">
      <h3><a href="{{url('')}}">Kudo Ticketing System</a></h3>
    </div>
        
        <div class="container">
             <div class="header clearfix">
        <div class="navbar">
        <div class="navbar-inner">
          <ul class="nav">
            <li>{{ link_to_route('home', 'Home') }}</li>
                @if(Auth::check())
                    <li>{{ link_to_route('profile', 'Profile' ) }}</li>
                    <li>{{ link_to_route('logout', 'Logout ('.Auth::user()->name.')') }}</li>
                @else
                    <li>{{ link_to_route('login', 'Login') }}</li>
                @endif
            @yield('menu')
          </ul>
        </div>
        </div
        
      </div>
            @if(Session::has('flash_notice'))
            <div id="flash_notice">{{ Session::get('flash_notice') }}</div>
            @endif

            @yield('konten')
            <footer class="footer">
        <p>&copy; Kudo Ticketing System 2015</p>
      </footer>
        </div>

    </body>

</html>