@extends('dashboard.layout')

@section('konten')
  <div class="panel panel-primary">
  	<div class="panel-heading"><strong>User Information</strong></div>
  	<table class="table table-hover">
			<tr>
			  <th>Nama: </th>
			  <td>{{Auth::user()->name}}</td>
			</tr>
			<tr>
			  <th>Email: </th>
			  <td>{{Auth::user()->email}}</td>
			</tr>
			<tr>
			  <th>Role: </th>
			  <td>{{Auth::user()->role->name}}</td>
			</tr>
			
		</table>
	</br>
		<td><a class="btn btn-default" href="profile/manage" role="button">Edit Profile</a></td>
  </div>
@stop