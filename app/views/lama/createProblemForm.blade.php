@extends('dashboard.layout')

@section('konten')

      <div class="form">
      	<form class="form-horizontal" method="POST" action="{{action ('ProductController@createProblem')}}">
		  	<h1>Tambah Problem {{$product->name}}</h1>
		  	<input type="hidden" name="product_id" value="{{$product->id}}">
			@if ($product->id_parent == 0)
		  		<input type="hidden" name="id_parent" value="{{$product->id}}">
		    @endif

		  <fieldset>
		    <div class="form-group @if ($errors->has('problemName')) has-error @endif">
		      <label for="namaProblem" class="col-lg-2 pull-left control-label">Nama Problem</label>
		      <div class="col-lg-6">
		        <input class="form-control" id="namaProblem" name="problemName" placeholder="(ex : Barang Rusak)" type="text">
		        @if ($errors->has('problemName')) <p class="help-block">{{$errors->first('problemName')}} @endif</p>
		      </div>
		    </div>
		    <hr>		    
		    <div class="form-group" id="submitbutton">		        
		        <button type="submit" class="btn btn-primary">Tambah</button>
		    </div>
		  </fieldset>
		</form>
      </div>
@stop
