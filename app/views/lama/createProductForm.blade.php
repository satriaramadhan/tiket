@extends('dashboard.layout')

@section('konten')

      <div class="form">
      	<form class="form-horizontal" method="POST" action="{{action ('ProductController@createProduct')}}">
		  <h1>Tambah Produk Baru</h1>
		  <fieldset>
		    <div class="form-group @if ($errors->has('productName')) has-error @endif">
		      <label for="productName" class="col-lg-2 pull-left control-label">Nama Produk</label>
		      <div class="col-lg-6">
		        <input class="form-control" id="productName" name="productName" placeholder="(ex : Bilna)" type="text">
		        @if ($errors->has('productName')) <p class="help-block">{{$errors->first('productName')}} @endif</p>
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="picName" class="col-lg-2 control-label">Nama PIC Produk</label>
		      <div class="col-lg-6">
		        <select id="picName" name="id_pic" class="selectpicker form-control">
		        @foreach($pics as $pic)
                  <option value="{{$pic->id}}">{{$pic->name}}</option>         
                @endforeach
                </select>
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="parent" class="col-lg-2 control-label">Jenis Produk</label>
		      <div class="col-lg-6">
		        <select  id="parent" name="id_parent" class="selectpicker form-control">
		        <option value="0">Parent</option>
		        @foreach($parent as $parent)
                  <option value="{{$parent->id}}">{{$parent->name}}</option>         
                @endforeach
                </select>
		      </div>
		    </div>
		    <hr>		    
		    <div class="form-group" id="submitbutton">		        
		        <button type="submit" class="btn btn-primary">Tambah</button>
		    </div>
		  </fieldset>
		</form>
      </div>

@stop
