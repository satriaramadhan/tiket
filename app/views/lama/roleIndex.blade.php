@extends('dashboard.layout')

@section('konten')

<h2 class="sub-header">Semua Role</h2>

<table class="table table-hover">
			<thead>
			<tr>
			  <th>Role Name</th>
			  <th>Action</th>
			  <th></th>
			</tr>
			</thead>
			@foreach ($roles as $roles)
			<tbody>
                <tr>
				  <td>{{ $roles->name }}</td>
				  <td>{{ link_to_route('role.edit', 'Edit', array($roles->id), array('class' => 'btn btn-info')) }}</td>
                  <td>{{ Form::open(array('method' => 'DELETE', 'route' => array('role.destroy', $roles->id))) }}                       
                      {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                      {{ Form::close() }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<div class="form-group" id="submitbutton" style="padding-bottom:13px">
	        <a href="{{url('/role/create')}}"><button type="submit" class="btn btn-primary pull-left">Tambah Role</button></a>
	    </div>
	  </div>

@stop