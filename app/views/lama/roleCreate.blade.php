@extends('dashboard.layout')
@section('konten')
    <h1>Create Role</h1>
    <div class="form">
    {{ Form::open(array('route' => 'role.store')) }}
        <fieldset>
            <div class="form-group @if ($errors->has('name')) has-error @endif">
                <div class="col-lg-2 pull-left control-label">   
                    {{ Form::label('name', 'Name:') }}
                </div>
                <div class="col-lg-9">
                    {{ Form::text('name') }}
                    @if ($errors->has('name')) <p class="help-block">{{$errors->first('name')}} @endif</p>
                </div>
            </div>
        <div class="form-group" id="submitbutton">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
        </div>  
        </fieldset> 
    {{ Form::close() }}
    </div>
@stop