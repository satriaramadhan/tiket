@extends('dashboard.layout')

@section('konten')

      <div class="form">
      	<form class="form-horizontal" method="POST" action="{{url('editProduct')}}">
		  <h1>Edit Produk </h1>
		  <fieldset>
		  <input type="hidden" name="id" value="{{$product->id}}">
		  
		    <div class="form-group @if ($errors->has('productName')) has-error @endif">
		      <label for="idAgen" class="col-lg-2 pull-left control-label">Nama Produk</label>
		      <div class="col-lg-6">
		        <input class="form-control" name="productName" value="{{$product->name}}" type="text">
		        @if ($errors->has('productName')) <p class="help-block">{{$errors->first('productName')}} @endif</p>
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="idPic" class="col-lg-2 control-label">Nama PIC Produk</label>
		      <div class="col-lg-6">
		        <select name="id_pic" class="selectpicker form-control">
		        @foreach($pics as $pic)
                  <option value="{{$pic->id}}" @if ($pic_product->id_pic == $pic->id) selected @endif>{{$pic->name}}</option>         
                @endforeach
                </select>
		      </div>
		    </div>
		    @if($product->id_parent != 0)
		    <div class="form-group">
		      <label for="idParent" class="col-lg-2 control-label">Jenis Produk</label>
		      <div class="col-lg-6">
		        <select name="id_parent" class="selectpicker form-control">
		        @foreach($parent as $parent)
                  <option value="{{$parent->id}}" @if ($product->id_parent == $parent->id) selected @endif>{{$parent->name}}</option>         
                @endforeach
                </select>
		      </div>
		    </div>
		    @else
		    	<input type="hidden" name="id_parent" value="0">
		    @endif
		    <hr>		    
		    <div class="form-group" id="submitbutton">		        
		        <button type="submit" class="btn btn-primary">Save</button>
		    </div>
		  </fieldset>
		</form>
      </div>

@stop
