@extends('dashboard.layout')
@section('konten')

<h2 class="sub-header">Semua User</h2>

<table class="table table-hover">
<thead>
			<tr>
			  <th>Name</th>
			  <th>email</th>
			  <th>Role</th>
			  <th>Action</th>
			  <th></th>
			</tr>
			</thead>
			@foreach ($user as $user)
			<tbody>
                <tr>
				  <td>{{ $user->name }}</td>
				  <td>{{ $user->email }}</td>
				  <td>{{ $user->role->name }}</td>
				  <td>{{ link_to_route('users.edit', 'Edit', array($user->id), array('class' => 'btn btn-info')) }}</td>
                  <td>{{ Form::open(array('method' => 'DELETE', 'route' => array('users.destroy', $user->id))) }}                       
                      {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                      {{ Form::close() }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<div class="form-group" id="submitbutton" style="padding-bottom:13px">
	        <a href="{{url('/users/create')}}"><button type="submit" class="btn btn-primary pull-left">Tambah User</button></a>
	    </div>
	  </div>

@stop