<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="shortcut icon" href="{{url('new/img/favicon.png')}}">

      <title>Kudo Ticketing System</title>

      <!-- Bootstrap CSS -->    
      <link href="{{url('new/css/bootstrap.min.css')}}" rel="stylesheet">
      <!-- bootstrap theme -->
      <link href="{{url('new/css/bootstrap-theme.css')}}" rel="stylesheet">
      <!--external css-->
      <!-- font icon -->
      <link href="{{url('new/css/elegant-icons-style.css')}}" rel="stylesheet" />
      <link href="{{url('new/css/font-awesome.min.css')}}" rel="stylesheet" />
      <!-- Custom styles -->
      <link href="{{url('new/css/style.css')}}" rel="stylesheet">
      <link href="{{url('new/css/style-responsive.css')}}" rel="stylesheet" />
       
      <script src="{{url('new/js/jquery.min.js')}}"></script>
      <script src="{{url('js/moment.min.js')}}" type="text/javascript"></script>
      <script src="{{url('js/nanobar-master/nanobar.js')}}" type="text/javascript"></script>
      <script type="text/javascript">
        var options = {
          bg: '#acf',
          // id for new nanobar
          id: 'mynano'
        };
        var nanobar = new Nanobar( options );
        // move bar
        nanobar.go(100); // size bar 30%
        // Finish progress bar
        nanobar.go(100);
      </script>
      <link rel="stylesheet" type="text/css" href="{{url('cleanzone/js/jquery.select2/select2.css')}}" />     
  </head>

  <body>
      @if (Session::has('flash_error'))                    
          <script>
              $(function() {
                  $('#notiferror').modal('show');
              });
          </script>
      @elseif (Session::has('flash_notice'))
          <script>
              $(function() {
                  $('#notifsuccess').modal('show');
              });
          </script>                    
      @endif
      <!-- container section start -->
      <section id="container" class="">
          @yield('menu')
          @yield('sidebar')
          <!--main content start-->
          <section id="main-content" >
              <section class="wrapper" >
                  <div id="pageload">
                  @yield('konten')
                  </div>
              </section>
          </section>
          <!--main content end-->
      </section>
      <!-- container section end -->

  </body>
     
  <script>
    function myticket() {
		$(".loader_page").css("display","block");
      $.ajax({
        type: "GET",
        url: '{{url("/myticket?action=content")}}',
        success: function(result){
          $("#pageload").html(result);
		  $(".loader_page").css("display","none");
        }
      });
      window.history.pushState("object or string", "Title", "{{url("/myticket")}}");
    };
    function unassigned() {
		$(".loader_page").css("display","block");
      $.ajax({
        type: "GET",
        url: '{{url("/bystatus/1?action=content")}}',
        success: function(result){
		 $(".loader_page").css("display","none");
          $("#pageload").html(result);

        }
      });
      //$("#pageload").load('{{url("/unassigned")}} #this');
      window.history.pushState("object or string", "Title", "{{url("/bystatus/1")}}");
    };
    function unreplied() {
		$(".loader_page").css("display","block");
      $.ajax({
        type: "GET",
        url: '{{url("/bystatus/3?action=content")}}',
        success: function(result){
			$(".loader_page").css("display","none");
          $("#pageload").html(result);
		   
        }
      });
      //$("#pageload").load('{{url("/unreplied")}} #this');
      window.history.pushState("object or string", "Title", "{{url("/bystatus/3")}}");
    };
    function replied() {
		$(".loader_page").css("display","block");
      $.ajax({
        type: "GET",
        url: '{{url("/bystatus/4?action=content")}}',
        success: function(result){
			$(".loader_page").css("display","none");
          $("#pageload").html(result);
		   
        }
      });
      //$("#pageload").load('{{url("/replied")}} #this');
      window.history.pushState("object or string", "Title", "{{url("/bystatus/4")}}");
    };
    function closed() {
		$(".loader_page").css("display","block");
      $.ajax({
        type: "GET",
        url: '{{url("/bystatus/2?action=content")}}',
        success: function(result){
			$(".loader_page").css("display","none");
          $("#pageload").html(result);
		   
        }
      });
      //$("#pageload").load('{{url("/closed")}} #this');
      window.history.pushState("object or string", "Title", "{{url("/bystatus/2")}}");
    };
    function home() {
		$(".loader_page").css("display","block");
      $.ajax({
        type: "GET",
        url: '{{url("/home?action=content")}}',
        success: function(result){
			 $(".loader_page").css("display","none");
          $("#pageload").html(result);
		  
        }
      });
      //  $("#pageload").load('{{url("/home")}} #this');
      window.history.pushState("object or string", "Title", "{{url("/home")}}");
      // setInterval(function(){ $("#pageload").load('{{url("/home")}} #this'); }, 10000);
    };
    function newPopup(url) {
      popupWindow = window.open(
      url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes')
    };
	
  	setInterval(function() {
      $.get("{{url('/get/ticket/status/json')}}", function(data, status){
      var $obj=jQuery.parseJSON(data);
  	 $("#status_all_ticket").html($obj.unassigned+$obj.unreplied+$obj.replied+$obj.closed);
  	 $("#status_unassigned_ticket").html($obj.unassigned);
  	 $("#status_unreplied_ticket").html($obj.unreplied);
  	 $("#status_replied_ticket").html($obj.replied);
  	 $("#status_closed_ticket").html($obj.closed);

      });
    }, 120000);
  </script>
  <!-- javascripts -->
  @if(!Request::ajax())
  <script src="{{url('new/js/jquery.js')}}"></script>
  <script src="{{url('new/js/bootstrap.min.js')}}"></script>
   
  @endif
  <!-- nice scroll -->
  <script src="{{url('new/js/jquery.scrollTo.min.js')}}"></script>
  <script src="{{url('new/js/jquery.nicescroll.js')}}"></script>
  <!--custome script for all page-->
  <script src="{{url('new/js/scripts.js')}}"></script>
  <!-- jquery validate js -->
  <script type="text/javascript" src="{{url('new/js/jquery.validate.min.js')}}"></script>
  <!-- custom form validation script for this page-->
  <script src="{{url('new/js/form-validation-script.js')}}"></script>
  <script src="{{url('js/highcharts.js')}}"></script>
  <script src="{{url('js/modules/data.js')}}"></script>
  <script src="{{url('js/modules/exporting.js')}}"></script>
  <script src="{{url('js/list.js')}}"></script>
  <script src="{{url('js/list.pagination.js')}}"></script>

  @if(!Request::ajax())
  <script type="text/javascript" src="{{url('cleanzone/js/jquery.select2/select2.min.js')}}" ></script>
  <script type="text/javascript" src="{{url('cleanzone/js/jquery.nanoscroller/jquery.nanoscroller.js')}}"></script>
  <script type="text/javascript" src="{{url('cleanzone/js/jquery.nestable/jquery.nestable.js')}}"></script>
  <script type="text/javascript" src="{{url('cleanzone/js/behaviour/general.js')}}"></script>
  <script src="{{url('cleanzone/js/jquery.ui/jquery-ui.js')}}" type="text/javascript"></script>
  <script type="text/javascript" src="{{url('cleanzone/js/bootstrap.switch/bootstrap-switch.min.js')}}"></script>
  <script type="text/javascript" src="{{url('cleanzone/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    App.init();
  });
  </script>
  @endif

 	   <div class="loader_page"> <i class="fa fa-spinner fa-spin fa-4x" style=" "></i></div>
</html>
