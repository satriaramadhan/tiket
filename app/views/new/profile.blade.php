@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="{{url('/home')}}">My Ticket</a></li>
        </ol>
    </div>
</div>
<!-- page start-->
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <strong>My Profile<span class="pull-right"></span></strong>
            </header>
            <!-- profile-widget -->
            <div class="col-lg-12">
                <div class="profile-widget profile-widget-info">
                    <div class="panel-body" style="margin-top:10px">
                        <div class="col-lg-2 col-sm-2">              
                            <div class="follow-ava" style="margin-top:20px">
                                <img src="{{url('new/img/logo-kudo.jpg')}}" alt="" style="height:100px; width:100px;">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 follow-info" style="padding-top:0px">
                            <p><h2>{{$user_name}}</h2></p>
                            <p>{{$user_email}}</p>
                            <p>@if($user_role == 1)Admin
                            @elseif($user_role == 2)PIC
                            @elseif($user_role == 3)Customer Service
                            @endif
                            </i></p>
                        </div>
                        <div class="col-lg-2 col-sm-6 follow-info weather-category">
                            <ul>
                                <li class="active">
                                    <i class="fa fa-ticket fa-4x"> </i><br>
                                    {{$tiket_assign}} Assignments
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-sm-6 follow-info weather-category">
                            <ul>
                                <li class="active">
                                    <i class="fa fa-check fa-4x"> </i><br>
                                    {{$tiket_closed}} Ticket Closed
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>    
<!-- page end-->
@stop