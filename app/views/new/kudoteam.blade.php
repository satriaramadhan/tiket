@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
    <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="{{url('/home')}}">My Ticket</a></li>
              </ol>
          </div>
    </div>
    <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Kudo Team
                    </header>
                    <table class="table table-striped table-advance table-hover">
                     <tbody>
                        <tr>
                           <th>ID</th>
                           <th><i class="icon_cart_alt"></i>Name</th>
                           <th><i class="icon_document_alt"></i> All Ticket</th>
                           <th><i class="icon_check_alt"></i> Closed Ticket</th>
                           <th><i class="icon_cogs"></i> Ongoing Ticket</th>
                        </tr>
                        <!-- Count Ticket -->
                        <?php $countTicket=new TicketController2();?>
                        @foreach ($teams as $team)
                        <tr style="cursor:pointer" onclick="petugas('{{$team['username']}}')">
                          <td >{{ $team['id']}}</td>
                          <td style="color: #337ab7">{{$team['username']}}</td>
                          <td ><?php $allTicket = Tickets::where('petugas', '=', $team['username'])->count() ?>{{$allTicket}}</td>
                          <td><?php $closedTicket = Tickets::where('petugas', '=', $team['username'])->where('id_status', '=', 2)->count() ?>{{$closedTicket}}</td>
                          <td><?php $ongoingTicket = Tickets::where('petugas', '=', $team['username'])->where('id_status', '!=', 2)->count() ?>{{$ongoingTicket}}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
                </section>
            </div>
        </div>    
        <!-- page end-->
@stop