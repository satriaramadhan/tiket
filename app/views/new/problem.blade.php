@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i>
                <a href="{{url('/home')}}">My Ticket / </a>
                <a href="{{url('home/product')}}">{{$product->name}} / </a>
                <a href="{{url('product/'.$product->id)}}">Problem </a></li>
            </ol>
        </div>
    </div>
    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Manage Problem {{$product->name}}
                    <span class="pull-right">
                      <a class="tooltips" data-placement="top" data-toggle="modal" href="#modaladd" data-original-title="Add new problem"><i class="icon_plus" style="border:none"></i>Add new problem</a>
                    </span>
                </header>
                <table class="table table-striped table-advance table-hover">
                 <tbody>
                    <tr>
                       <th>ID</th>
                       <th><i class="icon_cart_alt"></i> Problem Name</th>
                       <th><i class="icon_calendar"></i> Last Update</th>
                       <th><i class="icon_cogs"></i> Action</th>
                    </tr>
                    @foreach ($problems as $problem)
                    <tr style="cursor:pointer">
                      <td>{{ $problem->id}}</td>
                      <td style="color: #337ab7">{{ $problem->name}}</td>
                      <td>
                        <script type="text/javascript">
                          document.write(moment('{{$problem->last_update_time}}').fromNow()+' by');
                        </script>
                      <span class="label label-primary"><i class="icon_profile"></i>{{$problem->last_update_by}}</span></td>
                      <td>
                        <div class="btn-group">
                          <a class="btn btn-success tooltips" data-placement="top" data-toggle="modal" href="#modaledit-{{$problem->id}}" data-original-title="Edit problem"><i class="icon_pencil-edit"></i></a>
                        </div>
                        <div class="btn-group">
                          <a class="btn btn-danger tooltips" data-placement="top" data-toggle="modal" href="#modaldelete-{{$problem->id}}" data-original-title="Delete problem"><i class="icon_trash_alt"></i></a>
                        </div>
                      </td>
                    </tr>
                    <!-- Modal edit problem-->
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modaledit-{{$problem->id}}" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                    <h4 class="modal-title">Edit Problem {{$problem->name}}</h4>
                                </div>
                                <div class="modal-body">
                                    <form role="form" id="form_edit_problem" method="POST" action="{{url('editProblem')}}">
                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                        <input type="hidden" name="problem_id" value="{{$problem->id}}">
                                        @if ($product->id_parent == 0)
                                            <input type="hidden" name="id_parent" value="{{$product->id}}">
                                        @endif
                                        <div class="form-group">
                                            <label for="fullname">Problem Name</label>
                                            <input type="text" class="form-control" id="fullname" placeholder="Your Name" name="problemName" value="{{$problem->name}}" required>
                                        </div>
                                        <br>
                                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                        <button type="submit" class="btn btn-primary" onclick="this.disabled=true;">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal edit problem-->
                    <!-- Modal delete problem-->
                    <div class="modal fade" id="modaldelete-{{$problem->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Delete problem {{$problem->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p>Delete {{$problem->name}} from Problem {{$product->name}} table?</p>
                                  <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                  <button class="btn btn-danger" type="button" onclick="this.disabled=true; location.href='{{url('/product/'.$product->id.'/deleteProblem/'.$problem->id)}}'">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal delete problem-->
                    @endforeach
                    <tr>
                      <td colspan="6">
                        <div class="btn-group">
                          <a class="btn btn-success" data-toggle="modal" href="#modaladd"><i class="icon_plus"></i> Add New Problem</a>
                        </div>
                      </td>
                    </tr>     
                    <!-- Modal add problem-->
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modaladd" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                    <h4 class="modal-title">Add New Problem</h4>
                                </div>
                                <div class="modal-body">
                                   <form role="form" id="form_create_problem" method="POST" action="{{url('/createProblem')}}">
                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                        @if ($product->id_parent == 0)
                                            <input type="hidden" name="id_parent" value="{{$product->id}}">
                                        @endif
                                        <div class="form-group">
                                            <label for="fullname">Problem Name</label>
                                            <input type="text" class="form-control" id="fullname" placeholder="Problem Name" name="problemName" required>
                                        </div>
                                        <br>
                                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                        <button type="submit" class="btn btn-primary" onclick="this.disabled=true; setTimeout(this.removeAttribute('disabled'), 3000);">Add</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal add problem-->
                    @if (Session::has('flash_error'))                    
                        <script>
                            $(function() {
                                $('#notiferror').modal('show');
                            });
                        </script>
                    @elseif (Session::has('flash_notice'))
                        <script>
                            $(function() {
                                $('#notifsusccess').modal('show');
                            });
                        </script>                    
                    @endif
                    <!-- Modal notif-->
                    <div class="modal fade" id="notifsusccess" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">                            
                                <div class="modal-body">                                
                                    <h4 class="modal-title">{{ Session::get('flash_notice') }}</h4>
                                    <br>
                                    <button data-dismiss="modal" class="btn btn-primary" type="button">Oke</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="notiferror" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">                            
                                <div class="modal-body">                                
                                    <h4 class="modal-title">{{ Session::get('flash_error') }}</h4>
                                    <br>
                                    <button data-dismiss="modal" class="btn btn-danger" type="button">Try Again</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal notif-->                     
                 </tbody>
              </table>
            </section>
        </div>
    </div>    
    <!-- page end-->
@stop