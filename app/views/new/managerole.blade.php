@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
    <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="{{url('/')}}">Dashboard / </a>
                  <a href="{{url('home/role')}}">Role</a></li>
              </ol>
          </div>
      </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Manage Role
                        <span class="pull-right">
                          <a class="tooltips" data-placement="top" data-toggle="modal" href="#modaladd" data-original-title="Add new role"><i class="icon_plus" style="border:none"></i>Add new role</a>
                        </span>
                    </header>
                    
                    <table class="table table-striped table-advance table-hover">
                     <tbody>
                      <tr>
                           <th>ID</th>
                           <th><i class="icon_profile"></i> Role Name</th>
                           <th><i class="icon_cogs"></i> Action</th>
                        </tr>
                       @foreach($roles as $role)
                        <!-- Modal delete ticket-->
                        <div class="modal fade" id="modaldelete{{$role->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Delete role?</h4>
                                    </div>
                                    <div class="modal-body">
                                      <p>Delete {{$role->name}} from role table?</p>
                                      <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                      <button class="btn btn-danger" type="button" onclick="location.href='{{url('/home/role/delete/'.$role->id)}}'">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal delete ticket-->
                        <!-- Modal edit user-->
                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modaledit{{$role->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Edit Role {{$role->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::model($role, array('method' => 'PATCH', 'route' => array('roleUpdate', $role->id),'class'=>'form-horizontal')) }}
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" class="form-control" id="name" placeholder="Your Name" name="name" value="{{$role->name}}" required>
                                            </div>
                                            <br>
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal edit user-->
                        
                        <tr style="cursor:pointer">
                            <td>{{$role->id}}</td>
                            <td>{{$role->name}}</td>
                            <td>
                            <div class="btn-group">
                                <a class="btn btn-success tooltips" data-placement="top" data-toggle="modal" href="#modaledit{{$role->id}}" data-original-title="Edit Role"><i class="icon_pencil-edit"></i> Edit</a>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-danger tooltips" data-placement="top" data-toggle="modal" href="#modaldelete{{$role->id}}" data-original-title="Delete Role"><i class="icon_trash_alt"></i> Delete</a>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                          <td colspan="5">
                            <div class="btn-group">
                              <a class="btn btn-success" data-toggle="modal" href="#modaladd"><i class="icon_plus"></i> Add New Role</a>
                            </div>
                          </td>
                        </tr>
                        @if (Session::has('flash_error'))                    
                            <script>
                                $(function() {
                                    $('#notiferror').modal('show');
                                });
                            </script>
                        @elseif (Session::has('flash_notice'))
                            <script>
                                $(function() {
                                    $('#notifsusccess').modal('show');
                                });
                            </script>                    
                        @endif
                        <!-- Modal notif-->
                        <div class="modal fade" id="notifsusccess" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">                            
                                    <div class="modal-body">                                
                                        <h4 class="modal-title">{{ Session::get('flash_notice') }}</h4>
                                        <br>
                                        <button data-dismiss="modal" class="btn btn-primary" type="button">Oke</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="notiferror" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">                            
                                    <div class="modal-body">                                
                                        <h4 class="modal-title">{{ Session::get('flash_error') }}</h4>
                                        <br>
                                        <button data-dismiss="modal" class="btn btn-danger" type="button">Try Again</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal notif-->   
                        <!-- Modal add role-->
                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modaladd" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add Role</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::open(array('route' => 'createrole','id'=>'form_user_edit')) }}
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" class="form-control" id="name" placeholder="Your Name" name="name" required>
                                            </div>
                                            <br>
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal add role -->                  
                     </tbody>
                  </table>
                </section>
            </div>
        </div>    
        <!-- page end-->
@stop