@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
    <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="{{url('')}}">Dashboard / </a>
                <a href="{{url('home/user')}}">User</a></li>
              </ol>
          </div>
      </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Manage User
                        <span class="pull-right">
                          <a class="tooltips" data-placement="top" data-toggle="modal" href="#modaladd" data-original-title="Add new user"><i class="icon_plus" style="border:none"></i>Add new user</a>
                        </span>
                    </header>
                    
                    <table class="table table-striped table-advance table-hover">
                     <tbody>
                        <tr>
                           <th>ID</th>
                           <th><i class="icon_profile"></i> Name</th>
                           <th><i class="icon_mail_alt"></i> Email</th>
                           <th><i class="icon-task-l"></i> Role</th>
                           <th><i class="icon_cogs"></i> Action</th>
                        </tr>
                        @foreach($user as $user)
                        <!-- Modal delete user-->
                        <div class="modal fade" id="modaldelete{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Delete User?</h4>
                                    </div>
                                    <div class="modal-body">
                                      <p>Delete {{$user->name}} from user table?</p>
                                      <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                      <button class="btn btn-danger" type="button" onclick="location.href='{{url('/home/user/delete/'.$user->id)}}'">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal delete user-->
                        <!-- Modal edit user-->
                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modaledit{{$user->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Edit {{$user->name}}</h4>
                                    </div>
                                    <div class="modal-body">

                                        {{ Form::model($user, array('method' => 'PATCH', 'route' => array('userUpdate', $user->id),'class'=>'form-horizontal')) }}
                                            <div class="form-group">
                                                <label for="fullname">Name</label>
                                                <input type="text" class="form-control" id="fullname" placeholder="Your Name" name="name" value="{{$user->name}}" required>
                                            </div>                                            
                                            <div class="form-group ">
                                                <label for="email">Email <span class="required">*</span></label>
                                                <input class="form-control " id="email" name="email" type="email" placeholder="name@domain.com" value="{{$user->email}}" required/>
                                            </div>
                                            <div class="form-group ">
                                              <label for="role">Role</label>
                                              <select class="form-control m-bot15" id="role" name="id_role">
                                                @foreach($roles as $role)
                                                  <option value="{{$role->id}}" @if ($user->id_role == $role->id) selected @endif>{{$role->name}}</option>
                                                @endforeach
                                              </select>
                                            </div>
                                            <br>
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal edit user-->
                        <!-- Modal Reset Password-->
                        <div class="modal fade" id="modalReset{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Reset Password {{$user->name}}</h4>
                                  </div>
                                  <div class="modal-body">
                                    <form role="form" id="form_reset_password" method="POST" action="{{url('/home/profile/reset/'.$user->id)}}">
                                      <div class="form-group ">
                                          <label for="password">New Password <span class="required">*</span></label>
                                          <input class="form-control " id="password" name="password" type="password" placeholder="Password" required/>
                                      </div>
                                      <div class="form-group ">
                                          <label for="confirm_password">Confirm Password <span class="required">*</span></label>
                                          <input class="form-control " id="confirm_password" name="password_confirmation" type="password" placeholder="Re-Type Password" required/>
                                      </div>
                                      <br>
                                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                      <button type="submit" class="btn btn-primary">Save</button>
                                    </form>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Reset Password-->                      
                        <tr style="cursor:pointer">
                          <td>{{$user->id}}</td>
                          <td>{{$user->name}}</td>
                          <td>{{$user->email}}</td>
                          <td>{{$user->role->name}}</td>
                          <td>
                            <div class="btn-group">
                              <a class="btn btn-success tooltips" data-placement="top" data-toggle="modal" href="#modaledit{{$user->id}}" data-original-title="Edit user"><i class="icon_pencil-edit"></i> edit</a>
                            </div>
                            <div class="btn-group">    
                              <a class="btn btn-danger tooltips" data-placement="top" data-toggle="modal" href="#modaldelete{{$user->id}}" data-original-title="Delete user"><i class="icon_trash_alt"></i> delete</a>
                            </div>
                            <div class="btn-group">    
                              <a class="btn btn-warning tooltips" data-placement="top" data-toggle="modal" href="#modalReset{{$user->id}}" data-original-title="Reset Password"><i class="icon_pencil-edit"></i> reset password</a>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                          <td colspan="5">
                            <div class="btn-group">
                              <a class="btn btn-success" data-toggle="modal" href="#modaladd"><i class="icon_plus"></i> Add New User</a>
                            </div>
                          </td>
                        </tr>
                        @if (Session::has('flash_error'))                    
                            <script>
                                $(function() {
                                    $('#notiferror').modal('show');
                                });
                            </script>
                        @elseif (Session::has('flash_notice'))
                            <script>
                                $(function() {
                                    $('#notifsusccess').modal('show');
                                });
                            </script>                    
                        @endif
                        <!-- Modal notif-->
                        <div class="modal fade" id="notifsusccess" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">                            
                                    <div class="modal-body">                                
                                        <h4 class="modal-title">{{ Session::get('flash_notice') }}</h4>
                                        <br>
                                        <button data-dismiss="modal" class="btn btn-primary" type="button">Oke</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="notiferror" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">                            
                                    <div class="modal-body">                                
                                        <h4 class="modal-title">{{ Session::get('flash_error') }}</h4>
                                        <br>
                                        <button data-dismiss="modal" class="btn btn-danger" type="button">Try Again</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal notif-->
                        <!-- Modal add user-->
                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modaladd" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add New User</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::open(array('route' => 'createuser','class'=>'form-horizontal')) }}
                                            <div class="form-group">
                                                <label for="fullname">Name</label>
                                                <input type="text" class="form-control" id="fullname" placeholder="Your Name" name="name" required>
                                            </div>
                                            <div class="form-group ">
                                                <label for="password">Password <span class="required">*</span></label>
                                                <input class="form-control " id="password" name="password" type="password" placeholder="Password" require/>
                                            </div>
                                            <div class="form-group ">
                                                <label for="confirm_password">Confirm Password <span class="required">*</span></label>
                                                <input class="form-control " id="confirm_password" name="password_confirmation" type="password" placeholder="Re-Type Password" required/>
                                            </div>
                                            <div class="form-group ">
                                                <label for="email">Email <span class="required">*</span></label>
                                                <input class="form-control " id="email" name="email" type="email" placeholder="name@domain.com" required/>
                                            </div>
                                            <div class="form-group ">
                                              <label for="role">Role</label>
                                              <select class="form-control m-bot15" id="role" name="id_role">
                                                @foreach($roles as $role)
                                                  <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                              </select>
                                            </div>
                                            <br>
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal add user-->
                                       
                     </tbody>
                  </table>
                </section>
            </div>
        </div>    
        <!-- page end-->
@stop