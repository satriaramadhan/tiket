@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
<div id="pageshot" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
 <script type="text/javascript">
            $(function () {
                $('#pageshot').highcharts({
                    data: {
                        table: 'data'
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Laporan Tiket'
                    },
                    subtitle: {
                        text: 'by Produk'
                    },
                    yAxis: {
                        allowDecimals: false,
                        title: {
                            text: 'Jumlah Tiket'
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.series.name + '</b><br/>' +
                                this.point.y + ' ' + 'Tiket';
                        }
                    },
                });
            });
        </script>
<table id="data" style="display:none">
    <thead>
        <tr>
            <th></th>
            @foreach($data as $nama)
            <th>{{$nama->name}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Nama Produk</th>
            @foreach($data as $dataa)
            <td>{{$dataa->data}}</td>
            @endforeach
        </tr>
    </tbody>
@stop
