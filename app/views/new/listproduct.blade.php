@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
    <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="{{url('/home')}}">My Ticket</a></li>
              </ol>
          </div>
    </div>
    <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        List Product
                    </header>
                    <table class="table table-striped table-advance table-hover">
                     <tbody>
                        <tr>
                           <th>ID</th>
                           <th><i class="icon_cart_alt"></i> Product Name</th>
                           <th><i class="icon_document_alt"></i> All Ticket</th>
                           <th><i class="icon_check_alt"></i> Closed Ticket</th>
                           <th><i class="icon_profile"></i> Ongoing Ticket</th>
                        </tr>
                        <!-- Count Ticket -->
                        <?php $countTicket=new TicketController2();?>
                        @foreach ($products as $product)
                        <tr style="cursor:pointer" onclick="products('{{$product->id}}')">
                          <td >{{ $product->id}}</td>
                          <td style="color: #337ab7">{{ $product->name}}</td>
                          <td >{{$countTicket::countAllTicket($product->id)}}</td>
                          <td>{{$countTicket::countClosedTicket($product->id)}}</td>
                          <td>{{$countTicket::countOngoingTicket($product->id)}}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
                </section>
            </div>
        </div>    
        <!-- page end-->
@stop