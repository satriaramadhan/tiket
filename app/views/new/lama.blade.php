<script>
  function modalassign(id) {
    document.getElementById("titleassign").innerHTML = 'Assign ticket #'+id+'?';
    document.getElementById("inputidtiket").innerHTML = '<input type="hidden" name="id_tiket" value="'+id+'"></input>';
    $.get("{{url('/assignmentByTicket')}}/"+id, function(data, status){
      document.getElementById("inputpetugas").innerHTML = data;
    });
    $('#modalassignto').modal('show');
  }
  function modalrestore(id) {
    document.getElementById("restoreidtiket").innerHTML = 'Restore ticket #'+id+'?<input type="hidden" name="id_tiket" value="'+id+'"></input>';
    $('#modalrestoreticket').modal('show');
  }
  function modaldelete(id) {
    document.getElementById("deleteidtiket").innerHTML = 'Remove ticket #'+id+' from database?<input type="hidden" name="id_tiket" value="'+id+'"></input>';
    $('#modalforcedeleteticket').modal('show');
  }
  function loadDataProblemAssignment(idrec) {
    $.get("{{url('/problemByProduct')}}/"+idrec, function(data, status){
      document.getElementById("problem").innerHTML = data;
    });
    $.get("{{url('/assignmentByProduct')}}/"+idrec, function(data, status){
      document.getElementById("assignment").innerHTML = data;
    });
  }
</script>
<div id="this">
  <div class="row">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="{{url('/myticket')}}">My Ticket</a></li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <!-- Modal create new ticket-->
        <div class="modal fade" id="modalcreateticket" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Create New Ticket</h4>
                    </div>
                    <div class="modal-body">
                        @if($dataproduct->count() > 0)
                        {{ Form::open( array('route'=>'tiketcreate','method'=>'post','role'=>'form', 'files' => true)) }}
                            <div class="form-group">
                              <label for="agen">Nama Agen</label>
                              <input type="text" class="form-control" id="nama_agen" placeholder="Nama Agen" name="ticket_agent_name" required>
                            </div>
                            <div class="form-group">
                              <label for="email">Email Agen</label>
                              <input type="text" class="form-control" id="email_agen" placeholder="eg:remon@kudo.co.id" name="ticket_email_agent" required>
                            </div>
                            <div class="form-group ">
                              <label for="product">Product</label>
                              <select class="form-control m-bot15" id="product" name="produk" onchange="loadDataProblemAssignment(this.value);" required>
                                @foreach($dataproduct as $datap)
                                  <option value="{{$datap->id}}">{{$datap->name}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="form-group ">
                              <label for="problem">Problem</label>
                              <select class="form-control m-bot15" id="problem" name="id_problem">
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="no_antrian">No Antrian</label>
                              <input type="text" class="form-control" id="no_antrian" placeholder="123" name="noAntrian" required>
                            </div>
                            <div class="form-group">
                              <label for="tanggal_transaksi">Trx Date</label>
                              <input type="date" class="form-control" id="datepicker" placeholder="2014-05-06" name="tglTransaksi" required>
                            </div>
                            <div class="form-group">
                              <label for="subject">Subject</label>
                              <input type="text" class="form-control" id="subject" placeholder="Ticket's Subject" name="subject" required>
                            </div>
                            <div class="form-group">
                                <label for="comment" style="font-weight:bold">Comment</label>
                                <textarea class="form-control ckeditor" name="deskripsi" rows="3" id="comment"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="attachment" style="font-weight:bold">File input</label>
                                <input type="file" id="attachment" name="attachment">
                                <p class="help-block">Add your attachment.</p>
                            </div>
                            <div class="form-group ">
                              <label for="assignment">Assign ticket to</label>
                              <select class="form-control m-bot15" id="assignment" name="assignment">
                              </select>
                            </div> 
                            <div class="form-group">
                              <label for="cc" class="control-label">CC this ticket to</label>
                              <select class="select2" multiple id="cc" name="cc[]">
                                <?php $user=new UserController();?>
                                <optgroup label="Admin">
                                   <?php $user::getAllAdmin();?>
                                </optgroup>
                                <optgroup label="Customer Service">
                                   <?php $user::getAllCs();?>
                                </optgroup>
                                <optgroup label="PIC Product">
                                   <?php $user::getAllPic();?>
                                </optgroup>
                                <optgroup label="Other">
                                   <?php $user::getAllOther();?>
                                </optgroup>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="level">Level</label>
                              <select class="form-control m-bot15" id="level" name="level">
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="channel">Channel</label>
                              <select class="form-control m-bot15" id="channel" name="channel">
                                <option value="Twitter">Twitter</option>
                                <option value="Facebook">Facebook</option>
                                <option value="Kudo Box">Kudo Box</option>
                                <option value="WhatsApp">WhatsApp</option>
                                <option value="Email">Email</option>
                                <option value="Telepon">Telepon</option>
                                <option value="SMS">SMS</option>
                              </select>
                            </div>
                            <br>
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        @else
                          <p>Tambahkan product dahulu sebelum membuat tiket baru</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal create new ticket-->

        <!-- notif-->
        @if (Session::has('flash_error'))                    
            <script>
                $(function() {
                    $('#notiferror').modal('show');
                });
            </script>
        @elseif (Session::has('flash_notice'))
            <script>
                $(function() {
                    $('#notifsuccess').modal('show');
                });
            </script>                    
        @endif
        <!-- Modal notif-->
        <div class="modal fade" id="notifsuccess" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">                            
                    <div class="modal-body">                                
                        <h4 class="modal-title">{{ Session::get('flash_notice') }}</h4>
                        <br>
                        <button data-dismiss="modal" class="btn btn-primary" type="button">Oke</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="notiferror" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">                            
                    <div class="modal-body">                                
                        <h4 class="modal-title">{{ Session::get('flash_error') }}</h4>
                        <br>
                        <button data-dismiss="modal" class="btn btn-danger" type="button">Try Again</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal notif--> 

        <!-- Modal assign to ticket-->
        <div class="modal fade" id="modalassignto" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="titleassign"></h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{url('/setPetugas')}}" method="post" name="assignto">
                          <div class="form-group " id="inputidtiket">
                          </div>
                          <div class="form-group">
                            <label for="addassignment">Assign ticket to</label>
                            <select class="form-control m-bot15" id="inputpetugas" name="petugas">
                            </select>
                          </div>
                          <br>
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button type="submit" class="btn btn-primary">Save</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal assign to ticket--> 

        <!-- Modal restore ticket-->
        <div class="modal fade" id="modalrestoreticket" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="titlerestore">Restore Ticket</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{url('/restore')}}" method="post">
                          <div class="form-group" id="restoreidtiket">
                          </div>
                          <br>
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button type="submit" class="btn btn-primary">Restore</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal restore ticket-->  

        <!-- Modal force delete ticket-->
        <div class="modal fade" id="modalforcedeleteticket" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="titledelete">Force Delete</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{url('/forcedelete')}}" method="post">
                          <div class="form-group" id="deleteidtiket">
                          </div>
                          <br>
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button type="submit" class="btn btn-danger">Delete</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal force delete ticket-->

        <header class="panel-heading">
            <strong>{{$nama}}<span class="pull-right">
              <a class="tooltips" 
              <?php
              if($dataproduct->count() > 0) {
                echo 'onclick="loadDataProblemAssignment('.array_first($dataproduct, function($key,$value){return $value;})->id.')"';
              } 
              ?>
              data-placement="top" data-toggle="modal" href="#modalcreateticket" data-original-title="Create new ticket"><i class="icon_plus" style="border:none"></i>Create New Ticket</a>
            </span></strong>
        </header>
        
        <table class="table table-striped table-advance table-hover">
          <tbody>
            <tr>
              <th><i class=""></i> No</th>
              <th><i class=""></i> Ticket</th>
              <th><i class="icon_profile"></i> Agent</th>
              <th><i class="icon_mail_alt"></i> Subject</th>
              <th><i class="icon_bag_alt"></i> Product</th>
              <th><i class="icon_bag_alt"></i> Channel</th>
              <th><i class="icon_mobile"></i> Status</th>
              <th><i class="icon_calendar"></i> Last Update</th>
              <th><i class="icon_cogs"></i> Action</th>
            </tr>
          <?php $i = 1;?>
          @foreach ($tickets as $ticket)    
            <tr style="cursor:pointer" >
              <td onclick="location.href='{{url('/detail/'.$ticket->id)}}'" style="padding-left:15px">{{$i}}</td>
              <td onclick="location.href='{{url('/detail/'.$ticket->id)}}'" style="padding-left:15px">#{{$ticket->id}}</td>
              <td onclick="location.href='{{url('/detail/'.$ticket->id)}}'">{{$ticket->user_name}}</td>
              <td onclick="location.href='{{url('/detail/'.$ticket->id)}}'">{{$ticket->subject}}</td>
              <td onclick="location.href='{{url('/detail/'.$ticket->id)}}'">{{$ticket->product->name}}</td>
              <td onclick="location.href='{{url('/detail/'.$ticket->id)}}'">{{$ticket->channel}}</td>
              <td onclick="location.href='{{url('/detail/'.$ticket->id)}}'">
                @if($ticket->status->id == 1)
                  <span class="label label-danger"><i class="icon_profile"></i> 
                    {{$ticket->status->name}}
                  </span>
                @elseif($ticket->status->id == 2)
                  <span class="label label-success"><i class="icon_check_alt"></i> 
                    {{$ticket->petugas}}
                  </span>
                @elseif($ticket->status->id == 3)
                  <span class="label label-warning"><i class="icon_profile"></i> 
                    {{$ticket->petugas}}
                  </span>
                @elseif($ticket->status->id == 4)
                  <span class="label label-primary"><i class="icon_profile"></i> 
                    {{$ticket->petugas}}
                  </span>
                @else
                  <span class="label label-danger"><i class="icon_close"></i> 
                    deleted
                  </span>
                @endif
              </td>
              <td id="update{{$i++}}" onclick="location.href='{{url('/detail/'.$ticket->id)}}'">
                {{$ticket->last_update_time}} by {{$ticket->last_update_by}}
              </td>
              <td>
                @if($ticket->trashed())
                <div class="btn-group">
                  <a onclick="modalrestore({{$ticket->id}})" class="btn btn-success tooltips" data-placement="top" data-toggle="modal" data-original-title="Restore"><i class="icon_refresh"></i></a>
                </div>
                <div class="btn-group">
                  <a onclick="modaldelete({{$ticket->id}})" class="btn btn-danger tooltips" data-placement="top" data-toggle="modal" data-original-title="Force Delete"><i class="icon_trash"></i></a>
                </div>
                @else
                <div class="btn-group">
                  <a onclick="modalassign({{$ticket->id}})" class="btn btn-primary tooltips" data-placement="top" data-toggle="modal" data-original-title="Assigned to"><i class="icon_profile"></i></a>
                </div>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </section>
    </div>
  </div> 
</div>   
<!-- page end-->