<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/favicon.png">

        <title>Login | Kudo Ticketing System</title>

        <!-- Bootstrap CSS -->    
        <link href="{{url('new/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="{{url('new/css/bootstrap-theme.css')}}" rel="stylesheet">
        <!--external css-->
        <!-- font icon -->
        <link href="{{url('new/css/elegant-icons-sty')}}le.css" rel="stylesheet" />
        <link href="{{url('new/css/font-awesome.css')}}" rel="stylesheet" />
        <!-- Custom styles -->
        <link href="{{url('new/css/style.css')}}" rel="stylesheet">
        <link href="{{url('new/css/style-responsive.css')}}" rel="stylesheet" />
    </head>

    <body>
        <div class="container">
            {{ Form::open(array('login','POST','class'=>'login-form')) }}        
                <div class="login-wrap">
                    <p class="login-img"><img src="img/kudo.png"></p>
                    <div class="input-group">
                      <span class="input-group-addon" ><i class="icon_mail"></i></span>
                      <input type="text" class="form-control" name="email" placeholder="Email" autofocus>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                        <input type="password" class="form-control"  name="password" placeholder="Password">
                    </div>
                    <label class="checkbox" style="display:none">
                        <input type="checkbox" value="remember-me"> Remember me
                        <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
                    </label>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
                    @if (Session::has('flash_error'))
                    <div id="flash_error" class="alert alert-danger" role="alert" style="margin-top:20px; text-align:center">
                        <strong>
                            {{ Session::get('flash_error') }}
                        </strong>
                    </div>
                    @endif
                </div>
            </form>
        </div>
    </body>
</html>
