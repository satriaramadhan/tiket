<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="{{url('cleanzone/images/favicon.png')}}">

	<title>Clean Zone</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
	<link href="{{url('cleanzone/js/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">

	<link rel="stylesheet" href="{{url('cleanzone/fonts/font-awesome-4/css/font-awesome.min.css')}}">

	<!-- Custom styles for this template -->
	<link href="{{url('cleanzone/css/style.css')}}" rel="stylesheet" />	

</head>

<body class="texture">

<div id="cl-wrapper" class="error-container">
	<div class="page-error">
		<h1 class="number text-center">500</h1>
		<h2 class="description text-center">Don't worry, there is a little turbulence!</h2>
		<h3 class="text-center"> We're trying to fix it, please try again later.</h3>
	</div>
	<div class="text-center copy">&copy; 2015 <a href="kudo.co.id">Kudo</a></div>
</div>

<script src="{{url('cleanzone/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{url('cleanzone/js/behaviour/general.js')}}"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{url('cleanzone/js/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('cleanzone/js/jquery.flot/jquery.flot.js')}}"></script>
<script type="text/javascript" src="{{url('cleanzone/js/jquery.flot/jquery.flot.pie.js')}}"></script>
<script type="text/javascript" src="{{url('cleanzone/js/jquery.flot/jquery.flot.resize.js')}}"></script>
<script type="text/javascript" src="{{url('cleanzone/js/jquery.flot/jquery.flot.labels.js')}}"></script>
</body>
</html>
