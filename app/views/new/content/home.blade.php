<script src="{{url('js/nanobar-master/nanobar.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  var options = {
    bg: '#acf',
    // id for new nanobar
    id: 'mynano'
  };
  var nanobar = new Nanobar( options );
  // move bar
  nanobar.go(100); // size bar 30%
  // Finish progress bar
  nanobar.go(100);
</script>
<!-- Bootstrap CSS -->    
<link href="{{url('new/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- bootstrap theme -->
<link href="{{url('new/css/bootstrap-theme.css')}}" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="{{url('new/css/elegant-icons-style.css')}}" rel="stylesheet" />
<link href="{{url('new/css/font-awesome.min.css')}}" rel="stylesheet" />
<!-- Custom styles -->
<link href="{{url('new/css/style.css')}}" rel="stylesheet">
<link href="{{url('new/css/style-responsive.css')}}" rel="stylesheet" />

<script src="{{url('js/moment.min.js')}}" type="text/javascript"></script>
<link href="{{url('new/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">
@if($ajax==false)
 <script src="{{url('new/js/jquery.min.js')}}"></script>
<script src="{{url('js/list.js')}}"></script>
<script src="{{url('js/list.pagination.js')}}"></script>
@endif
@if(Request::ajax())
<!-- Start of Clean Zone CSS & JS -->
<link rel="stylesheet" type="text/css" href="{{url('cleanzone/js/jquery.select2/select2.css')}}" />
<script src="{{url('cleanzone/js/jquery.select2/select2.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('cleanzone/js/jquery.nanoscroller/jquery.nanoscroller.js')}}"></script>
<script type="text/javascript" src="{{url('cleanzone/js/jquery.nestable/jquery.nestable.js')}}"></script>
<script type="text/javascript" src="{{url('cleanzone/js/behaviour/general.js')}}"></script>
<script src="{{url('cleanzone/js/jquery.ui/jquery-ui.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('cleanzone/js/bootstrap.switch/bootstrap-switch.min.js')}}"></script>
<script type="text/javascript" src="{{url('cleanzone/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript">
  function cc(){
    //initialize the javascript
    App.init();
  }
</script>
<!-- End of Clean Zone CSS & JS -->
@endif
<!-- javascripts -->
<script type="text/javascript" src="{{url('new/js/jquery.validate.min.js')}}"></script>
<!-- custom form validation script for this page-->
<script src="{{url('new/js/form-validation-script.js')}}"></script>

<script type="text/javascript"> 
  function modalassign(id) {
    document.getElementById("titleassign").innerHTML = 'Assign ticket #'+id+'?';
    document.getElementById("inputidtiket").innerHTML = '<input type="hidden" name="id_tiket" value="'+id+'"></input>';
    $('#modalassignto').modal('show');
  }
  function modalrestore(id) {
    document.getElementById("restoreidtiket").innerHTML = 'Restore ticket #'+id+'?<input type="hidden" name="id_tiket" value="'+id+'"></input>';
    $('#modalrestoreticket').modal('show');
  }
  function modaldelete(id) {
    document.getElementById("deleteidtiket").innerHTML = 'Remove ticket #'+id+' from database?<input type="hidden" name="id_tiket" value="'+id+'"></input>';
    $('#modalforcedeleteticket').modal('show');
  }
  function loadDataProblemAssignment(idrec) {
    $.get("{{url('/problemByProduct')}}/"+idrec, function(data, status){
      document.getElementById("problem").innerHTML = data;
    });
  }
  document.getElementById("count_notif").innerHTML = 0;
  setInterval(function() {
    $.get("{{url('/getwa')}}", function(data, status){
      document.getElementById("count_notif").innerHTML = parseInt(data) - parseInt({{$count}});
    });
  }, 5000);
</script>

<div id="this">
  <div class="row">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="{{url('/myticket')}}">My Ticket</a></li>
      </ol>
    </div>
  </div>
  <!-- page start-->
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <!-- Modal create new ticket-->
        <div class="modal fade" id="modalcreateticket" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Create New Ticket</h4>
                    </div>
                    <div class="modal-body">
                        @if($dataproduct->count() > 0)
                        {{ Form::open( array('route'=>'tiketcreate','method'=>'post','role'=>'form', 'files' => true)) }}
                            <div class="form-group row">
                              <div class="col-lg-6">

                                <label for="agen" style="font-weight:bold">Nama Agen</label>
                                <select class="form-control" id="nama_agen" name="ticket_agent_name" onchange="setEmail(this.value);" required>
								                  <?php $user = new UserController();?>
                                  <?php $user::getAllAgent();?>
                                </select>
                              </div>
                              <div class="col-lg-6">
                                <label for="email" style="font-weight:bold">Email Agen</label>
                                <input type="text" class="form-control" id="email_agen" placeholder="eg:remon@kudo.co.id" name="ticket_email_agent" required readonly="readonly">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-lg-6">
                                <label for="product" style="font-weight:bold">Product</label>
                                <select class="form-control m-bot15" id="product" name="produk" onchange="loadDataProblemAssignment(this.value);" required>
                                  @foreach($dataproduct as $datap)
                                  	 <optgroup label="{{$datap->name}}">
                                     	<?php
                  											$cProd = Products::where('id_parent','=',$datap->id)->get();
                  											foreach($cProd as $rs) {
                  												echo '<option value="'.$rs->id.'">'.$rs->name.'</option>';
                  											}
                  										?>
                                     </optgroup>
                                  @endforeach
                                </select>
                              </div>
                              <div class="col-lg-6">
                                <label for="problem" style="font-weight:bold">Problem</label>
                                <select class="form-control m-bot15" id="problem" name="id_problem">
                                </select>
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="comment" style="font-weight:bold">Message</label>
                                <textarea class="form-control ckeditor" name="deskripsi" rows="3" id="comment"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="attachment" style="font-weight:bold">File input</label>
                                <input type="file" id="attachment" name="attachment">
                                <p class="help-block">Add your attachment.</p>
                            </div>
                            <div class="form-group">
                              <label for="cc" class="control-label" style="font-weight:bold">CC this ticket to</label>
                              <select class="select2" multiple id="cc" name="cc[]">
                                <?php $user=new UserController();?>
                                <optgroup label="Admin">
                                   <?php $user::getAllAdmin();?>
                                </optgroup>
                                <optgroup label="Customer Service">
                                   <?php $user::getAllCs();?>
                                </optgroup>
                                <optgroup label="PIC Product">
                                   <?php $user::getAllPic();?>
                                </optgroup>
                                <optgroup label="Other">
                                   <?php $user::getAllOther();?>
                                </optgroup>
                              </select>
                            </div>
                            @if(Session::get('role') == 3)
                            <div class="form-group">
                              <label for="channel" style="font-weight:bold">Channel</label>
                              <select class="form-control m-bot15" id="channel" name="channel">
                                <option value="Twitter">Twitter</option>
                                <option value="Facebook">Facebook</option>
                                <option value="KudoBox">Kudo Box</option>
                                <option value="WhatsApp">WhatsApp</option>
                                <option value="Email">Email</option>
                                <option value="Telepon">Telepon</option>
                                <option value="SMS">SMS</option>
                              </select>
                            </div>
                            @else
                            <input type="hidden" name="channel" value="Internal">
                            @endif
                            <br>
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        @else
                          <p>Tambahkan product dahulu sebelum membuat tiket baru</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal create new ticket-->

        <!-- notif-->
        @if (Session::has('flash_error'))                    
            <script>
                $(function() {
                    $('#notiferror').modal('show');
                });
            </script>
        @elseif (Session::has('flash_notice'))
            <script>
                $(function() {
                    $('#notifsuccess').modal('show');
                });
            </script>                    
        @endif
        <!-- Modal notif-->
        <div class="modal fade" id="notifsuccess" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">                            
                    <div class="modal-body">                                
                        <h4 class="modal-title">{{ Session::get('flash_notice') }}</h4>
                        <br>
                        <button data-dismiss="modal" class="btn btn-primary" type="button">Oke</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="notiferror" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">                            
                    <div class="modal-body">                                
                        <h4 class="modal-title">{{ Session::get('flash_error') }}</h4>
                        <br>
                        <button data-dismiss="modal" class="btn btn-danger" type="button">Try Again</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal notif--> 

        <!-- Modal assign to me ticket-->
        <div class="modal fade" id="modalassignto" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="titleassign"></h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{url('/setPetugas')}}" method="post" name="assignto" id="assignto">
                          <div class="form-group " id="inputidtiket">
                          </div>
                          <div class="form-group">
                            <label for="addassignment">Are you sure to assign this ticket ?</label>
                            <input class="form-control m-bot15" type="hidden" name="petugas" value="{{Session::get('name')}}"></input>
                          </div>
                          <br>
                          <button data-dismiss="modal" class="btn btn-default" type="button">No</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal assign to me ticket--> 

        <!-- Modal restore ticket-->
        <div class="modal fade" id="modalrestoreticket" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="titlerestore">Restore Ticket</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{url('/restore')}}" method="post">
                          <div class="form-group" id="restoreidtiket">
                          </div>
                          <br>
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button type="submit" class="btn btn-primary">Restore</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal restore ticket-->  

        <!-- Modal force delete ticket-->
        <div class="modal fade" id="modalforcedeleteticket" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="titledelete">Force Delete</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{url('/forcedelete')}}" method="post">
                          <div class="form-group" id="deleteidtiket">
                          </div>
                          <br>
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button type="submit" class="btn btn-danger">Delete</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal force delete ticket-->
        <div id="ticket-list">
        <header class="panel-heading">
            <strong>{{$nama}} <input class="search" placeholder="Search" style="background-color: white;height:28px;"/><span class="pull-right">
              <a class="tooltips" 
              <?php
              if($dataproduct->count() > 0) {
                echo 'onclick="loadDataProblemAssignment('.array_first($dataproduct, function($key,$value){return $value;})->id.');cc();"';
              } 
              ?>
              data-placement="top" data-toggle="modal" href="#modalcreateticket" data-original-title="Create new ticket"><i class="icon_plus" style="border:none"></i>Create New Ticket</a>
            </span>
          </strong>
        </header>
        <div id="ticket-list">
        <table class="table table-striped table-advance table-hover">
          <thead>
            <tr>
              <th class="sort" data-sort="no"><i class=""></i><a href="javascript:;" class="">No</a></th>
              <th class="sort" data-sort="ticket"><i class=""></i><a href="javascript:;" class=""> Ticket</a></th>
              <th class="sort" data-sort="agent"><i class="icon_profile"></i><a href="javascript:;" class=""> Agent</a></th>
              <th class="sort" data-sort="subject"><i class="icon_mail_alt"></i><a href="javascript:;" class=""> Message</a></th>
              <th class="sort" data-sort="product"><i class="icon_bag_alt"></i><a href="javascript:;" class=""> Product</a></th>
              <th class="sort" data-sort="channel"><i class="icon_bag_alt"></i><a href="javascript:;" class=""> Channel</a></th>
              <th class="sort" data-sort="status"><i class="icon_mobile"></i><a href="javascript:;" class=""> Status</a></th>
              <th class="sort" data-sort="lastupdate"><i class="icon_calendar"></i><a href="javascript:;" class=""> Last Update</a></th>
              <th><i class="icon_cogs"></i> Action</th>
            </tr>
          </thead>
          <tbody class="list">
          <?php $i = 1;?>
          @foreach ($tickets as $ticket)
            <tr style="cursor:pointer" >
              <td class="no" onclick="location.href='{{url('/detail/'.$ticket->id)}}'" style="padding-left:15px">{{$i}}</td>
              <td class="ticket "onclick="location.href='{{url('/detail/'.$ticket->id)}}'" style="padding-left:15px">#{{$ticket->id}}</td>
              <td class="agent" onclick="location.href='{{url('/detail/'.$ticket->id)}}'">{{$ticket->user_name}}</td>
              <td class="subject" onclick="location.href='{{url('/detail/'.$ticket->id)}}'">{{substr($ticket->message,0,25)}}..</td>
              <td class="product" onclick="location.href='{{url('/detail/'.$ticket->id)}}'">{{$ticket->product->name}}</td>
              <td class="channel" onclick="location.href='{{url('/detail/'.$ticket->id)}}'">{{$ticket->channel}}</td>
              <td class="status" onclick="location.href='{{url('/detail/'.$ticket->id)}}'">
                @if($ticket->status->id == 1)
                  <span class="label label-danger"><i class="icon_profile"></i> 
                  <?php
                    switch ($ticket->group_ass) {
                      case 3:
                        echo 'Open in CS';
                        break;
                      case 5:
                        echo 'Open in IT';
                        break;
                      case 6:
                        echo 'Open in QA';
                        break;
                      case 2:
                        echo 'Open in PIC';
                        break;
                      case 1:
                        echo 'Open in Admin';
                        break;
                      default:
                        echo 'Open in Others';
                        break;
                    }
                  ?>
                  </span>
                @elseif($ticket->status->id == 2)
                  <span class="label label-success"><i class="icon_check_alt"></i> 
                    {{$ticket->petugas}}
                  </span>
                @elseif($ticket->status->id == 3)
                  <span class="label label-warning"><i class="icon_profile"></i> 
                    {{$ticket->petugas}}
                  </span>
                @elseif($ticket->status->id == 4)
                  <span class="label label-primary"><i class="icon_profile"></i> 
                    {{$ticket->petugas}}
                  </span>
                @else
                  <span class="label label-danger"><i class="icon_close"></i> 
                    deleted
                  </span>
                @endif
              </td>
              <td class="lastupdate" id="update{{$i++}}" onclick="location.href='{{url('/detail/'.$ticket->id)}}'">
                {{$ticket->last_update_time}} by {{$ticket->last_update_by}}
              </td>
              <td>
                @if($ticket->trashed())
                <div class="btn-group">
                  <a onclick="modalrestore({{$ticket->id}})" class="btn btn-success tooltips" data-placement="top" data-toggle="modal" data-original-title="Restore"><i class="icon_refresh"></i></a>
                </div>
                <div class="btn-group">
                  <a onclick="modaldelete({{$ticket->id}})" class="btn btn-danger tooltips" data-placement="top" data-toggle="modal" data-original-title="Force Delete"><i class="icon_trash"></i></a>
                </div>
                @else
                @if($ticket->petugas != Session::get('name'))
                <div class="btn-group">
                  <a onclick="modalassign({{$ticket->id}})" class="btn btn-primary tooltips" data-placement="top" data-toggle="modal" data-original-title="Assign to me">Assign to me</a>
                </div>
                @endif
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
          <ul class="pagination"></ul>
        </table>
      </div>
      </section>
    </div>
  </div> 
</div>   
<script type="text/javascript">
  try{
    //var options = {
     // valueNames: [ 'no', 'ticket', 'agent', 'subject', 'product', 'channel', 'status', 'lastupdate' ]
    //};
    var userList = new List('ticket-list', {valueNames: [ 'no', 'ticket', 'agent', 'subject', 'product', 'channel', 'status', 'lastupdate' ],
      page: 20,
      plugins: [ ListPagination({}) ]
    });
  }catch(e){}
  	  
  function setEmail(value) {
  	var arr = value.split("|");
  	document.getElementById("email_agen").value = arr[0];
  }
</script>
<!-- page end-->