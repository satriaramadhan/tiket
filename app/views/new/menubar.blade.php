<!-- header start--> 
<header class="header kudo-blue">
    <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
    </div>

    <!--logo start-->
    <a href="{{url('/')}}" class="logo" style="color:#fff">Kudo Ticketing System</a>
    <!--logo end-->

    <div class="nav search-row" id="top_menu" style="display:none">
        <!--  search form start -->
        <ul class="nav top-menu">                    
            <li>
                <form class="navbar-form">
                    <input class="form-control" placeholder="Search" type="text">
                </form>
            </li>                    
        </ul>
        <!--  search form end -->                
    </div>
    <?php $classView=new TicketController2();?>
     <?php $count=$classView::countMyTicket();?>
    <div class="top-nav notification-row">                
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
            <!-- inbox notificatoin start-->
            <li id="mail_notificatoin_bar" class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="icon-envelope-l"></i>
                    <span class="badge bg-important" id="count_notif"></span>
                </a>
                <ul class="dropdown-menu extended inbox">
                    <div class="notify-arrow notify-arrow-blue"></div>
                    <li>
                        <p class="blue">You have {{$count}} open ticket assigned to you</p>
                    </li>
                    <li>
                        <a href="{{url('/myticket')}}">See all my ticket</a>
                    </li>
                </ul>
            </li>
            <!-- inbox notificatoin end -->
            <!-- alert notification start-->
            <li id="alert_notificatoin_bar" class="dropdown" style="display:none">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="icon-bell-l"></i>
                    <span class="badge bg-important">2</span>
                </a>
                <ul class="dropdown-menu extended notification">
                    <div class="notify-arrow notify-arrow-blue"></div>
                    <li>
                        <p class="blue">You have 2 new notifications</p>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label label-primary"><i class="icon_chat"></i></span> 
                            Ticket #23 commented by agent
                            <span class="small italic pull-right">5 mins</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label label-success"><i class="icon_check_alt2"></i></span>  
                            Ticket #32 closed by agent
                            <span class="small italic pull-right">50 mins</span>
                        </a>
                    </li>                          
                    <li>
                        <a href="#">See all notifications</a>
                    </li>
                </ul>
            </li>
            <!-- alert notification end-->
            <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="profile-ava">
                        <img alt="" src="{{url('new/img/logo-kudo-small.jpg')}}">
                    </span>
                    <span class="username">{{Session::get('name')}}</span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu extended logout">
                    <div class="log-arrow-up"></div>
                    <li class="eborder-top">
                        <a href="{{url('/home/profile')}}"><i class="icon_profile"></i> My Profile</a>
                    </li>
                    <li>
                        <a href="{{url('/myticket')}}"><i class="icon_mail_alt"></i> My Ticket</a>
                    </li>
                    <li>
                        <a href="{{url('/logout')}}"><i class="icon_key_alt"></i> Log Out</a>
                    </li>
                </ul>
            </li>
            <!-- user login dropdown end -->
        </ul>
        <!-- notification dropdown end-->
    </div>
</header>      
<!--header end