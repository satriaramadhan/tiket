@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
    <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="{{url('/home')}}">My Ticket / </a><a href="{{url('home/product')}}">Product</a></li>
              </ol>
          </div>
    </div>
    <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Manage Product
                        <span class="pull-right">
                          <a class="tooltips" data-placement="top" data-toggle="modal" href="#modaladd" data-original-title="Add new product"><i class="icon_plus" style="border:none"></i>Add new product</a>
                        </span>
                    </header>
                    <table class="table table-striped table-advance table-hover">
                     <tbody>
                        <tr>
                           <th>ID</th>
                           <th><i class="icon_cart_alt"></i> Product Name</th>
                           <th><i class="icon_cart_alt"></i> Category</th>
                           <th><i class="icon_profile"></i> PIC</th>
                           <th><i class="icon_calendar"></i> Last Update</th>
                           <th><i class="icon_cogs"></i> Action</th>
                        </tr>
                        @foreach ($products as $product)
                        <tr style="cursor:pointer">
                          <td onclick="location.href='{{url('/product/'.$product->id)}}'">{{ $product->id}}</td>
                          <td onclick="location.href='{{url('/product/'.$product->id)}}'" style="color: #337ab7">{{ $product->name}}</td>
                          <td onclick="location.href='{{url('/product/'.$product->id)}}'">{{ $product->parent->name or $product->name}}</td>
                          <td onclick="location.href='{{url('/product/'.$product->id)}}'"> 
                            @foreach($product->pic as $pic)
                              <span class="label label-primary" style="margin-left:5px"><i class="icon_profile"></i> 
                                {{$pic->nama_pic or 1}} 
                              </span>
                            @endforeach
                          </td>
                          <td onclick="location.href='{{url('/product/'.$product->id)}}'">
                            <script type="text/javascript">
                              document.write(moment('{{$product->last_update_time}}').fromNow()+' by');
                            </script>
                          <span class="label label-primary"><i class="icon_profile"></i>{{$product->last_update_by}}</span></td>
                          <td>
                            <div class="btn-group">
                              <a class="btn btn-success tooltips" data-placement="top" data-toggle="modal" href="#modaledit-{{$product->id}}" data-original-title="Edit product"><i class="icon_pencil-edit"></i></a>
                            </div>
                            <div class="btn-group">
                              <a class="btn btn-danger tooltips" data-placement="top" data-toggle="modal" href="#modaldelete-{{$product->id}}" data-original-title="Delete product"><i class="icon_trash_alt"></i></a>
                            </div>
                          </td>
                        </tr>
                        <!-- Modal edit product-->
                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modaledit-{{$product->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Edit Product {{$product->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" id="form_edit_product" method="POST" action="{{url('editProduct')}}">
                                            <input type="hidden" name="id" value="{{$product->id}}">
                                            <div class="form-group">
                                                <label for="fullname">Product Name</label>
                                                <input type="text" class="form-control" id="fullname" placeholder="Your Name" name="productName" value="{{$product->name}}" required>
                                            </div>
                                            <div class="form-group ">
                                              <label for="pic">PIC</label><br>
                                              <?php
                                                $arr = array(); 
                                                foreach ($product->pic as $pic) {
                                                  array_push($arr, $pic->id_pic);
                                                }
                                              ?>
                                              @foreach($pics as $pic)
                                                @if(in_array($pic['id'], $arr))
                                                  <input type="checkbox" name="pic_edit{{$product->id}}[]" value="{{$pic['id'].','.$pic['username'].','.$pic['email']}}" checked>
                                                @else
                                                  <input type="checkbox" name="pic_edit{{$product->id}}[]" value="{{$pic['id'].','.$pic['username'].','.$pic['email']}}">
                                                @endif
                                                {{$pic['username']}}<br>
                                              @endforeach
                                            </div>
                                            
                                            <div class="form-group ">
                                              <label for="category">Category</label>
                                              <select class="form-control m-bot15" id="category" name="id_parent">
                                                <option value=0>(As a New Category)</option>
                                                @foreach($parents as $parent)
                                                    @if($product->id_parent == 0)
                                                      <option value="{{$parent->id}}" @if ($product->id == $parent->id) selected="selected" @endif>{{$parent->name}}</option>
                                                    @else
                                                      <option value="{{$parent->id}}" @if ($product->id_parent == $parent->id) selected @endif>{{$parent->name}}</option>
                                                    @endif
                                                @endforeach
                                              </select>
                                            </div>
                                            <br>
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                            <button type="submit" class="btn btn-primary" onclick="this.disabled=true; setTimeout(this.removeAttribute('disabled'), 3000); return validate({{$product->id}});">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal edit product-->
                        <!-- Modal delete product-->
                        <div class="modal fade" id="modaldelete-{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Delete Product {{$product->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                      <p>Delete {{$product->name}} from Product table?</p>
                                      <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                      <button id="btn-save-assign" class="btn btn-danger" type="button" onclick="this.disabled=true; setTimeout(this.removeAttribute('disabled'), 3000); location.href='{{url('/product/'.$product->id.'/deleteProduct')}}'">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal delete Product-->
                        @endforeach
                        <tr>
                          <td colspan="6">
                            <div class="btn-group">
                              <a class="btn btn-success" data-toggle="modal" href="#modaladd"><i class="icon_plus"></i> Add New Product</a>
                            </div>
                          </td>
                        </tr>     
                        <!-- Modal add product-->
                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modaladd" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add New Product</h4>
                                    </div>
                                    <div class="modal-body">
                                      @if(count($pics) > 0)
                                        <form role="form" id="form_create_product" method="POST" action="{{url('/createProduct')}}">
                                            <div class="form-group">
                                                <label for="fullname">Product Name</label>
                                                <input type="text" class="form-control" id="fullname" placeholder="Product Name" name="productName" required>
                                            </div>
                                            <div class="form-group ">
                                              <label for="pic">PIC</label><br>
                                              @foreach($pics as $pic)
                                                <input type="checkbox" name="pic_add[]" value="{{$pic['id'].','.$pic['username'].','.$pic['email']}}"> {{$pic['username']}}<br>
                                              @endforeach
                                            </div>
                                            <div class="form-group ">
                                              <label for="category">Category</label>
                                              <select class="form-control m-bot15" id="category" name="id_parent">
                                                <option value=0>(As a New Category)</option>
                                                @foreach($parents as $parent)
                                                  <option value="{{$parent->id}}">{{$parent->name}}</option>      
                                                @endforeach
                                              </select>
                                            </div>
                                            <br>
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                            <button type="submit" class="btn btn-primary btn-submit" onclick="this.disabled=true; setTimeout(this.removeAttribute('disabled'), 3000); return validate(0);">Add</button>
                                        </form>
                                        <script type="text/javascript">
                                        function validate(i) {
                                          if(i > 0) {
                                            var arr = document.getElementsByName('pic_edit'+i+'[]');
                                          }
                                          else {
                                            var arr = document.getElementsByName('pic_add[]');
                                          }
                                          var j = 0;
                                          for(var i = 0; i < arr.length; i++) {
                                            if(arr[i].checked) {
                                              j++;
                                            }
                                          }
                                          if(j > 0) {
                                            if(j > 2) {
                                              document.getElementById("messagenotiferror").innerHTML = '<p style="color:red">PIC tidak boleh lebih dari dua.</p>';
                                              $('#notiferror').modal('show');
                                              return false;
                                            }
                                            else {
                                              return true;
                                            }
                                          }
                                          else {
                                            document.getElementById("messagenotiferror").innerHTML = 'PIC tidak boleh kosong.';
                                            $('#notiferror').modal('show');
                                            return false;
                                          }
                                        }
                                        </script>
                                      @else 
                                        <p>Tambahkan user dengan role PIC dulu ya!</p>
                                      @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal add product-->
                        @if (Session::has('flash_error'))                    
                            <script>
                                $(function() {
                                    $('#notiferror').modal('show');
                                });
                            </script>
                        @elseif (Session::has('flash_notice'))
                            <script>
                                $(function() {
                                    $('#notifsuccess').modal('show');
                                });
                            </script>                    
                        @endif
                        <!-- Modal notif-->
                        <div class="modal fade" id="notifsuccess" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">                            
                                    <div class="modal-body">                                
                                        <h4 class="modal-title">{{ Session::get('flash_notice') }}</h4>
                                        <br>
                                        <button data-dismiss="modal" class="btn btn-primary" type="button">Oke</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="notiferror" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">                            
                                    <div class="modal-body">                                
                                        <h4 class="modal-title" id="messagenotiferror">
                                        <?php 
                                          if(Session::has('flash_error')) {
                                            echo Session::get('flash_error');
                                          }
                                        ?>
                                        </h4>
                                        <br>
                                        <button data-dismiss="modal" class="btn btn-danger" type="button">Try Again</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal notif-->                     
                     </tbody>
                  </table>
                </section>
            </div>
        </div>    
        <!-- page end-->
@stop