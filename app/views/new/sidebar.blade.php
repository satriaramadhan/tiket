<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">                
            <li class="">
                <a class="" onclick="myticket();">
                    <i class="icon_house_alt"></i>
                    <span>My Ticket</span>
                </a>
            </li>
            <?php $classView=new TicketController2();?>
            <?php $count1=$classView::countUnassigned();?>
            <?php $count2=$classView::countUnreplied();?>
            <?php $count3=$classView::countReplied();?>
            <?php $count4=$classView::countClosed();?>
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_document_alt"></i>
                    <span>Tickets</span>
                    <span class="badge bg-important" style="padding-top:4px; margin-left:12px;">{{$count1+$count2+$count3}} open</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                
                <ul class="sub">
                    <li>
                         <a class="" onclick="home();">All
                         <span class="badge bg-important pull-right" style="margin-right:10px; margin-top:9px;" id="status_all_ticket">{{$count1+$count2+$count3+$count4}}</span></a>
                    </li>
                    <li>
                         <a class="" onclick="unassigned();">Open
                         <span class="badge bg-unassigned pull-right" style="margin-right:10px; margin-top:9px;" id="status_unassigned_ticket">{{$count1}}</span></a>
                    </li>                          
                    <li>
                         <a class="" onclick="unreplied();">Unreplied
                         <span class="badge bg-warning pull-right" style="margin-right:10px; margin-top:9px;" id="status_unreplied_ticket">{{$count2}}</span></a>
                    </li> 
                    <li>
                         <a class="" onclick="replied();">Replied
                         <span class="badge bg-replied pull-right" style="margin-right:10px; margin-top:9px;" id="status_replied_ticket">{{$count3}}</span></a>
                    </li> 
                    <li>
                         <a class="" onclick="closed();">Closed
                         <span class="badge bg-success pull-right" style="margin-right:10px; margin-top:9px;" id="status_closed_ticket">{{$count4}}</span></a>
                    </li> 
                </ul>
            </li>       
            @if(Session::get('role') == 1 || Session::get('role') == 3)
            <li class="">
                <a class="" href="{{url('/home/kudoteam')}}">
                    <i class="icon_profile"></i>
                    <span>Kudo Team</span>
                </a>
            </li>
            <li class="">
                <a class="" href="{{url('/home/listproduct')}}">
                    <i class="icon_cart_alt"></i>
                    <span>Products</span>
                </a>
            </li> 
            @endif
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_datareport"></i>
                    <span>Reports</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li>
                         <a class="" href="{{url('/home/report/produk')}}">Produk</a>
                    </li>
                    <li>
                         <a class="" href="{{url('/home/report/problem')}}">Problem</a>
                    </li>
                    <li>
                         <a class="" href="{{url('/home/report/status')}}">Status</a>
                    </li>
                    <li>
                         <a class="" href="{{url('/home/report/periode')}}">Periode</a>
                    </li>                        
                    <li>
                         <a class="" href="{{url('/home/report/priority')}}">Priority</a>
                    </li>  
                    <li>
                         <a class="" href="#">Agent Area</a>
                    </li>
                </ul>
            </li>
            @if(Session::get('role') == 1)
            <li class="">
                <a class="" href="{{url('/home/product')}}">
                    <i class="icon_cog"></i>
                    <span>Manage Products</span>
                </a>
            </li>
            @endif
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<style>
#sidebar a{
	cursor:pointer;
}
</style>
<!--sidebar end-->
