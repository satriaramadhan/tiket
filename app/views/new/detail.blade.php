@extends('new.layout')
@section('menu')
  @extends('new.menubar')
@stop
@section('sidebar')
  @extends('new.sidebar')
@stop
@section('konten')
<script>
    function loadDataProblemAssignment(idrec) {
        $.get("{{url('/problemByProduct')}}/"+idrec, function(data, status){
          document.getElementById("problem").innerHTML = data;
        });
        $.get("{{url('/assignmentByProduct')}}/"+idrec, function(data, status){
          document.getElementById("assignment").innerHTML = data;
        });
    }
    function getTrx() {
        var param = document.getElementById('no_antrian').value+'|'+document.getElementById('tglTransaksi').value;
        $('#detail_trx').select2('val', '');
        $.get("{{url('/detailtrx')}}/"+param, function(data, status){
            document.getElementById("detail_trx").innerHTML = data;
        });
    }
</script>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="{{url('/')}}">My Ticket</a></li>
        </ol>
    </div>
</div>
<!-- page start-->
<div class="row">
    <div class="col-lg-7">
        <section class="panel">
            <header class="panel-heading">
                <strong>Tiket #{{$ticket->id}}
                    @if($ticket->id_status == 1)
                        <label style="color:#ff2d55; font-weight:bold">
                        <?php
                            switch ($ticket->group_ass) {
                              case 3:
                                echo '(Open in CS)';
                                break;
                              case 5:
                                echo '(Open in IT)';
                                break;
                              case 6:
                                echo '(Open in QA)';
                                break;
                              case 2:
                                echo '(Open in PIC)';
                                break;
                              case 1:
                                echo '(Open in Admin)';
                                break;
                              default:
                                echo '(Open in Others)';
                                break;
                            }
                        ?>
                        </label>
                    @elseif($ticket->id_status == 3)
                        <label style="color:#ffcc00; font-weight:bold">(Unreplied by {{$ticket->petugas}})</label>
                    @elseif($ticket->id_status == 4)
                        <label style="color:#50a5cb; font-weight:bold">(Replied by {{$ticket->petugas}})</label>
                    @else
                        <label style="color:#4cd964; font-weight:bold">(Closed)</label>
                    @endif
                    @if($ticket->last_solved_by == "")
                        <label style="color:#ff2d55; font-weight:bold">
                        <?php
                            switch ($ticket->group_ass) {
                              case 3:
                                echo '(Unsolved in CS)';
                                break;
                              case 5:
                                echo '(Unsolved in IT)';
                                break;
                              case 6:
                                echo '(Unsolved in QA)';
                                break;
                              case 2:
                                echo '(Unsolved in PIC)';
                                break;
                              case 1:
                                echo '(Unsolved in Admin)';
                                break;
                              default:
                                echo '(Unsolved in Others)';
                                break;
                            }
                        ?>
                        </label>
                    @elseif($ticket->last_solved_by != "")
                        <label style="color:#4cd964; font-weight:bold">
                        Solved By {{$ticket->last_solved_by}}
                        </label>
                    @endif
                    <span class="pull-right">
                        @if($ticket->petugas == Session::get('name') && Session::get('role') != 3 && Session::get('role') != 2)
                        <a style="color:#4cd964" class="tooltips" data-placement="top" data-toggle="modal" href="#modalsolved" data-original-title="Mark as Solved">Mark as Solved | <i class="icon_check_alt" style="border:none"></i></a>
                        <a style="color:#ff2d55" class="tooltips" data-placement="top" data-toggle="modal" href="#modalunsolved" data-original-title="Mark as Unsolved">Mark as Unsolved | <i class="icon_close_alt" style="border:none"></i></a>
                        @elseif($ticket->petugas == Session::get('name') && $ticket->last_solved_by != "" && Session::get('role') == 3)
                        <a style="color:#ff2d55" class="tooltips" data-placement="top" data-toggle="modal" href="#modalunsolved" data-original-title="Mark as Unsolved">Mark as Unsolved | <i class="icon_close_alt" style="border:none"></i></a>
                        @elseif($ticket->petugas == Session::get('name') && $ticket->last_solved_by == "" && Session::get('role') == 2)
                        <a style="color:#4cd964" class="tooltips" data-placement="top" data-toggle="modal" href="#modalsolved" data-original-title="Mark as Solved">Mark as Solved | <i class="icon_check_alt" style="border:none"></i></a>
                        @elseif(Session::get('role') == 4)
                        @endif
                        @if($ticket->id_status == 4)
                            @if(Session::get('role') == 3 || $ticket->created_by == Session::get('name'))
                            <a style="color:#ff2d55" class="tooltips" data-placement="top" data-toggle="modal" href="#modalclosing" data-original-title="Set as closed">Close Ticket | <i class="icon_close_alt" style="border:none"></i></a>
                            @endif
                        @elseif($ticket->id_status == 2)
                            @if(Session::get('role') == 3 || $ticket->created_by == Session::get('name'))
                            <a style="color:#4cd964" class="tooltips" data-placement="top" data-toggle="modal" href="#modalreopen" data-original-title="Set as open">Re-Open | <i class="icon_check_alt" style="border:none"></i></a>
                            @endif
                        @endif
                    </span>
                </strong>
            </header>
            <!-- Widget content -->
            <div class="panel-body">
                <div style="font-weight:bold" class="chat-meta">{{$ticket->user_name}}<span class="pull-right">{{$ticket->created_date}}</span></div>
                <br>
                <p>{{$ticket->message}}</p>
                <div class="attachment">
                @if($ticket->attachment != null)
                <img src="{{url($ticket->attachment->path)}}" style="max-width:400px" onClick="newPopup('{{url($ticket->attachment->path)}}');">
                @endif
                </div>
                <div class="pull-right">
                    @if($ticket->id_status == 1 )
                    <div class="btn-group">
                      <a class="btn btn-primary tooltips" data-placement="top" data-toggle="modal" href="#modalassigntome" data-original-title="Assign to me">Assign to me</a>
                    </div>
                    @endif
                    @if($ticket->id_status != 2 && Session::get('role')!=2)
                        @if($ticket->no_antrian != 0 && $ticket->petugas != '')
                            <div class="btn-group">
                                <a class="btn btn-primary tooltips" data-placement="top" data-toggle="modal" href="#modalassignto" data-original-title="Assigned to"><i class="fa fa-group"></i></a>
                            </div>
                        @endif
                    @endif
                    <div class="btn-group">
                        <a class="btn btn-danger tooltips" data-placement="top" data-toggle="modal" href="#modaldelete" data-original-title="Delete Ticket"><i class="icon_trash"></i></a>
                    </div>
                </div>
                <br>
                <hr>
                <div class="form-group row">
                    <div class="col-lg-8">
                        Cc this ticket to :
                        <br>
                        @foreach($listcc as $cc)
                        <span class="label label-success">{{$cc->user_name}}</span>
                        @endforeach
                        <span> | </span>
                        <a data-toggle="modal" href="#modalcc"><span class="label label-default" data-placement="top" data-original-title="Edit CC Ticket"><i class="icon_pencil"></i> Edit cc</span></a>
                    </div>
                    <div class="col-lg-4">
                        Duplicates to :
                        <br>
                        <div style="margin-bottom:5px">
                            @if($ticket->duplicate_to > 0)
                            <a href="{{url('/detail/'.$ticket->duplicate_to)}}"><span class="label label-success">#{{$ticket->duplicate_to}}</span></a>
                            @else
                            -
                            @endif
                            <span> | </span>
                            <a data-toggle="modal" href="#modalduplicate"><span class="label label-default" data-placement="top" data-original-title="Edit duplicate"><i class="icon_pencil"></i> Edit Duplicate</span></a>
                        </div>
                        <div style="margin-bottom:5px;">Duplicate by :</div>
                        <div style="margin-bottom:5px;">
                            @if(count($duplicate_by) > 0)
                                @foreach($duplicate_by as $dup) 
                                <a href="{{url('/detail/'.$dup)}}"><span class="label label-success">#{{$dup}}</span></a>
                                @endforeach
                            @else 
                            -
                            @endif
                        </div>       
                    </div>
                </div>
                <ul class="chats">
                @if(count($comments) > 0)
                    @foreach ($comments as $comment)
                    @if($comment->user_name == Session::get('name'))
                    <!-- Chat me. Use the class "by-me". -->
                    <li class="by-me">
                        <!-- Use the class "pull-left" in avatar -->
                        <div class="avatar pull-left">
                            <!-- <img src="img/user.jpg" alt=""/> -->
                            <img alt="avatar" src="{{url('new/img/logo-kudo-mini.jpg')}}">
                        </div>
                        <div class="chat-content">
                            <!-- In meta area, first include "name" and then "time" -->
                            <div class="chat-meta">{{$comment->user_name}} <span class="pull-right">{{$comment->date}}</span></div>
                            {{$comment->message}}
                            <div class="attachment">
                            @if($comment->attachment != null)
                            <img style="max-width:400px" src="{{url($comment->attachment->path)}}" onClick="newPopup('{{url($comment->attachment->path)}}');">
                            @endif
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </li> 
                    @else
                    <!-- Chat by other. Use the class "by-other". -->
                    <li class="by-other">
                        <!-- Use the class "pull-right" in avatar -->
                        <div class="avatar pull-right">
                            <!-- <img src="img/user22.png" alt=""/> -->
                            <img alt="avatar" src="{{url('new/img/logo-kudo-mini.jpg')}}">
                        </div>

                        <div class="chat-content">
                            <!-- In the chat meta, first include "time" then "name" -->
                            <div class="chat-meta">{{$comment->date}} <span class="pull-right">{{$comment->user_name}} {{$ticket->user_name}}</span></div>
                            {{$comment->message}}
                            <div class="attachment">
                            @if($comment->attachment != null)
                                <img style="max-width:400px" src="{{url($comment->attachment->path)}}" onClick="newPopup('{{url($comment->attachment->path)}}');">
                            @endif
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </li> 
                    @endif
                    @endforeach
                @else
                    <p>No comment yet for this ticket!</p> 
                @endif
                </ul>
                <hr>
                <?php 
                    $arr = array();
                    foreach ($listcc as $cc) {
                        array_push($arr, $cc->email_user);
                    }
                ?>
                @if(in_array(Session::get('email'), $arr) && $ticket->id_status != 2)    
                {{ Form::open( array('route'=>'komentari','method'=>'post','role'=>'form', 'files' => true)) }}
                    <div class="form-group">
                        <label for="type" style="font-weight:bold">Send as : </label><br>
                        <input id="type" name="type" type="radio" value=2> Send as comment to agent</input>
                        <div class="pull-right">
                            <input id="type" name="type" type="radio" value=1 checked> Send as internal comment</input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="comment" style="font-weight:bold">Comment</label>
                        <textarea class="form-control ckeditor" name="komentar" rows="3" id="comment"></textarea>
                    <input type="hidden" name="id_ticket" value="{{$ticket->id}}">
                    </div>
                    <div class="form-group">
                        <label for="attachment" style="font-weight:bold">File input</label>
                        <input type="file" id="attachment" name="attachment">
                        <p class="help-block">Add your attachment.</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                @endif
                @if(Session::get('role') == 1)
                <div class="btn-group pull-right">
                  <a class="btn btn-danger tooltips" data-placement="top" data-toggle="modal" href="#modalforcedelete{{$ticket->id}}" data-original-title="Force Delete">Force Delete This Ticket?</i></a>
                </div>
                @endif
            </div>
        </section>
    </div>

    <!-- This is right-side section -->
    <div class="col-lg-5">
        <section class="panel">
            <header class="panel-heading accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_detailticket">
                <strong>Detail Ticket 
                    @if($ticket->id_status != 2)
                        @if($ticket->petugas != '')
                        <span class="pull-right">
                            <a class="tooltips" data-placement="top" data-toggle="modal" onclick="loadDataAssignment({{$ticket->id}});" href="#modaleditticket" data-original-title="Edit detail ticket" ><i class="icon_pencil-edit" style="border:none"></i>Edit Ticket</a>
                        </span>
                        @endif
                    @endif
                </strong>
            </header>
            <div id="collapse_detailticket" class="panel-body panel-collapse collapse in">
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Created At</label>
                    <label class="col-lg-8">{{$ticket->created_date}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Created By</label>

                    <label class="col-lg-8">{{$ticket->created_by}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Last Update At</label>
                    <label class="col-lg-8">{{$ticket->last_update_time}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Last Update By</label>
                    <label class="col-lg-8">{{$ticket->last_update_by}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Closed At</label>
                    <label class="col-lg-8">{{$closed_at}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Closed By</label>
                    <label class="col-lg-8">{{$closed_by}}<label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Contact Agent</label>

                    <label class="col-lg-8">{{$ticket->email_user}}</label>

                </div>
                <hr>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">No Antrian/ Trx Date</label>
                    <label class="col-lg-8">#{{$ticket->no_antrian}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Trx Date</label>
                    <label class="col-lg-8">{{$ticket->transaction_date}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Product</label>
                    <label class="col-lg-8">{{$ticket->product->name}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Problem</label>
                    <label class="col-lg-8">{{$ticket->problem->name}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Level</label>
                    <label class="col-lg-8">{{$ticket->level}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Status</label>
                    <label class="col-lg-8">
                        @if($ticket->status->id == 1)
                        <span class="label label-danger"><i class="icon_profile"></i> 
                        <?php
                            switch ($ticket->group_ass) {
                              case 3:
                                echo 'Open in CS';
                                break;
                              case 5:
                                echo 'Open in IT';
                                break;
                              case 6:
                                echo 'Open in QA';
                                break;
                              case 2:
                                echo 'Open in PIC';
                                break;
                              case 1:
                                echo 'Open in Admin';
                                break;
                              default:
                                echo 'Open in Others';
                                break;
                            }
                        ?>    
                        @elseif($ticket->status->id == 2)
                        <span class="label label-success"><i class="icon_check_alt"></i> 
                            {{$ticket->status->name}}
                        @elseif($ticket->status->id == 3)
                        <span class="label label-warning"><i class="icon_profile"></i> 
                            {{$ticket->status->name}}
                        @else
                        <span class="label label-primary"><i class="icon_profile"></i> 
                            {{$ticket->status->name}}
                        @endif
                        </span></td>
                    </label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Assigned To</label>
                    <label class="col-lg-8">{{$ticket->petugas}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Untouched Time</label>
                    <label class="col-lg-8">
                        <script type="text/javascript">
                            if({{$ticket->status->id}} == 1) {
                                document.write(moment('{{$ticket->created_date}}').fromNow(true));
                            }
                            else {
                                // document.write('Butuh start solving_time');
                                var a =moment('{{$ticket->created_date}}');
                                var b =moment('{{$ticket->assign_time}}');
                                document.write(a.from(b, true));
                            }
                        </script>
                    </label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Expected Time</label>
                    <label class="col-lg-8">
                    @if($ticket->resolution_time == '')
                    <a class="tooltips" data-placement="top" data-toggle="modal" href="#modaleditticket" data-original-title="Set Expected Time" > - [set expected time]</a>
                    @else
                    {{$ticket->resolution_time}} minutes
                    @endif
                    </label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Solving Time</label>
                    <label class="col-lg-8">
                        <script type="text/javascript">
                            if({{$ticket->status->id}} == 1) {
                                document.write('-');
                            }
                            else if({{$ticket->status->id}} == 2) {
                                // document.write('Butuh closing time');
                                document.write(moment('{{$ticket->assign_time}}').from('{{$ticket->closing_time}}', true));
                            }
                            else {
                                // document.write('Butuh start solving_time');
                                document.write(moment('{{$ticket->assign_time}}').fromNow(true));
                            }
                        </script>
                    </label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">FO/ASOC</label>
                    <label class="col-lg-8">{{$ticket->name_fo}}</label>
                </div>
                <div class="row">
                    <label style="font-weight:bold" class="col-lg-4">Channel</label>
                    <label class="col-lg-8">{{$ticket->channel}}</label>
                </div>
            </div>
        </section>
        <section class="panel" style="margin-top:-25px">
            <header class="panel-heading accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_detailtrx">
                <strong>Detail Transaksi Bermasalah</strong>
            </header>
            <div id="collapse_detailtrx" class="panel-body panel-collapse collapse in">
                @foreach($arr_trx as $trx) 
                <div>
                    @foreach(explode(';',$trx) as $detail)
                    <div class="row">
                        <label>{{$detail}}</label>
                    </div>
                    @endforeach
                </div>
                <br>
                @endforeach
            </div>
        </section>
    </div>
    <!-- End of right-side section -->

<!-- Start of Modal for main-section -->
    <!-- Modal assign to me ticket-->
    <div class="modal fade" id="modalassigntome" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Assign ticket #{{$ticket->id}}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/setPetugas')}}" method="post" name="assignto">
                      <div class="form-group " id="inputidtiket">
                      </div>
                      <div class="form-group">
                        <input type="hidden" name="id_tiket" value="{{$ticket->id}}"></input>
                        <label for="addassignment">Are you sure to assign this ticket ?</label>
                      </div>
                      <br>
                      <button data-dismiss="modal" class="btn btn-default" type="button">No</button>
                      <button type="submit" class="btn btn-primary">Yes</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal assign to me ticket-->
    <!-- Modal assign to ticket-->
    <div class="modal fade" id="modalassignto" tabixndex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                    <h4 class="modal-title">Assign ticket #{{$ticket->id}}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/setGroupAssign')}}" method="post">
                      <div class="form-group ">
                        <input type="hidden" name="id_tiket" value="{{$ticket->id}}"></input>
                        <label for="assignment">Assign ticket to Group</label>
                        <select class="form-control m-bot15" name="group">
                            <option value=3>Customer Service</option>
                            <option value=5>IT Helpdesk</option>
                            <option value=6>QA</option>
                            <option value=2>PIC Product</option>
                        </select>
                      </div>
                      <br>
                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                      <button type="submit" class="btn btn-primary">Save</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal assign to ticket-->  
    <!-- Modal delete ticket-->
    <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete ticket #{{$ticket->id}}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/delete/'.$ticket->id)}}" method="get">
                      Delete this ticket?
                      <br><br>
                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                      <button type="submit" class="btn btn-danger">Delete</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal delete ticket--> 
    <!-- Modal force delete ticket-->
    <div class="modal fade" id="modalforcedelete{{$ticket->id}}" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Remove ticket #{{$ticket->id}} from database?</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/forcedelete')}}" method="post">
                      <div class="form-group ">
                        <input type="hidden" name="id_tiket" value="{{$ticket->id}}"></input>
                      </div>
                      <br>
                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                      <button type="submit" class="btn btn-danger">Delete</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal force delete ticket--> 
    <!-- Modal closing ticket-->
    <div class="modal fade" id="modalclosing" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Set ticket #{{$ticket->id}} as closed?</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/setClosed')}}" method="post">
                      <div class="form-group ">
                        <input type="hidden" name="ticket_id" value="{{$ticket->id}}"></input>
                      </div>
                      Set this ticket as closed?
                      <br><br>
                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                      <button type="submit" class="btn btn-success">Save</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal closing ticket-->
    <!-- Modal re-open ticket-->
    <div class="modal fade" id="modalreopen" tabindex="-1" role="dialog" aria-labelledby="reopenModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Set ticket #{{$ticket->id}} as open?</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/setOpen')}}" method="post">
                      <div class="form-group ">
                        <input type="hidden" name="ticket_id" value="{{$ticket->id}}"></input>
                      </div>
                      Set this ticket as open?
                      <br><br>
                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                      <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal re-open ticket-->
    <!-- Modal cc ticket-->
    <div class="modal fade" id="modalcc" tabindex="-1" role="dialog" aria-labelledby="reopenModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit cc ticket</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/editcc')}}" method="post">
                        <div class="form-group ">
                            <input type="hidden" name="ticket_id" value="{{$ticket->id}}"></input>
                        </div>
                        <div class="form-group">
                          <label for="cc" class="control-label">CC this ticket to</label>
                          <select class="select2" multiple id="cc" name="cc[]">
                            <?php $user=new UserController();?>
                            <?php $user::getAllCc($ticket->id);?>
                          </select>
                        </div>                                  
                        <br><br>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button type="submit" class="btn btn-success">Save</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal cc ticket-->
    <!-- Modal duplicate ticket-->
    <div class="modal fade" id="modalduplicate" tabindex="-1" role="dialog" aria-labelledby="duplicateModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Report Duplicate</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/duplicate')}}" method="post">
                        <div class="form-group ">
                            <input type="hidden" name="ticket_id" value="{{$ticket->id}}"></input>
                        </div>
                        <div class="form-group">
                          <label for="duplicate_to" class="control-label">Report this ticket as duplicate to</label>
                            <select class="form-control" id="duplicate_to" name="duplicate_to" required>
                                <?php $ticketduplicate = new TicketController2();?>
                                <?php $ticketduplicate::getTicketNoDuplicate($ticket->id);?>
                            </select>
                        </div>                                  
                        <br><br>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-success" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal duplicate ticket-->
    <!-- notif-->
    @if (Session::has('flash_error'))                    
    <script>
        $(function() {
            $('#notiferror').modal('show');
        });
    </script>
    <div class="modal fade" id="notiferror" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">                            
                <div class="modal-body">                                
                    <h4 class="modal-title">{{ Session::get('flash_error') }}</h4>
                    <br>
                    <button data-dismiss="modal" class="btn btn-danger" type="button">Try Again</button>
                </div>
            </div>
        </div>
    </div>
    @elseif (Session::has('flash_notice'))
    <script>
        $(function() {
            $('#notifsuccess').modal('show');
        });
    </script>       
    <div class="modal fade" id="notifsuccess" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">                            
                <div class="modal-body">                                
                    <h4 class="modal-title">{{ Session::get('flash_notice') }}</h4>
                    <br>
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Oke</button>
                </div>
            </div>
        </div>
    </div>             
    @endif
    <!-- Modal edit ticket-->
    <div class="modal fade" id="modaleditticket" tabindex="-1" role="dialog" aria-labelledby="closingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                    <h4 class="modal-title">Edit Detail Ticket {{$ticket->id}}</h4>
                </div>
                <div class="modal-body">
                    {{ Form::model($ticket, array('method' => 'PATCH', 'route' => array('tiketUpdate', $ticket->id))) }}
                        <div class="form-group">
                          <div class="row">
                            <div class="col-lg-6">
                              <label for="tanggal_transaksi">Trx Date</label>
                              <div>
                                <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd">
                                    @if($ticket->transaction_date == '0000-00-00')
                                    <input class="form-control" size="16" type="text" name="tglTransaksi" id="tglTransaksi">
                                    @else
                                    <input class="form-control" size="16" type="text" value="{{$ticket->transaction_date}}" name="tglTransaksi" id="tglTransaksi">
                                    @endif
                                    <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                                </div>          
                              </div>
                            </div>
                            <div class="col-lg-6">
                              <label for="no_antrian">No Antrian</label>
                              <input value="{{$ticket->no_antrian}}" onkeyup="getTrx();" type="text" class="form-control" id="no_antrian" placeholder="123" name="noAntrian" required>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="detail_trx" class="control-label">Trx Bermasalah</label>
                          <select class="select2" multiple id="detail_trx" name="detail_trx[]">
                          </select>
                        </div>
                        <div class="form-group ">
                          <label for="product">Product</label>
                          <select class="form-control m-bot15" id="product" name="product" onchange="loadDataProblemAssignment(this.value);" required>
                            @foreach($products as $product)
                              <option value="{{$product->id}}" @if ($product->id == $ticket->product->id) selected="selected" @endif>{{$product->name}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group ">
                          <label for="problem">Problem</label>
                          <select class="form-control m-bot15" id="problem" name="problem">
                                <option value="{{$problem->id}}">{{$problem->name}}</option>
                          </select>
                        </div>
                        <div class="form-group ">
                          <label for="level">Level</label>
                          <select class="form-control m-bot15" id="level" name="level">
                              <option value="High" @if ($ticket->level == "High") selected @endif>High</option>
                              <option value="Medium" @if ($ticket->level == "Medium") selected @endif>Medium</option>
                              <option value="Low" @if ($ticket->level == "Low") selected @endif>Low</option>
                          </select>
                        </div>
                        <br>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal edit ticket--> 
     <!-- Modal mark as solved-->
    <div class="modal fade" id="modalsolved" tabindex="-1" role="dialog" aria-labelledby="solvedModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                    <h4 class="modal-title">Mark ticket #{{$ticket->id}} as solved?</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/setSolved')}}" method="post">
                      <div class="form-group ">
                        <input type="hidden" name="ticket_id" value="{{$ticket->id}}"></input>
                      </div>
                      Mark this ticket as Solved?
                      <br><br>
                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                      <button type="submit" class="btn btn-success">Save</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal solved ticket-->
    <!-- Modal mark as unsolved-->
    <div class="modal fade" id="modalunsolved" tabindex="-1" role="dialog" aria-labelledby="unsolvedModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                    <h4 class="modal-title">Mark ticket #{{$ticket->id}} as Unsolved?</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/setUnsolved')}}" method="post">
                      <div class="form-group ">
                        <input type="hidden" name="ticket_id" value="{{$ticket->id}}"></input>
                      </div>
                      Mark this ticket as Unsolved?
                      <br><br>
                      <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                      <button type="submit" class="btn btn-success">Save</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal unsolved ticket-->
<!-- End of Modal for main-section -->
</div>    
<!-- page end-->
@stop