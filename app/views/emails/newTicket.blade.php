<h4>You have a new ticket [ID Ticket <a href="{{$url}}">#{{$id_tiket}}]</a> created by {{$user}}</h4>
<hr>
<div id="detail_tiket">
	<strong>{{$subject}}</strong>
	<p>{{$messages}}</p>
	<br>
	<p>No Antrian : {{$no_antrian}}</p>
	<p>Tanggal Transaksi : {{$transaction_date}}</p>
	<p>Product : {{$product_name}}</p>
	<p>Problem : {{$problem_name}}</p>
</div>