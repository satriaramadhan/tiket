<?php

class Pics extends Eloquent {
	protected $table = 'pic';
	protected $fillable = array('id_product','id_pic','email_pic','nama_pic');
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('products','id_product');
    }
}