<?php

class Problems extends Eloquent {
	protected $table = 'problem';
	protected $fillable = array('name','id_product');
    public $timestamps = false;

	  public function ticket()
    {
        return $this->hasMany('tickets','id_problem');
    }

     public function product()
    {
        return $this->belongsTo('products','id_product');
    }
}