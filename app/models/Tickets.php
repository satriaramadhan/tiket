<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Tickets extends Eloquent {

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    
    protected $table = 'ticket';
    
    protected $guarded = array('id');
	
    protected $fillable = array('no_antrian','transaction_date','subject','message','id_status','level',
        'channel','created_date','created_by','last_update_time','last_update_by','email_user','user_name',
        'id_fo','name_fo','id_product','id_problem','petugas','resolution_time','assign_time','closing_time',
        'ass_cs','ass_it','ass_qa','ass_pic','last_solved_by','group_ass','duplicate_to','device_id');
    
    public $timestamps = false;
	
    public function comment()
    {
    	return $this->hasMany('comments','id_ticket');
    }

    public function cc()
    {
        return $this->hasMany('cc','id_ticket');
    }

    public function product()
    {
        return $this->belongsTo('products','id_product');
    }

    public function problem()
    {
        return $this->belongsTo('problems','id_problem');
    }

    public function attachment()
    {
    	return $this->hasOne('attachmentsTickets','id_ticket');
    }
    public function status()
    {
        return $this->belongsTo('status','id_status');
    }
    
}