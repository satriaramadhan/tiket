<?php

class Products extends Eloquent {
    
	protected $table = 'product';
    protected $guarded = array('id');
	protected $fillable = array('name','id_parent','last_update_time','last_update_by');
    public $timestamps = false;

	public function problem()
    {
        return $this->hasMany('problems','id_product');
    }

    public function pic()
    {
        return $this->hasMany('pics','id_product');
    }

    public function ticket()
    {
        return $this->hasMany('tickets','id_product');
    }

    public function parent()
    {
        return $this->belongsTo('products','id_parent');
    }
}