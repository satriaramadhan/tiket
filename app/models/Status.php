<?php

class Status extends Eloquent {
	protected $table = 'status';
	protected $fillable = array('name');
    public $timestamps = false;

	public function ticket()
    {
        return $this->hasMany('tickets','id_status');
    }
}