<?php

class Cc extends Eloquent {
	protected $table = 'cc';
	protected $guarded = array('id');
	protected $fillable = array('id_ticket','email_user','user_name');
    public $timestamps = true;

	public function ticket()
    {
        return $this->belongsTo('tickets','id_ticket');
    }
}