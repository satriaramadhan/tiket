<?php

class Comments extends Eloquent {
	protected $table = 'comment';
    protected $guarded = array('id');
	protected $fillable = array('id_ticket','id_user','date','message','type');
    public $timestamps = false;

	public function ticket()
    {
        return $this->belongsTo('tickets','id_ticket');
    }

    public function attachment()
    {
        return $this->hasOne('attachmentscomments', 'id_comment');
    }
}