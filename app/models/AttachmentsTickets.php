<?php

class AttachmentsTickets extends Eloquent {
	protected $table = 'attachmentTicket';
	protected $fillable = array('id_ticket','file_name','path');
	public $timestamps = false;

	public function ticket()
    {
        return $this->belongsTo('tickets','id_ticket');
    }

}