<?php

class AttachmentsComments extends Eloquent {
	protected $table = 'attachmentComment';
	protected $fillable = array('id_comment','file_name','path');
	public $timestamps = false;

	public function comment()
    {
        return $this->belongsTo('comments','id_comment');
    }

}