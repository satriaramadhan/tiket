<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
// Error Page
Route::get('/error/404','ViewController@get404');
Route::get('/error/500','ViewController@get500');
/**
 * Route for UserController. Include routing for login, logout, manage user, and profile.
 */
// Routing for login and logout
Route::get('/login',array('as' => 'login', 'uses' => 'UserController@getLogin'))->before('guest');
Route::post('/login', 'UserController@postLogin')->before('guest');
Route::get('/logout', array('as' => 'logout', 'uses' => 'UserController@postLogout'))->before('login');
// Routing for manage user
Route::get('/home/user', 'UserController@index')->before('admin');
Route::post('/home/user/create', array('as' => 'createuser','uses' => 'UserController@store'))->before('admin');
Route::get('/home/user/delete/{id}', 'UserController@destroy')->before('admin');
Route::patch('/home/user/update/{id}', array('as' => 'userUpdate','uses' => 'UserController@update'))->before('admin');
// Routing for profile
Route::post('/home/profile/reset/{id}', 'UserController@resetPassword')->before('admin');
Route::get('/home/profile', 'UserController@viewProfile')->before('login');
Route::post('/home/profile/update/{id}', 'UserController@editProfile')->before('login');
Route::post('/home/profile/password/{id}', 'UserController@changePassword')->before('login');
Route::get('/home/profile/manage', array('as' => 'manage', 'uses' => 'UserController@getManageProfile'))->before('login');

Route::get('/getcc/{id}', 'UserController@getAllCc');

/**
 * Route for TicketController2. 
 */
// Get List Ticket
Route::get('/', 'TicketController2@getIndex')->before('login');
Route::get('/myticket', 'TicketController2@getListMyTicket')->before('login');
Route::get('/home', 'TicketController2@getListAllTicket')->before('login');
Route::get('/bystatus/{id_status}', 'TicketController2@getListTicketByStatus')->before('login');
Route::get('/byproduct/{id_product}', 'TicketController2@getListTicketByProduct')->before('login');
Route::get('/bygroup/{group_ass}', 'TicketController2@getListTicketByGroupAss')->before('login');
Route::get('/petugas/{petugas}', 'TicketController2@getListTicketByPetugas')->before('login');
Route::get('/trashed', 'TicketController2@getListTrashedTicket')->before('login');
// Other
Route::get('/detail/{id}', 'TicketController2@getDetailTicket');
Route::get('/home/kudoteam', 'TicketController2@getKudoTeam')->before('login');
Route::get('/home/listproduct', 'TicketController2@getListProduct')->before('login');
Route::get('/assignmentByTicket/{id}', 'TicketController2@getListAssignmentByIdTicket');
// Action
Route::post('/setClosed', 'TicketController2@postSetClosed')->before('login');
Route::post('/setOpen', 'TicketController2@postSetOpen')->before('login');
Route::post('/setSolved', 'TicketController2@postSetSolved')->before('login');
Route::post('/setUnsolved', 'TicketController2@postSetUnsolved')->before('login');
Route::post('/home/ticket/create', array('as' => 'tiketcreate','uses' => 'TicketController2@postCreateByCS'))->before('login');
Route::get('/delete/{id}', 'TicketController2@postSoftDelete')->before('login');
Route::post('/setPetugas', 'TicketController2@postSetAssignTo')->before('login');
Route::post('/setGroupAssign', 'TicketController2@postSetAssignToGroup')->before('login');
Route::post('/restore', 'TicketController2@postRestore')->before('admin');
Route::post('/forcedelete', 'TicketController2@postDelete')->before('admin');
Route::post('/editcc', 'TicketController2@postEditCc')->before('login');
Route::post('duplicate','TicketController2@postReportDuplicateTo');
// Notif WA
Route::get('/getwa','TicketController2@postCreateFromWa');
Route::get('/get/ticket/status/json','TicketController2@getTicketStatusJson');


/**
 * Route for CommentController2. 
 */
Route::post('/komentari', array('as' => 'komentari','uses' => 'CommentController2@postComment'))->before('login');

/**
 * Route for ProductController. 
 */
Route::get('/home/product', 'ProductController@showlist')->before('admin');
Route::post('/createProduct', 'ProductController@createProduct')->before('admin');
Route::get('/product/{id}/deleteProduct', 'ProductController@deleteProduct')->before('admin');
Route::patch('/home/product/update/{id}', array('as' => 'productUpdate','uses' => 'ProductController@editProduct'))->before('admin');
Route::post('editProduct', 'ProductController@editProduct')->before('login');
Route::get('/problemByProduct/{id}', 'ProductController@getListProblemByIdProduct');
Route::get('/assignmentByProduct/{id}', 'ProductController@getListAssignmentByIdProduct');
Route::get('/detailtrx/{param}','ProductController@getTrx');

// Problem
Route::get('/product/{id}', 'ProductController@showDetail')->before('admin');
Route::post('createProblem', 'ProductController@createProblem')->before('admin');
Route::post('editProblem', 'ProductController@editProblem')->before('admin');
Route::get('/product/{id}/deleteProblem/{p_id}','ProductController@deleteProblem')->before('admin');

//Route Role
Route::post('/home/role/create', array('as' => 'createrole','uses' => 'RoleController@store'))->before('admin');
Route::get('/home/role', 'RoleController@index')->before('admin');
Route::get('/home/role/delete/{id}', 'RoleController@destroy')->before('admin');
Route::patch('/home/role/update/{id}', array('as' => 'roleUpdate','uses' => 'RoleController@update'))->before('admin');

// Route Report
Route::get('/home/report/produk', 'ViewController@reportProduk')->before('login');
Route::get('/home/report/problem', 'ViewController@reportProblem')->before('login');
Route::get('/home/report/status', 'ViewController@reportStatus')->before('login');
Route::get('/home/report/priority', 'ViewController@reportPriority')->before('login');
Route::get('/home/report/periode', 'ViewController@reportPeriode')->before('login');
//Test WA
Route::get('/tes/WA', 'ViewController@waAPI'); 

Route::patch('/home/ticket/update/{id}', array('as' => 'tiketUpdate','uses' => 'TicketController2@edit'))->before('login');

// API
Route::get('/api/getProducts', 'apiController@getProducts');
Route::post('/api/listTicket', 'apiController@getListTicket');
Route::post('/api/setClosed', 'apiController@postSetClosed');
Route::post('/api/setOpen', 'apiController@postSetOpen');
Route::get('/api/getProblems', 'apiController@getProblems');
Route::get('/api/getCommentTicket', 'apiController@getCommentTicket'); // by id. Untuk tambahan jika ingin menampilkan detail ticket.

// Sample Script

Route::get('/sample/ajaxLoad', 'sampleController@ajaxLoad');
Route::get('/sample/sampleOption/{id}', 'sampleController@sampleOption');
Route::get('tes/tes', 'TicketController2@tesInOrWhere');



